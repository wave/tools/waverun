#!/bin/sh

# read namelist.nml and extract keys and values to write them into the global attributes file for netcdf output files

if [ $# -ne 2 ]
then
  echo 'need 2 arguments:'
  echo '$1 : input file [namelists.nml]'
  echo '$2 : output file [NC_globatt.inp]'
  exit 1
fi

inputfile=$1
outputfile=$2

cat $inputfile | while read line; do


#skip line starting by $
if [ "$(echo $line | cut -c 1)" != '$' ]
then
  #skip line without =
  if [ $(echo $line | grep -c '=') -gt 0 ]
  then
    #loop on each word on the line
    numwords=$(echo $line | wc -w)
    for i in $(seq 1 $numwords)
    do
      #if word is =, get the variable and value
      if [ "$(echo $line | cut -d ' ' -f$i)" = '=' ]
      then
        var=$(echo $line | cut -d ' ' -f$(expr $i - 1))
        val=$(echo $line | cut -d ' ' -f$(expr $i + 1))
        echo $var | cut -d ',' -f1 >> $outputfile
        echo $val | cut -d ',' -f1 >> $outputfile
      fi
    done
  fi
fi

done

