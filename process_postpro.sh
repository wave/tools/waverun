#!/bin/bash -e

# -------------------------------------------------------------------- #
# Waverun toolbox : process model postprocessing                       #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : GPLv3                                                      #
# Creation date : 01-Dec-2011                                          #
# Modification date : 06-Jan-2022                                      #
# -------------------------------------------------------------------- #


###########################################################################################
#   process postpro for a selected time span defined in argument                          #
#                                                                                         #
# need to be in main_dir to execute the script                                            #
#                                                                                         #
# example :                                                                               #
#  qsub -v startrun='20161101 000000',stoprun='20161201 000000'                           #
#       -lmem=20G -l walltime=12:00:00                                                    #
#       -N GLOBpost ~/TOOLS/SCRIPTS/HINDCAST/process_postpro.sh                           #
#                                                                                         #
# additional bashrc file can be sourced if defined by $WAVERUN_PROFIL (with -v for qsub)  #
###########################################################################################


#----------------------
## 1. INPUT ARGUMENTS
#----------------------
echo -n '1. date: ' && date

# shell arguments
if [ $# -ge 2 ] ; then
  startrun=$1  # '20170314 000000'
  stoprun=$2   # '20170320 000000'
fi

# qsub arguments
if [ -z "$(echo $startrun)" ] || [ -z "$(echo $stoprun)" ]; then
  echo ''
  echo '[ERROR] missing startrun,stoprun arguments in qsub command'
  echo "[EXAMPLE] qsub -v startrun='20161101 000000',stoprun='20161201 000000' process_postpro.sh"
  exit 1
fi

# start and stop dates
echo "startrun: $startrun"
echo "stoprun: $stoprun"

startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -uR)"

stopyear="$(echo $stoprun | cut -c1-4)"
stopmonth="$(echo $stoprun | cut -c5-6)"
stopday="$(echo $stoprun | cut -c7-8)"
stophour="$(echo $stoprun | cut -c10-11)"
stopmin="$(echo $stoprun | cut -c12-13)"
stopsec="$(echo $stoprun | cut -c14-15)"
stoprunformat="$(date -d "${stopyear}-${stopmonth}-${stopday} ${stophour}:${stopmin}:${stopsec}" -uR)"



#----------------------
## 2. ENVIRONMENT
#----------------------
echo -n '2. date: ' && date

# pbs environment
if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  cd $PBS_O_WORKDIR
fi

# wavepath environment
if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
else  echo 'no wavepath.env found in $PWD or $HOME' ; ls $PWD; exit 1 ; fi
echo "source $wavepath" && source $wavepath

# additional environment
if [ ! -z "$(echo $WAVERUN_PROFIL)" ] ; then
  echo "source $WAVERUN_PROFIL" && source $WAVERUN_PROFIL
fi

# mpi environment
if [ $NCPUS -eq 1 ]; then
  MPI_LAUNCH=""
  echo "sequentiel mode $MPI_LAUNCH"
else
  if [ -z "$(echo $MPI_LAUNCH)" ] ; then
    MPI_LAUNCH=mpiexec
  fi
  echo "MPI_LAUNCH : $(which $MPI_LAUNCH)"
fi

# wavesetup environment
echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env

# Error message function
errmsg ()
{
  echo "" 2>&1
  while [ $# != 0 ]
  do
    echo "ERROR: $1" 2>&1
    shift
  done
  echo "" 2>&1
}

# use RAM if less than 32days to postprocess
if [ $(($(date -u -d "$stoprunformat" +%s) - $(date -u -d "$startrunformat" +%s))) -lt 2764800 ]; then
  USE_RAM=true
else
  USE_RAM=false
fi

#----------------------
## 3. INIT & CHECK
#----------------------
echo -n '3. date: ' && date

# check wavesetup.env
$waverun_path/check_setup.sh $main_dir

# set dates
logdate="${startyear}-${startmonth}-${startday}T${starthour}_${stopyear}-${stopmonth}-${stopday}T${stophour}"

# date cycle
if [ -e $main_dir/date_cycle ] ; then
  datecycle=$(cat $main_dir/date_cycle)
  cycleyear=$(echo ${datecycle:0:4})
  cyclemonth=$(echo ${datecycle:4:2})
  cycleday=$(echo ${datecycle:6:2})
  cyclehour=$(echo ${datecycle:9:2})
  cycledate=$(date -d "$cycleyear-$cyclemonth-$cycleday" +%Y%m%d)T$cyclehour
  cycledateformat=$(date -d "$cycleyear-$cyclemonth-$cycleday $cyclehour:00:00" -u)
  pastcycledate=$(date -d "$cycledateformat -1 days" +%Y%m%d)
fi

# check run dates consistency
if [ "$startyear" == "$stopyear" ] ; then
  year=$startyear
else
  if [ -e $main_dir/date_cycle ] ; then
    echo "[INFO] no check done on overlapping years for forecast system"
    year=$stopyear
  else
    if [ $(expr $stopyear - $startyear) -gt 2 ] ; then
      echo "   [ERROR] It is not possible to run over more than one year. Reduce the duration."
      exit 1
    elif [ $(expr $stopyear - $startyear) -eq 2 ] || [ $startmonth -le 11 ] ; then
      if [ $stopmonth -ne 1 ] || [ $stopday -ne 1 ] ; then
        echo "   [ERROR] It is not possible to run over two different years. Reduce the duration."
        exit 1
      fi
    fi
    # set year for forcing
    if [ $stopmonth -eq 1 ] && [ $stopday -eq 1 ] ; then
      year=$(expr $stopyear - 1)
    else
      year=$stopyear
    fi
  fi
fi

# make work directory
export path_w="$work_path/$run_tag/work${logdate}"
export path_o="$work_path/$run_tag/work${logdate}/output"
mkdir -p $path_w
ln -sfn $path_w $work_path/$run_tag/lastwork
ln -sfn $path_w $run_path/$run_tag/lastwork
ln -sfn $path_w $work_path/lastwork
ln -sfn $path_w $run_path/lastwork

# create output path
echo "   [INFO] output path : $path_o"
echo "   [INFO] work path : $path_w"
mkdir -p $path_o
if [ ! -e $path_w ] ; then
  echo "ERROR: the monthly work directory doesn't exist :"
  echo "$path_w"
  echo "       check at the end of process_hindcast.sh to know how to do it"
  exit 1
fi
cd $path_w

#check if some errors occured during last process
echo "*** check past log ***"
if [ ! -z "$(ls -A $path_o 2> /dev/null)" ] ; then
  $waverun_path/check_error.sh $path_o
fi


#----------------------
## 4. PROGRAM
#----------------------
echo -n '4. date: ' && date

###########################
# dedicated spectral grid #
###########################

UPTS='T'
if [ "$dt_po" = "0" ]
then
  UPT='F'
  buoys='no'
fi
if [ "$buoys" = "no" ]
then
	UPTS='F'
fi
echo "   [INFO] unified point = $UPTS"


#########################
# spectral track output #
#########################

function make_trnc_nml {

mod="$1"

cat > ww3_trnc.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_trnc.nml - Track output post-processing          !
! -------------------------------------------------------------------- !


! -------------------------------------------------------------------- !
! Define the output fields to postprocess via TRACK_NML namelist
!
! * namelist must be terminated with /
! * definitions & defaults:
!     TRACK%TIMESTART            = '19000101 000000'  ! Stop date for the output field
!     TRACK%TIMESTRIDE           = '0'                ! Time stride for the output field
!     TRACK%TIMECOUNT            = '1000000000'       ! Number of time steps
!     TRACK%TIMESPLIT            = 6                  ! [4(yearly),6(monthly),8(daily),10(hourly)]
! -------------------------------------------------------------------- !
&TRACK_NML
  TRACK%TIMESTART        =  '${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}'
  TRACK%TIMESTRIDE       =  '$dt_oat'
  TRACK%TIMECOUNT        =  '$(( $(wc -l $path_d/track_i.$mod1 | cut -d' ' -f1) * 8 ))'
  TRACK%TIMESPLIT        =  $ndates
/


! -------------------------------------------------------------------- !
! Define the content of the output file via FILE_NML namelist
!
! * namelist must be terminated with /
! * definitions & defaults:
!     FILE%PREFIX        = 'ww3.'            ! Prefix for output file name
!     FILE%NETCDF        = 3                 ! Netcdf version [3|4]
! -------------------------------------------------------------------- !
&FILE_NML
  FILE%PREFIX        = '${project}${mod1}_'
  FILE%NETCDF        = $ncvers
/


! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF

chmod 775 ww3_trnc.nml
}


function make_trnc_inp {

mod="$1"

cat > ww3_trnc.inp << EOF
$ -------------------------------------------------------------------- $
$ WAVEWATCH III Track output post-processing                           $
$ -------------------------------------------------------------------- $
$ First output time (yyyymmdd hhmmss), increment of output (s),
$ and number of output times.
$
  ${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}  $dt_oat  $(( $(wc -l $path_d/track_i.$mod1 | cut -d' ' -f1) * 8 ))
$
$ Output type -------------------------------------------------------- $
$ netCDF version [3,4]
$ file prefix
$ number of characters in date [4(yearly),6(monthly),8(daily),10(hourly)]
$
  $ncvers
  ${project}${mod1}_
  $ndates
$
$ -------------------------------------------------------------------- $
$ End of input file                                                    $
$ -------------------------------------------------------------------- $
EOF

chmod 755 ww3_trnc.inp
}


mod1=$(echo $mods | cut -d ' ' -f1)
if [ -e track_o.$mod1 ] && [ $dt_oat -gt 0 ] ; then

  echo "*** ww3_trnc on $mod1 ***"
  rm -f $path_w/ww3_trnc && cp $path_e/ww3_trnc $path_w/ && chmod 775 $path_w/ww3_trnc
  mkdir -p $path_w/TRACK_NC/$mod1 && rm -rf $path_w/TRACK_NC/$mod1/*
  cd $path_w/TRACK_NC/$mod1
  ln -sfn $path_w/track_o.$mod1 track_o.ww3
  ln -sfn $path_w/mod_def.$mod1 mod_def.ww3


  if [ -e "$path_d/ww3_trnc.inp.$mod1" ]
  then
    echo "   [INFO] copy ww3_trnc.inp.${mod1} file from data folder"
    sed -e "s/OUTPUT_TIME/${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}  $dt_oat  $(( $(wc -l $path_d/track_i.$mod1 | cut -d' ' -f1) * 8 ))/" \
        -e "s/FILE_PREFIX/${project}${mod1}_/" -e "s/NDATES/$ndates/" \
        -e "s/NCVERS/$ncvers/"  \
          $path_d/ww3_trnc.inp.$mod1 > ww3_trnc.inp
  else
    echo "   [INFO] create ww3_trnc.nml file from scratch"
    eval make_trnc_nml $mod
  fi

  echo -n "ww3_trnc... "
  echo -n 'start. date: ' && date
  if ! $path_w/ww3_trnc >& $path_o/ww3_trnc.${mod1}_${logdate}.out
  then errmsg "Error occured during ww3_trnc";  exit 1; fi
  echo -n 'stop. date: ' && date
  unlink mod_def.ww3
  unlink track_o.ww3
  if [ -e ww3_trnc.inp ] ; then
    mv ww3_trnc.inp $path_w/ww3_trnc.inp.$mod1
  fi
  if [ -e ww3_trnc.nml ] ; then
    mv ww3_trnc.nml $path_w/ww3_trnc.nml.$mod1
  fi

fi


################################
# output gridded interpolation #
################################

mod1=$(echo $mods | cut -d ' ' -f1)
for mod in $mods
do
  if [ "$mod" != "$mod1" ] && [ -e $path_d/ww3_gint.inp.$mod ] ; then
    echo "*** ww3_gint on $mod ***" 
    cd $path_w
    cp $path_d/ww3_gint.inp.$mod ww3_gint.inp
    cp $path_e/ww3_gint . && chmod 775 ww3_gint
    echo -n "ww3_gint... "
    echo -n 'start. date: ' && date
    if ! ./ww3_gint >& $path_o/ww3_gint.${mod}_${logdate}.out
    then errmsg "Error occured during ww3_gint";  exit 1; fi
    echo -n 'stop. date: ' && date
  fi
done


########################
# output gridded field #
########################

function make_ounf_nml() {

mod="$1"

cat > ww3_ounf.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_ounf.nml - Grid output post-processing           !
! -------------------------------------------------------------------- !


! -------------------------------------------------------------------- !
! Define the output fields to postprocess via FIELD_NML namelist
!
! * the detailed list of field names FIELD%LIST is given in ww3_shel.nml
!  DPT CUR WND AST WLV ICE IBG D50 IC1 IC5
!  HS LM T02 T0M1 T01 FP DIR SPR DP HIG
!  EF TH1M STH1M TH2M STH2M WN
!  PHS PTP PLP PDIR PSPR PWS PDP PQP PPE PGW PSW PTM10 PT01 PT02 PEP TWS PNR
!  UST CHA CGE FAW TAW TWA WCC WCF WCH WCM FWS
!  SXY TWO BHD FOC TUS USS P2S USF P2L TWI FIC
!  ABR UBR BED FBB TBB
!  MSS MSC WL02 AXT AYT AXY
!  DTD FC CFX CFD CFK
!  U1 U2
!
! * namelist must be terminated with /
! * definitions & defaults:
!     FIELD%TIMESTART            = '19000101 000000'  ! Stop date for the output field
!     FIELD%TIMESTRIDE           = '0'                ! Time stride for the output field
!     FIELD%TIMECOUNT            = '1000000000'       ! Number of time steps
!     FIELD%TIMESPLIT            = 6                  ! [4(yearly),6(monthly),8(daily),10(hourly)]
!     FIELD%LIST                 = 'unset'            ! List of output fields
!     FIELD%PARTITION            = '0 1 2 3'          ! List of wave partitions ['0 1 2 3 4 5']
!     FIELD%SAMEFILE             = T                  ! All the variables in the same file
!     FIELD%TYPE                 = 3                  ! [2 = SHORT, 3 = it depends , 4 = REAL]
! -------------------------------------------------------------------- !
&FIELD_NML
  FIELD%TIMESTART        =  '${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}'
  FIELD%TIMESTRIDE       =  '$dt_mwp'
  FIELD%TIMESPLIT        =  $ndates
  FIELD%LIST             =  '$fields'
  FIELD%PARTITION        =  '$outparts'
  FIELD%SAMEFILE         =  $var_together
  FIELD%TYPE             =  $nctype
/


! -------------------------------------------------------------------- !
! Define the content of the output file via FILE_NML namelist
!
! * namelist must be terminated with /
! * definitions & defaults:
!     FILE%PREFIX        = 'ww3.'            ! Prefix for output file name
!     FILE%NETCDF        = 3                 ! Netcdf version [3|4]
!     FILE%IX0           = 1                 ! First X-axis or node index
!     FILE%IXN           = 1000000000        ! Last X-axis or node index
!     FILE%IY0           = 1                 ! First Y-axis index
!     FILE%IYN           = 1000000000        ! Last Y-axis index
! -------------------------------------------------------------------- !
&FILE_NML
  FILE%PREFIX        = '${project}${mod}_'
  FILE%NETCDF        = $ncvers
/

! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF
chmod 775 ww3_ounf.nml
}

function make_ounf_inp() {

mod="$1"

cat > ww3_ounf.inp << EOF
$ -------------------------------------------------------------------- $
$ WAVEWATCH III Grid output post-processing                            $
$ -------------------------------------------------------------------- $
$ First output time (yyyymmdd hhmmss), increment of output (s), 
$ and number of output times.
$
  ${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec} $dt_mwp  10000000
$
$ Fields requested --------------------------------------------------- $
$
$ Output request flags identifying fields as in ww3_shel.inp and
$ section 2.4 of the manual. If the first flag is 'N' then a namelist
$ is read.
$
  N
  $fields
$
$ -------------------------------------------------------------------- $
$ netCDF version [3,4]
$        and variable type 4 [2 = SHORT, 3 = it depends , 4 = REAL]
$ swell partitions [0 1 2 3 4 5]
$ variables in the same file [T] or not [F] 
$
  $ncvers $nctype
  $outparts
  $var_together
$
$ -------------------------------------------------------------------- $
$ File prefix
$ number of characters in date [4(yearly),6(monthly),8(daily),10(hourly)]
$ IX and IY ranges. Note that for UNST grids, the IX range 
$ is the node range and the IY range is normally [1 1]
$ (but in practice, [1 any number] is accepted as well)
$
  ${project}${mod}_
  $ndates
  1 1000000 1 1000000
$
$ For each field and time a new file is generated with the file name
$ ww3.date_xxx.nc , where date is a conventional time indicator with S3
$ characters, and xxx is a field identifier.
$
$ -------------------------------------------------------------------- $
$ End of input file                                                    $
$ -------------------------------------------------------------------- $
EOF
chmod 775 ww3_ounf.inp
}

# copy ww3_ounf
rm -f $path_w/ww3_ounf && cp $path_e/ww3_ounf $path_w/ && chmod 775 $path_w/ww3_ounf

# loops on each domain
for mod in $mods
do
  if [ -e "$path_w/out_grd.$mod" ] && [ $dt_mwp -gt 0 ] ; then

    # create grid folder
    echo "*** ww3_ounf on grid $mod ***"
    cd $path_w
    mkdir -p FIELD_NC/$mod && rm -f FIELD_NC/$mod/*

    # use RAM if less than 32days to postprocess
    if [ $USE_RAM = true ]; then
      echo "   [INFO] RAM postprocessing enabled"
      mkdir -p $TMPDIR/FIELD_NC/$mod
      cd $TMPDIR/FIELD_NC/$mod
    else
      echo "   [INFO] RAM postprocessing disabled"
      cd $path_w/FIELD_NC/$mod
    fi

    # link ww3 files
    ln -sfn $path_w/mod_def.$mod mod_def.ww3
    ln -sfn $path_w/out_grd.$mod out_grd.ww3

    # generates the global attributes for NetCDF files
    #if [ -f "$path_d/NC_globatt.inp" ]
    #then
    #  mv $path_d/NC_globatt.inp $path_d/metadata.csv
    #fi
    if [ -f "$path_d/NC_globatt.inp" ]
    then
      rm -f NC_globatt.inp
      cp $path_d/NC_globatt.inp NC_globatt.inp
      echo 'creation_date' >> NC_globatt.inp 
      now=`date -u +"%Y-%m-%dT%H:%M:%SZ"`
      echo $now               >> NC_globatt.inp  
      echo 'product_version'  >> NC_globatt.inp  
      echo 1.0  >> NC_globatt.inp  
      echo 'history' >> NC_globatt.inp  
      echo "$now : Creation" >> NC_globatt.inp  
      echo 'run_time' >> NC_globatt.inp  
      echo $now  >> NC_globatt.inp  
      echo 'grid' >> NC_globatt.inp  
      echo $mod >> NC_globatt.inp  
      if [ -f "$main_dir/date_cycle" ]
      then
        date_cycle="${cycleyear}-${cyclemonth}-${cycleday}T${cyclehour}:00:00Z"
        echo 'date_cycle' >> NC_globatt.inp  
        echo $date_cycle >> NC_globatt.inp  
      fi
      echo 'forcing_wind' >> NC_globatt.inp  
      echo $wind >> NC_globatt.inp  
      echo 'forcing_ice' >> NC_globatt.inp  
      echo $ice >> NC_globatt.inp  
      echo 'forcing_level' >> NC_globatt.inp  
      echo $lev >> NC_globatt.inp  
      echo 'forcing_current' >> NC_globatt.inp  
      echo $cur >> NC_globatt.inp  
      echo 'forcing_ice1' >> NC_globatt.inp  
      echo $ice1 >> NC_globatt.inp  
      echo 'forcing_ice2' >> NC_globatt.inp  
      echo $ice2 >> NC_globatt.inp  
      echo 'forcing_ice3' >> NC_globatt.inp  
      echo $ice3 >> NC_globatt.inp  
      echo 'forcing_ice4' >> NC_globatt.inp  
      echo $ice4 >> NC_globatt.inp  
      echo 'forcing_ice5' >> NC_globatt.inp  
      echo $ice5 >> NC_globatt.inp  
      if [ -e $path_w/namelists_${mod}.nml ] ; then
        $waverun_path/get_varval.sh $path_w/namelists_${mod}.nml NC_globatt.inp >& $path_o/get_varval_${mod}_${year}${month}.out
      else
        $waverun_path/get_varval.sh $path_w/ww3_grid.inp.$mod NC_globatt.inp >& $path_o/get_varval_${mod}_${year}${month}.out
      fi
    fi

    # create ww3_ounf.nml (from a template or from scratch)
    if [ -f "$path_d/ww3_ounf.nml.$mod" ] ; then
      echo "   [INFO] copy ww3_ounf.nml.${mod} file from data folder"
      sed -e "s/<timestart>/${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}/" \
          -e "s/<timestride>/$dt_mwp/" \
          -e "s/<timestop>/${stopyear}${stopmonth}${stopday} ${stophour}${stopmin}${stopsec}/" \
          -e "s/<timecount>/10000000/" -e "s/<timesplit>/$ndates/" \
          -e "s/<list>/$fields/" -e "s/<partition>/$outparts/" \
          -e "s/<samefile>/$var_together/" -e "s/<type>/$nctype/" \
          -e "s/<prefix>/${project}${mod}_/" -e "s/<netcdf>/$ncvers/" \
          $path_d/ww3_ounf.nml.$mod > ww3_ounf.nml
    elif [ -f "$path_d/ww3_ounf.inp.$mod" ] ; then
      echo "   [INFO] copy ww3_ounf.inp.${mod} file from data folder"
      sed -e "s/OUTPUT_TIME/${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec} $dt_mwp  100000000/" \
          -e "s/FIELDS/$fields/" -e "s/NCVERS/$ncvers/" \
          -e "s/NCTYPE/$nctype/" -e "s/IPART/$outparts/" \
          -e "s/TOGETHER/$var_together/" \
          -e "s/FILE_PREFIX/${project}${mod}_/" -e "s/NDATES/$ndates/" \
          $path_d/ww3_ounf.inp.$mod > ww3_ounf.inp
    else
      echo "   [INFO] create ww3_ounf.nml file from scratch"
      eval make_ounf_nml $mod
    fi


    # process ww3_ounf
    echo -n "ww3_ounf... "
    echo -n 'start. date: ' && date
    if ! $path_w/ww3_ounf >& $path_o/ww3_ounf.${mod}_${logdate}.out
    then errmsg "Error occured during ww3_ounf";  exit 1; fi
    echo -n 'stop. date: ' && date
    # if RAM enabled, move to results from RAM to work directory
    if [ $USE_RAM = true ]; then
      find . -type f -name "*.nc" | xargs -n1 -P28 -I% rsync -a % $path_w/FIELD_NC/$mod
      rm *.nc
    fi
    unlink mod_def.ww3
    unlink out_grd.ww3
    if [ -e ww3_ounf.nml ] ; then
      mv ww3_ounf.nml $path_w/ww3_ounf.nml.$mod
    fi
    if [ -e ww3_ounf.nml.log ] ; then
      mv ww3_ounf.nml.log $path_w/ww3_ounf.nml.log.$mod
    fi
    if [ -e ww3_ounf.inp ] ; then
      mv ww3_ounf.inp $path_w/ww3_ounf.inp.$mod
    fi
    mv NC_globatt.inp $path_w/NC_globatt.inp.$mod
    rm -f gmon.out

    # interpolate fields from mesh on regular grid
    mod1=$(echo $mods | cut -d ' ' -f1)
    for modint in $modsint
    do
      if [ -e $path_d/ww3_interp_${modint}.nml ] ; then
        echo "*** ww3_interp on $modint ***"
        rm -rf $path_w/INTERP_NC/$modint
        mkdir -p $path_w/INTERP_NC/$modint

        # use RAM if less than 32days to postprocess
        if [ $USE_RAM = true ]; then
          echo "   [INFO] RAM postprocessing enabled"
          mkdir -p $TMPDIR/INTERP_NC/$modint
          cd $TMPDIR/INTERP_NC/$modint
        else
          echo "   [INFO] RAM postprocessing disabled"
          cd $path_w/INTERP_NC/$modint
        fi

        sed -e "s/<nctype>/$nctype/" $path_d/ww3_interp_${modint}.nml > ww3_interp.nml
        ls -1 --ignore="*_ef.nc" $path_w/FIELD_NC/$mod1/*.nc > ww3_interp.list
        if [ -e $path_d/ww3_interp_${modint}.grd ] ; then
          rsync -aL $path_d/ww3_interp_${modint}.grd ww3_interp.grd
        fi
        echo -n "ww3_interp... "
        echo -n 'start. date: ' && date
        if ! $path_te/ww3_interp >& $path_o/ww3_interp_${modint}_${logdate}.out
        then errmsg "Error occured during ww3_interp";  exit 1; fi
        echo -n 'stop. date: ' && date
        # if RAM enabled, move to results from RAM to work directory
        if [ $USE_RAM = true ]; then
          find . -type f -name "*.nc" | xargs -n1 -P28 -I% rsync -a % $path_w/INTERP_NC/$modint
          rm *.nc
        fi
        mv fort.* $path_o/
        mv ww3_interp.nml $path_w/ww3_interp.nml.$modint
        mv ww3_interp.list $path_w/ww3_interp.list.$modint
      fi
    done


    # check if some errors occured during process
    $waverun_path/check_error.sh $path_o

  else
    pwd
    echo "[WARNING] no out_grd.$mod found"
  fi
done # mod


###############################
# scores for ECMWF LC-WFV     #
###############################

#create rolling archive for ECMWF over GLOB-30M grid
if [ "${run_tag:0:4}" == "GLOB" ] && [ -e $main_dir/date_cycle ] && [ -e $path_w/FIELD_NC/GLOB-30M ] ; then

  echo $cyclehour

  if [ "$cyclehour" == "12" ] ; then
    ymdformat="$(date -d "$(date -u -d "$cycledateformat" +%Y-%m-%d) 00:00:00" -uR)"
  elif [ "$cyclehour" == "00" ] ; then
    ymdformat="$(date -d "$(date -u -d "$cycledateformat -1 day" +%Y-%m-%d) 12:00:00" -uR)"
  fi
  ymdh=$(date -u -d "$ymdformat" +%Y%m%d%H)
  ymdThZ=$(date -u -d "$ymdformat" +%Y%m%dT%HZ)
  timeunits="$(date -u -d "$ymdformat" +'%Y-%m-%d %H'):00:00"
  
  echo "*** field wave_LOPS_${ymdh}_prod_fc.nc ***"
  outncfile="$path_a/$year/NETCDF/wave_LOPS_${ymdh}_prod_fc.nc"
  outgrb2file="$path_a/$year/GRIB/wave_LOPS_${ymdh}_prod_fc.grb2"
  mkdir -p $path_a/$year/NETCDF $path_a/$year/GRIB
  rm -f $outncfile $outgrb2file
  cd $path_w/FIELD_NC/GLOB-30M
  lastdate=$(date -u -d "$stoprunformat" +%Y%m%d%H)
  globlist=''
  echo "current date : " $ymdh
  echo "last date : " $lastdate
  while [ $ymdh -lt $lastdate ]
  do
    globnew="MARC_WW3-GLOB-30M_${ymdThZ}.nc"
    globlist="$globlist $globnew"
    ymdformat=$(date -d "$ymdformat +3hours" -uR)
    ymdh=$(date -u -d "$ymdformat" +%Y%m%d%H)
    ymdThZ=$(date -u -d "$ymdformat" +%Y%m%dT%HZ)
  done
  echo $globlist
  # extract variables
  ncrcat -v uwnd,vwnd,dir,t02,fp,hs $globlist $outncfile
  # convert time
  ncap2 -O -s 'time=time*24' $outncfile $outncfile
  ncap2 -O -s 'time=time-time(0)' $outncfile $outncfile
  ncatted -a units,time,o,c,"hours since ${timeunits}" $outncfile
  # convert longitude
  ncks -O --msa -d longitude,0.,179.996 -d longitude,-180.,-0.003 $outncfile $outncfile
  ncap2 -O -s 'where(longitude<0) longitude=(longitude+360)%360' $outncfile $outncfile
  # convert fp to tp
  ncap2 -A -s 'tp=(1/fp).short()' $outncfile $outncfile
  ncatted -a long_name,tp,m,c,'wave peak period' $outncfile
  ncatted -a standard_name,tp,m,c,'sea_surface_peak_period' $outncfile
  ncatted -a globwave_name,tp,m,c,'dominant_wave_period' $outncfile
  ncatted -a units,tp,m,c,'s' $outncfile
  ncks -O --no_abc -x -v fp $outncfile $outncfile
  # rename variables
  ncrename -v uwnd,u -v vwnd,v -v dir,mwd -v t02,mp2 -v tp,pp1d -v hs,swh $outncfile
  ncatted -a long_name,u,o,c,'10 metre U wind component' $outncfile
  ncatted -a standard_name,u,d,, $outncfile
  ncatted -a globwave_name,u,d,, $outncfile
  ncatted -a long_name,v,o,c,'10 metre V wind component' $outncfile
  ncatted -a standard_name,v,d,, $outncfile
  ncatted -a globwave_name,v,d,, $outncfile
  ncatted -a long_name,mwd,o,c,'Mean wave period based on second moment' $outncfile
  ncatted -a standard_name,mwd,d,, $outncfile
  ncatted -a globwave_name,mwd,d,, $outncfile
  ncatted -a long_name,mp2,o,c,'Mean wave direction' $outncfile
  ncatted -a standard_name,mp2,d,, $outncfile
  ncatted -a globwave_name,mp2,d,, $outncfile
  ncatted -a long_name,pp1d,o,c,'Peak wave period' $outncfile
  ncatted -a standard_name,pp1d,d,, $outncfile
  ncatted -a globwave_name,pp1d,d,, $outncfile
  ncatted -a long_name,swh,o,c,'Significant height of combined wind waves and swell' $outncfile
  ncatted -a standard_name,swh,d,, $outncfile
  ncatted -a globwave_name,swh,d,, $outncfile
  # convert to grb2
  cdo -f grb2 copy $outncfile ${outgrb2file}.tmp
  # set ecmwf conventions
  grib_set -s centre=84,subCentre=202,tablesVersion=19,productionStatusOfProcessedData=1,typeOfProcessedData=1,productDefinitionTemplateNumber=0,typeOfFirstFixedSurface=103,scaledValueOfFirstFixedSurface=10 ${outgrb2file}.tmp $outgrb2file
  # print ecmwf conventions
  grib_get_data -p centre,subCentre,tablesVersion,productionStatusOfProcessedData,typeOfProcessedData,productDefinitionTemplateNumber,typeOfFirstFixedSurface,scaledValueOfFirstFixedSurface $outgrb2file | head -n2
  rm ${outgrb2file}.tmp
fi



###############################
# fill field from coarse grid #
###############################

# fill grids in high precision grids
for modfill in $modsfill
do
  if [ -e $path_d/mww3_fill_grids_nc.inp.${modfill} ] && [ $dt_mwp -gt 0 ] ; then
    echo "*** fillgrids_nc on $modfill ***"
    cd $path_w
    declare -a inparray
    readarray -t inparray <<< "$(grep -v '^\$' $path_d/mww3_fill_grids_nc.inp.${modfill})"
    srcdir="$(echo ${inparray[0]} | awk -F' ' '{print $1}' | cut -d \" -f2 | cut -d \' -f2)"
    dstdir="$(echo ${inparray[1]} | awk -F' ' '{print $1}' | cut -d \" -f2 | cut -d \' -f2)"
    srcdir="$( cd $srcdir && pwd )"
    dstdir="$( cd $dstdir && pwd )"
    cd $dstdir
    pwd
    cp $path_d/mww3_fill_grids_nc.inp.${modfill} $path_w/mww3_fill_grids_nc.inp
    ls -1 $srcdir/*.nc > $path_w/fillgrids_nc.list
    declare -a srcarray
    readarray -t srcarray < $path_w/fillgrids_nc.list
    i=0
    while [ ${#srcarray[@]} -gt $i ] ; do
      cd $path_w
      srcfile="$(basename ${srcarray[$i]} | awk -F' ' '{print $1}' | cut -d \" -f2 | cut -d \' -f2)"
      date=$(echo $srcfile | cut -d '_' -f3)
      option=$(echo $srcfile | cut -d '_' -f4)
      if [ -z "$(echo $option)" ] ; then
        dstfile=${project}${modfill}_${date}
      else
        dstfile=${project}${modfill}_${date}_${option}
      fi
      echo -n "mww3_fill_grids_nc... "
      echo -n 'start. date: ' && date
      if ! $path_te/mww3_fill_grids_nc $srcfile $dstfile >& $path_o/fillgrids_nc_${modfill}_${logdate}.out
      then errmsg "Error occured during mww3_fill_grids_nc";  exit 1; fi
      echo -n 'stop. date: ' && date
      i=$((i+1))
    done
    mv $path_w/mww3_fill_grids_nc.inp $path_w/mww3_fill_grids_nc.inp.$modfill
    mv $path_w/fillgrids_nc.list $path_w/fillgrids_nc.list.$modfill
  fi
done




########################
# output spectra point #
########################

function make_ounp_nml() {

mod="$1"
if [ "$mod" = "$buoys" ] ; then
  prefix="${project}$(echo $mods | cut -d ' ' -f1)"
else
  prefix="${project}${mod}"
fi
if [ "$pt_together" = "T" ] ; then
  prefix="${prefix}_"
else
  prefix="${prefix}-"
fi

cat > ww3_ounp.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_ounp.nml - Point output post-processing          !
! -------------------------------------------------------------------- !


! -------------------------------------------------------------------- !
! Define the output fields to postprocess via POINT_NML namelist
!
!
! * namelist must be terminated with /
! * definitions & defaults:
!     POINT%TIMESTART            = '19000101 000000'  ! Stop date for the output field
!     POINT%TIMESTRIDE           = '0'                ! Time stride for the output field
!     POINT%TIMECOUNT            = '1000000000'       ! Number of time steps
!     POINT%TIMESPLIT            = 6                  ! [4(yearly),6(monthly),8(daily),10(hourly)]
!     POINT%LIST                 = 'all'              ! List of points index ['all'|'1 2 3']
!     POINT%SAMEFILE             = T                  ! All the points in the same file
!     POINT%BUFFER               = 150                ! Number of points to process per pass
!     POINT%TYPE                 = 1                  ! [0=inventory | 1=spectra | 2=mean param | 3=source terms]
!     POINT%DIMORDER             = T                  ! [time,station=T | station,time=F]
! -------------------------------------------------------------------- !
&POINT_NML
  POINT%TIMESTART        =  '${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}'
  POINT%TIMESTRIDE       =  '$dt_po'
  POINT%TIMESPLIT        =  $ndates
  POINT%SAMEFILE         =  $pt_together
/


! -------------------------------------------------------------------- !
! Define the content of the output file via FILE_NML namelist
!
! * namelist must be terminated with /
! * definitions & defaults:
!     FILE%PREFIX        = 'ww3.'            ! Prefix for output file name
!     FILE%NETCDF        = 3                 ! Netcdf version [3|4]
! -------------------------------------------------------------------- !
&FILE_NML
  FILE%PREFIX        = '$prefix'
  FILE%NETCDF        = $ncvers
/


! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF
chmod 775 ww3_ounp.nml
}


function make_ounp_inp() {

mod="$1"
if [ "$mod" = "$buoys" ] ; then
  prefix="${project}$(echo $mods | cut -d ' ' -f1)"
else
  prefix="${project}${mod}"
fi
if [ "$pt_together" = "T" ] ; then
  prefix="${prefix}_"
else
  prefix="${prefix}-"
fi

cat > ww3_ounp.inp << EOF
$ -------------------------------------------------------------------- $
$ WAVEWATCH III NETCDF Point output post-processing                    $
$ -------------------------------------------------------------------- $
$ First output time (yyyymmdd hhmmss), increment of output (s), 
$ and number of output times.
$
  ${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}  $dt_po  10000000
$
$ Points requested --------------------------------------------------- $
$
$ Define points index for which output is to be generated. 
$ If no one defined, all points are selected
$ One index number per line, negative number identifies end of list.
$ 1
$ 2
$ mandatory end of list
  -1
$
$ -------------------------------------------------------------------- $
$ file prefix
$ number of characters in date [4(yearly),6(monthly),8(daily),10(hourly)]
$ netCDF version [3,4]
$ points in same file [T] or not [F] 
$                    and max number of points to be processed in one pass
$ output type ITYPE [0,1,2,3]
$ flag for global attributes WW3 [0] or variable version [1-2-3-4]
$ flag for dimensions order time,station [T] or station,time [F]
$
  $prefix
  $ndates
  $ncvers
  $pt_together 150
  1
  0
  T
$
$ -------------------------------------------------------------------- $
$ ITYPE = 0, inventory of file.
$            No additional input, the above time range is ignored.
$
$ -------------------------------------------------------------------- $
$ ITYPE = 1, netCDF Spectra.
$          - Sub-type OTYPE :  1 : Print plots.
$                              2 : Table of 1-D spectra
$                              3 : Transfer file.
$                              4 : Spectral partitioning.
$          - Scaling factors for 1-D and 2-D spectra Negative factor
$            disables, output, factor = 0. gives normalized spectrum.
$
  3  1  0
$
$ The transfer file contains records with the following contents.
$
$ - File ID in quotes, number of frequencies, directions and points.
$   grid name in quotes (for unformatted file C*21,3I,C*30).
$ - Bin frequencies in Hz for all bins.
$ - Bin directions in radians for all bins (Oceanographic conv.).
$                                                         -+
$ - Time in yyyymmdd hhmmss format                         | loop
$                                             -+           |
$ - Point name (C*10), lat, lon, d, U10 and    |  loop     | over
$   direction, current speed and direction     |   over    |
$ - E(f,theta)                                 |  points   | times
$                                             -+          -+
$
$ -------------------------------------------------------------------- $
$ ITYPE = 2, netCDF Tables of (mean) parameter
$          - Sub-type OTYPE :  1 : Depth, current, wind
$                              2 : Mean wave pars.
$                              3 : Nondimensional pars. (U*)
$                              4 : Nondimensional pars. (U10)
$                              5 : 'Validation table'
$                              6 : WMO standard output 
$  4
$
$ -------------------------------------------------------------------- $
$ ITYPE = 3, netCDF Source terms
$          - Sub-type OTYPE :  1 : Print plots.
$                              2 : Table of 1-D S(f).
$                              3 : Table of 1-D inverse time scales
$                                  (1/T = S/F).
$                              4 : Transfer file
$          - Scaling factors for 1-D and 2-D source terms. Negative
$            factor disables print plots, factor = 0. gives normalized
$            print plots.
$          - Flags for spectrum, input, interactions, dissipation,
$            bottom, ice and total source term.
$          - scale ISCALE for OTYPE=2,3
$                              0 : Dimensional.
$                              1 : Nondimensional in terms of U10
$                              2 : Nondimensional in terms of U*
$                             3-5: like 0-2 with f normalized with fp.
$
$  4  0  0  T T T T T T T  0
$
$ The transfer file contains records with the following contents.
$
$ - File ID in quotes, nubmer of frequencies, directions and points,
$   flags for spectrum and source terms (C*21, 3I, 6L)
$ - Bin frequencies in Hz for all bins.
$ - Bin directions in radians for all bins (Oceanographic conv.).
$                                                         -+
$ - Time in yyyymmdd hhmmss format                         | loop
$                                             -+           |
$ - Point name (C*10), depth, wind speed and   |  loop     | over
$   direction, current speed and direction     |   over    |
$ - E(f,theta) if requested                    |  points   | times
$ - Sin(f,theta) if requested                  |           |
$ - Snl(f,theta) if requested                  |           |
$ - Sds(f,theta) if requested                  |           |
$ - Sbt(f,theta) if requested                  |           |
$ - Sice(f,theta) if requested                 |           |
$ - Stot(f,theta) if requested                 |           |
$                                             -+          -+
$
$ -------------------------------------------------------------------- $
$ End of input file                                                    $
$ -------------------------------------------------------------------- $
EOF
chmod 775 ww3_ounp.inp
}



function make_spectofreq_nc_inp() {

cat > $path_w/spectofreq_nc.inp << EOF
$ -------------------------------------------------------------------- $
$ WAVEWATCH III conversion from spectral file to freq file             $
$ -------------------------------------------------------------------- $
$
$ output directory
$ frequency cut-off
$ netCDF version of output file [3-4]
$
$
$path_w/FREQ_NC/$mod
0.5
$ncvers
$
$ /!\ Do not put space before and after arguments
$
$
$ PS : need a spectofreq_nc.list file
$ use the command : find SPEC_NC -name "*spec.nc" > spectofreq_nc.list
$
$ -------------------------------------------------------------------- $
$ End of input file                                                    $
$ -------------------------------------------------------------------- $
EOF
chmod 775 $path_w/spectofreq_nc.inp
}

# copy ww3_ounp and spectofreq_nc
rm -f $path_w/ww3_ounp && cp $path_e/ww3_ounp $path_w/ && chmod 775 $path_w/ww3_ounp
rm -f $path_w/spectofreq_nc && cp $path_te/spectofreq_nc $path_w/ && chmod 775 $path_w/spectofreq_nc


# loop on domains and spectral grid
for mod in $buoys
do
  if [ -e "$path_w/out_pnt.$mod" ] && [ $dt_po -gt 0 ] ; then

    # prepare SPEC_NC and FREQ_NC folders
    echo "*** ww3_ounp on $mod ***"
    cd $path_w
    if [ -e SPEC_NC/$mod ]; then mkdir -p OLDSPEC_NC; mv SPEC_NC/$mod OLDSPEC_NC/;fi
    if [ -e FREQ_NC/$mod ]; then mkdir -p OLDFREQ_NC; mv FREQ_NC/$mod OLDFREQ_NC/;fi
    mkdir -p SPEC_NC/$mod
    mkdir -p FREQ_NC/$mod

    # use RAM if less than 32days to postprocess
    if [ $USE_RAM = true ]; then
      echo "   [INFO] RAM postprocessing enabled"
      mkdir -p $TMPDIR/SPEC_NC/$mod
      cd $TMPDIR/SPEC_NC/$mod
    else
      echo "   [INFO] RAM postprocessing disabled"
      cd $path_w/SPEC_NC/$mod
    fi

    # link ww3 files
    ln -sfn $path_w/out_pnt.$mod out_pnt.ww3
    ln -sfn $path_w/mod_def.$mod mod_def.ww3

    # generates the global attributes for NetCDF files
    #if [ -f "$path_d/NC_globatt.inp" ]
    #then
    #  mv $path_d/NC_globatt.inp $path_d/metadata.csv
    #fi
    if [ -f "$path_d/NC_globatt.inp" ]
    then
      rm -f NC_globatt.inp
      cp $path_d/NC_globatt.inp NC_globatt.inp
      echo 'creation_date' >> NC_globatt.inp
      now=`date -u +"%Y-%m-%dT%H:%M:%SZ"`
      echo $now               >> NC_globatt.inp
      echo 'product_version'  >> NC_globatt.inp
      echo 1.0  >> NC_globatt.inp
      echo 'history' >> NC_globatt.inp
      echo "$now : Creation" >> NC_globatt.inp
      echo 'run_time' >> NC_globatt.inp
      echo $now  >> NC_globatt.inp
      echo 'grid' >> NC_globatt.inp
      echo $mod >> NC_globatt.inp
      if [ -f "$main_dir/date_cycle" ]
      then
        date_cycle="${cycleyear}-${cyclemonth}-${cycleday}T${cyclehour}:00:00Z"
        echo 'date_cycle' >> NC_globatt.inp
        echo $date_cycle >> NC_globatt.inp
      fi
      echo 'forcing_wind' >> NC_globatt.inp
      echo $wind >> NC_globatt.inp
      echo 'forcing_ice' >> NC_globatt.inp
      echo $ice >> NC_globatt.inp
      echo 'forcing_level' >> NC_globatt.inp
      echo $lev >> NC_globatt.inp
      echo 'forcing_current' >> NC_globatt.inp
      echo $cur >> NC_globatt.inp
      echo 'forcing_ice1' >> NC_globatt.inp
      echo $ice1 >> NC_globatt.inp
      echo 'forcing_ice2' >> NC_globatt.inp
      echo $ice2 >> NC_globatt.inp
      echo 'forcing_ice3' >> NC_globatt.inp
      echo $ice3 >> NC_globatt.inp
      echo 'forcing_ice4' >> NC_globatt.inp
      echo $ice4 >> NC_globatt.inp
      echo 'forcing_ice5' >> NC_globatt.inp
      echo $ice5 >> NC_globatt.inp
      if [ -e $path_w/namelists_${mod}.nml ] ; then
        $waverun_path/get_varval.sh $path_w/namelists_${mod}.nml NC_globatt.inp >& $path_o/get_varval_${mod}_${year}${month}.out
      else
        $waverun_path/get_varval.sh $path_w/ww3_grid.inp.$mod NC_globatt.inp >& $path_o/get_varval_${mod}_${year}${month}.out
      fi
    fi

    # create ww3_ounp.inp (from a template or from scratch)
    if [ "$mod" = "$buoys" ] ; then
      prefix="${project}$(echo $mods | cut -d ' ' -f1)"
    else
      prefix="${project}${mod}"
    fi
    if [ "$pt_together" = "T" ] ; then
      prefix="${prefix}_"
    else
      prefix="${prefix}-"
    fi

    if [ -e "$path_d/ww3_ounp.nml.$mod" ]
    then
      echo "   [INFO] copy ww3_ounp.nml.${mod} file from data folder"
      sed -e "s/<timestart>/${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}/" \
          -e "s/<timestride>/$dt_po/" \
          -e "s/<prefix>/${prefix}/" -e "s/<timesplit>/$ndates/" \
          -e "s/<netcdf>/$ncvers/" -e "s/<samefile>/$pt_together/" \
          -e "s/<buffer>/150/" \
          $path_d/ww3_ounp.nml.$mod > ww3_ounp.nml
    elif [ -e "$path_d/ww3_ounp.inp.$mod" ]
    then
      echo "   [INFO] copy ww3_ounp.inp.${mod} file from data folder"
      sed -e "s/OUTPUT_TIME/${startyear}${startmonth}${startday} ${starthour}${startmin}${startsec}  $dt_po  100000000/" \
          -e "s/FILE_PREFIX/${prefix}/" -e "s/NDATES/$ndates/" \
          -e "s/NCVERS/$ncvers/" -e "s/TOGETHER/$pt_together/" \
          -e "s/BUFFER/150/" \
          $path_d/ww3_ounp.inp.$mod > ww3_ounp.inp
    else
      echo "   [INFO] create ww3_ounp.nml file from scratch"
      eval make_ounp_nml $mod
    fi

    # process ww3_ounp
    echo -n "ww3_ounp... "
    echo -n 'start. date: ' && date
    if ! $path_w/ww3_ounp >& $path_o/ww3_ounp.${mod}_${logdate}.out
    then errmsg "Error occured during ww3_ounp";  exit 1; fi
    # if RAM enabled, move to results from RAM to work directory
    if [ $USE_RAM = true ]; then
      find . -type f -name "*.nc" | xargs -n1 -P28 -I% rsync -a % $path_w/SPEC_NC/$mod
      rm *.nc
    fi
    echo -n 'stop. date: ' && date
    unlink mod_def.ww3
    unlink out_pnt.ww3
    if [ -e ww3_ounp.nml ] ; then
      mv ww3_ounp.nml $path_w/ww3_ounp.nml.$mod
    fi
    if [ -e ww3_ounp.nml.log ] ; then
      mv ww3_ounp.nml.log $path_w/ww3_ounp.nml.log.$mod
    fi
    if [ -e ww3_ounp.inp ] ; then
      mv ww3_ounp.inp $path_w/ww3_ounp.inp.$mod
    fi
    mv NC_globatt.inp $path_w/NC_globatt.inp.$mod
    rm -f gmon.out

    #check if some errors occured during process
    $waverun_path/check_error.sh $path_o

    # convert 2D spec into 1D freq
    echo "*** spectofreq_nc on $mod ***"
    cd $path_w
    eval make_spectofreq_nc_inp

    # use RAM if less than 32days to postprocess
    if [ $USE_RAM = true ]; then
      echo "   [INFO] RAM postprocessing enabled"
      mkdir -p $TMPDIR/FREQ_NC/$mod
      cd $TMPDIR/FREQ_NC/$mod
    else
      echo "   [INFO] RAM postprocessing disabled"
      cd $path_w/FREQ_NC/$mod
    fi

    find $path_w/SPEC_NC/$mod/ -name "*spec.nc" > spectofreq_nc.list
    ln -sfn $path_w/spectofreq_nc.inp .
    echo -n "spectofreq_nc... "
    echo -n 'start. date: ' && date
    if ! $path_w/spectofreq_nc >& $path_o/spectofreq_nc_${logdate}.out
    then errmsg "Error occured during spectofreq_nc";  exit 1; fi
    echo -n 'stop. date: ' && date
    # if RAM enabled, move to results from RAM to work directory
    if [ $USE_RAM = true ]; then
      find . -type f -name "*.nc" | xargs -n1 -P28 -I% rsync -a % $path_w/FREQ_NC/$mod
      #rm *.nc
    fi
    unlink spectofreq_nc.inp
    mv fort.* $path_o/
    mv spectofreq_nc.list $path_w/spectofreq_nc.list.$mod

    # split by zones if necessary
    if [ $(cat ${path_d}/${pt_file} | wc -l) -gt 10000 ] ; then
      echo "*** split spec and freq by zones on $mod ***"
      cd $path_w
      for folder in FREQ SPEC
      do
        # clean up
        echo "   [INFO] create $folder directories"
        mkdir -p $PWD/${folder}_AT_BUOYS/$mod && rm -rf $PWD/${folder}_AT_BUOYS/$mod/*
        mkdir -p $PWD/${folder}_SE/$mod && rm -rf $PWD/${folder}_SE/$mod/*
        mkdir -p $PWD/${folder}_SW/$mod && rm -rf $PWD/${folder}_SW/$mod/*
        mkdir -p $PWD/${folder}_NW180to140/$mod && rm -rf $PWD/${folder}_NW180to140/$mod/*
        mkdir -p $PWD/${folder}_NW139to100/$mod && rm -rf $PWD/${folder}_NW139to100/$mod/*
        mkdir -p $PWD/${folder}_NW60to99/$mod && rm -rf $PWD/${folder}_NW60to99/$mod/*
        mkdir -p $PWD/${folder}_NW20to59/$mod && rm -rf $PWD/${folder}_NW20to59/$mod/*
        mkdir -p $PWD/${folder}_NW19toE19/$mod && rm -rf $PWD/${folder}_NW19toE19/$mod/*
        mkdir -p $PWD/${folder}_NE20to59/$mod && rm -rf $PWD/${folder}_NE20to59/$mod/*
        mkdir -p $PWD/${folder}_NE60to99/$mod && rm -rf $PWD/${folder}_NE60to99/$mod/*
        mkdir -p $PWD/${folder}_NE100to139/$mod && rm -rf $PWD/${folder}_NE100to139/$mod/*
        mkdir -p $PWD/${folder}_NE140to180/$mod && rm -rf $PWD/${folder}_NE140to180/$mod/*
        # move
        echo "   [INFO] move files to splitted $folder directories"
        find $PWD/${folder}_NC/$mod -type f -not -name "${prefix}[E,W][0-9][0-9][0-9][0-9][N,S][0-9][0-9][0-9]_*_*.nc" -exec mv {} $PWD/${folder}_AT_BUOYS/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}E[0-9][0-9][0-9][0-9]S[0-9][0-9][0-9]_*_*.nc"         -exec mv {} $PWD/${folder}_SE/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}W[0-9][0-9][0-9][0-9]S[0-9][0-9][0-9]_*_*.nc"         -exec mv {} $PWD/${folder}_SW/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}W1[4-8][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NW180to140/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}W1[0-3][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NW139to100/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}W0[6-9][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NW60to99/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}W0[2-5][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NW20to59/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}W0[0-1][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NW19toE19/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}E0[0-1][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NW19toE19/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}E0[2-5][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NE20to59/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}E1[4-8][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NE140to180/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}E1[0-3][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NE100to139/$mod/ \;
        find $PWD/${folder}_NC/$mod -type f      -name "${prefix}E0[6-9][0-9][0-9]N[0-9][0-9][0-9]_*_*.nc"   -exec mv {} $PWD/${folder}_NE60to99/$mod/ \;
        # clean up
        echo "   [INFO] clean up empty $folder directories"
        find $PWD -type d -empty -delete
      done
    fi

    #check if some errors occured during process
    $waverun_path/check_error.sh $path_o


function make_bounc_nml {

mod="$1"

cat > ww3_bounc.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_bounc.nml - Boundary input post-processing       !
! -------------------------------------------------------------------- !


! -------------------------------------------------------------------- !
! Define the input boundaries to preprocess via BOUND_NML namelist
!
! * namelist must be terminated with /
! * definitions & defaults:
!     BOUND%MODE                 = 'WRITE'            ! ['WRITE'|'READ']
!     BOUND%INTERP               = 2                  ! interpolation [1(nearest),2(linear)]
!     BOUND%VERBOSE              = 1                  ! [0|1|2]
!     BOUND%FILE                 = 'spec.list'        ! input _spec.nc listing file
! -------------------------------------------------------------------- !
&BOUND_NML
     BOUND%FILE                 = 'spec_$mod.list'
/

! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF

chmod 755 ww3_bounc.nml
rm -f spec_$mod.list
find NEST/ -name "*spec.nc" >> spec_$mod.list
}

function make_bounc_inp {

mod="$1"

cat > ww3_bounc.inp << EOF
$ -------------------------------------------------------------------- $
$ WAVEWATCH III Boundary input processing                              $
$ -------------------------------------------------------------------- $
$ boundary option: READ or WRITE
 WRITE
$
$ Interpolation method: 1: nearest
$                       2: linear interpolation
 2
$
$ verbose mode [0-1]
 0
$
$ list of spectra files:
$ list of spectra files. These netCDF files use the WAVEWATCH III format
$ examples of such files can be found at (for example):
$   http://tinyurl.com/iowagaftp/HINDCAST/NORGASUG/2010_ECMWF/SPEC_NC
$
EOF
  find NEST/ -name "*spec.nc" >> ww3_bounc.inp
  cat >> ww3_bounc.inp << EOF
$
'STOPSTRING'
$
$ -------------------------------------------------------------------- $
$ End of input file                                                    $
$ -------------------------------------------------------------------- $
EOF

chmod 775 ww3_bounc.inp
}


    # prepare nest.ww3 for subgrids
    for nestdir in $nests_ww3
    do
      echo "*** ww3_bounc on $nestdir ***"
      mod1=$(cat $run_path/$nestdir/wavesetup.env | grep 'mods=' | cut -d "'" -f2 | cut -d ' ' -f1)
      # get the list of spectral boundaries
      if [ -e $run_path/$nestdir/data/spec_$mod1.list ]
      then
        list_spec=$(cat $run_path/$nestdir/data/spec_$mod1.list | grep '\_spec.nc')
      elif [ -e $run_path/$nestdir/data/ww3_bounc.inp.$mod1 ]
      then
        list_spec=$(cat $run_path/$nestdir/data/ww3_bounc.inp.$mod1 | grep '\_spec.nc')
      else
        echo "  [ERROR] spec_zone is activated but no ww3_bounc.inp.$mod1 is found in $run_path/$nestdir/data"
        exit 1
      fi
      mkdir -p $work_path/$nestdir/work/NEST && rm -rf $work_path/$nestdir/work/NEST/*
      # concatenate all daily spec files
      for specfile in $list_spec
      do
        specfile=$(basename $specfile)
        specfileall=$(echo $specfile | sed -e s/DATERUN/*/ | sed -e s/\<daterun\>/*/ | sed -e s/\<project\>/$project/)
        specfilelog=$(echo $specfile | sed -e s/DATERUN/${logdate}/ | sed -e s/\<daterun\>/${logdate}/ | sed -e s/\<project\>/$project/)
        ncrcat -h $path_w/SPEC_NC/${buoys}/${specfileall}  $work_path/$nestdir/work/NEST/${specfilelog}
      done

      cd $work_path/$nestdir/work
      rm -f nest.ww3

      echo "   [INFO] create ww3_bounc.nml file from scratch"
      eval make_bounc_nml $mod1

      ln -sfn mod_def.$mod1 mod_def.ww3
      rm -f ww3_bounc && cp $path_e/ww3_bounc . && chmod 775 ww3_bounc
      echo -n "ww3_bounc... "
      echo -n 'start. date: ' && date
      if ! ./ww3_bounc >& $path_o/ww3_bounc_${nestdir}_${logdate}.out
      then errmsg "Error occured during ww3_bounc";  exit 1; fi
      echo -n 'stop. date: ' && date
    done

    #check if some errors occured during process
    $waverun_path/check_error.sh $path_o

  else
    echo "[INFO] dt_po=0 or no out_pnt.$mod file found"
  fi

done # mod



# define a symbolic link for the result directory
ln -sfn $path_w $main_dir/result
if [ -e $main_dir/date_cycle ] ; then
  echo "*** set symbolic link to result-$(date -u +%Y%m%d)T$hour ***"
  ln -sfn $path_w $work_path/$run_tag/result-$(date -u +%Y%m%d)T$cyclehour
  ln -sfn $path_w $work_path/result-$(date -u +%Y%m%d)T$cyclehour
fi


echo "!END OF POSTPRO!"
date




