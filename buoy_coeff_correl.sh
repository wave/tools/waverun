#!/bin/bash -e

# Checks the correlation coefficients in the SUMMARY output file from process_correl.sh
#
# Structure of the file to read :
#
# Results of correl for hs
#   BuoyID  Year  Month  Day     Lat     Lon   Mean_mod   Mean_obs  Scat_mod  Scat_obs  Coef_cor  Nrms_err
# WMO13130  2012      1    1   28.19  -15.81     1.4192     1.5690    0.4430    0.4335    0.9481   0.1265
# ...
#

# input arguments
if [ $# -lt 5 ]
then
  echo 'need the following arguments :'
  echo '$1 : year [YYYY]'
  echo '$2 : monthlist (comma-separated values)'
  echo '$3 : variable name [ hs | dir | wnd | wnddir]'
  echo '$4 : value below which the coefficient is considered bad [0-1]'
  echo '$5 : directory where to find the Results_correl_${var}.out file'
  exit
fi

year="$1"
if [ "$2" = "all" ]
then
  monthlist="01,02,03,04,05,06,07,08,09,10,11,12"
else
  monthlist="$2"
fi
var="$3"
min_value="$4" # minimum value accepted for the coefficient of correlation
path="$5"


for month in $(echo "$monthlist" | tr ',' '\n')
do
  filename="${path}/results_correl_${year}${month}_${var}.out"

  echo "List of buoys for which the correlation coefficient is < $min_value :" > ${path}/coef_corr_below_${min_value}_for_${var}_${year}${month}.out

  cat $filename | while read line; do
  #  echo $line

    if [ "${line:0:3}" = "WMO" ]
    then
      buoyID=$(echo $line | awk -F' ' '{print $1}')
#      year=$(echo $line | awk -F' ' '{print $2}')
#      month=$(echo $line | awk -F' ' '{print $3}')
#      month=$(printf "%2d\n" $month)
      day=$(echo $line | awk -F' ' '{print $4}')
      day=$(printf "%2d\n" $day)

      coef_cor=$(echo $line | awk -F' ' '{print $11}')    
      coef_cor=$(printf "%.4f\n" $coef_cor)

      restmp=$(echo "$coef_cor < $min_value" | bc)
      if [ $restmp -eq 1 ]
      then
        echo "$buoyID $year $month $day : $coef_cor" >> ${path}/coef_corr_below_${min_value}_for_${var}_${year}${month}.out
      fi
    fi
  done

  nb_lines=$(cat $filename | wc -l)
  nb_bad_coeff=$(cat ${path}/coef_corr_below_${min_value}_for_${var}_${year}${month}.out | wc -l)
  if [ $nb_lines -gt 2 ]
  then
    nb_lines=$(($nb_lines - 2))
    nb_bad_coeff=$(($nb_bad_coeff - 1))
    echo "$nb_bad_coeff correlation coefficients < $min_value out of $nb_lines buoys for month $month"
  else
    echo "no buoy results found in $filename for month $month"
  fi

done # Loop on months
