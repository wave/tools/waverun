#!/bin/bash -e
#PBS -J 1-256

# -------------------------------------------------------------------- #
# Waverun toolbox : process copy over multi threading                  #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : GPLv3                                                      #
# Creation date : 01-Dec-2011                                          #
# Modification date : 07-Jun-2021                                      #
# -------------------------------------------------------------------- #

#----------------------
## 1. INPUT ARGUMENTS
#----------------------

if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  max_thread=256
else
  max_thread=1
fi

#----------------------
## 2. ENVIRONMENT
#----------------------

# pbs environment
if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  cd $PBS_O_WORKDIR
fi

# wavepath environment
if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
else  echo 'no wavepath.env found in $PWD or $HOME' ; ls $PWD; exit 1 ; fi
echo "source $wavepath" && source $wavepath

# wavesetup environment
echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env

# Error message function
errmsg ()
{
  echo "" 2>&1
  while [ $# != 0 ]
  do
    echo "ERROR: $1" 2>&1
    shift
  done
  echo "" 2>&1
}


#----------------------
## 3. INIT & CHECK
#----------------------

echo "PBS_ARRAY_INDEX : $PBS_ARRAY_INDEX"
output=$PWD/OUTPUT_PBS/$PBS_ARRAY_INDEX
mkdir -p $output && rm -f $output/*
tag=${run_tag:0:6}


#----------------------
## 4. PROGRAM
#----------------------

cd lastwork/$run_tag 

i=0
for file in $(find -L . -mindepth 1 -type f | sort -n)
do
  i=$(($i + 1))
  if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
    echo "rsync -aL --rsh=ssh $file $path_a/${run_tag}/$(dirname $file)"
    echo -n "rsync... "
    if ! rsync -aL --rsh=ssh $file $path_a/${run_tag}/$(dirname $file)
    then errmsg "Error occured during rsync";  exit 1;
    else echo "OK"; fi
  fi
done

if [ $PBS_ARRAY_INDEX -eq 1 ] ; then
  for thread in $(seq 2 $max_thread)
  do
    logfile=$PBS_O_WORKDIR/${tag}sync.o$(echo $PBS_JOBID | cut -d '[' -f1).$thread
    while [ ! -e $logfile ]
    do
      echo "no logfile for thread $thread, sleep"
      sleep 10
    done
  done

  logfiles=$PBS_O_WORKDIR/${tag}sync.o$(echo $PBS_JOBID | cut -d '[' -f1)
  mainlog="$(echo $PBS_JOBID | cut -d '[' -f1)[]"
  if [ ! -z "$(qstat -f $mainlog | grep 'Subjob failed')" ] ; then
    echo "[ERROR] a subjob has failed"
    echo "tail -n3 ${logfiles}*"
    rm -rf $path_a/${run_tag}
    exit 1
  fi

  for thread in $(seq 2 $max_thread)
  do
    logfile=$PBS_O_WORKDIR/${tag}sync.o$(echo $PBS_JOBID | cut -d '[' -f1).$thread
    if [ ! -z "$(grep -ai 'error' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'No Such ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'SIGSEGV ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'illegal ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'Permission ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'cannot ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'exceeds ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'severe ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'killed ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'abort ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'does not exist ' ${logfile})" ] || \
       [ ! -z "$(grep -ai 'aborting ' ${logfile})" ] ; then

      echo "[ERROR] a subjob has an error message"
      echo "tail -n3 ${logfile}"
      rm -rf $path_a/${run_tag}
      exit 1
    fi
    rm $logfile
  done

  echo '*** Compare the content of the source and destination directories before removing the source ***'
  echo ""
  echo "cd $main_dir/lastwork/$run_tag && find -L . | sort -n > sync.list"
  cd $main_dir/lastwork/$run_tag && find -L . | sort -n > sync.list
  echo "cd $path_a/${run_tag} && find . | sort -n > sync.list"
  cd $path_a/${run_tag} && find . | sort -n > sync.list
  echo " diff $main_dir/lastwork/$run_tag/sync.list $path_a/${run_tag}/sync.list"
  diff $main_dir/lastwork/$run_tag/sync.list $path_a/${run_tag}/sync.list
  if [ -z "$(diff $main_dir/lastwork/$run_tag/sync.list $path_a/${run_tag}/sync.list)" ]; then
    rm $main_dir/lastwork/$run_tag/sync.list
    rm $path_a/${run_tag}/sync.list
    echo " no differences found."
    rm -rf $main_dir/work/* $main_dir/lastwork/*
    #find $path_a/${run_tag}/ -type d -exec chmod 775 -R {} \; ;
  else
    echo " differences found. exit with error status"
    rm -rf $path_a/${run_tag}
    exit 1
  fi
fi
