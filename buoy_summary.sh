#!/usr/bin/sh -e

# Plot correlation maps with the output files of process_correl.sh
#
# Structure of the files to read :
#
#  input model file :
#  PATH/ww3.BUOYID_YYYYMM_freq.nc
#  input observation file :
#  PATH_GLOBWAVE/WMO${BUOYID}_DATEbegin_DATEend_Lat_XXX_Lon_XXX.nc
# mean mod : X.XXXXm (Mean Hs for model)
# mean obs : X.XXXXm (Mean Hs for observations)
# scat mod : X.XXXXm 
# scat obs : X.XXXXm
# coef cor : X.XXXXm
# nrms err : X.XXXXm
#  output result file :
#  PATH/corr_mod2obs_$BUOYID_YYYYMM.nc
#
#

# input arguments
if [ $# -lt 4 ]
then
  echo 'need 4 arguments :'
  echo '$1 : year [YYYY]'
  echo '$2 : monthlist (comma-separated values)'
  echo '$3 : variable list [hs|wnd] (comas separated values)'
  echo '$2 : BUOY directory'
  exit
fi

# directory names for the variables	
year="$1"
if [ "$2" = "all" ]
then
  monthlist="01,02,03,04,05,06,07,08,09,10,11,12"
else
  monthlist="$2"
fi
varlist="$3"
buoydir="$4"


cd $buoydir


for var in $( echo "$varlist" | tr ',' '\n')
do
  cd $var
  echo `pwd`
  for month in $(echo "$monthlist" | tr ',' '\n')
  do
    rm -f ${buoydir}/results_correl_${year}${month}_${var}.out
    printf "results of correl in ${year}${month} for $var \n" > ${buoydir}/results_correl_${year}${month}_${var}.out
    printf "%8s  %4s  %4s  %3s  %6s  %6s  %9s  %9s  %8s  %8s  %8s  %8s\n" "BuoyID" "Year" "Month" "Day" "Lat" "Lon" "Mean_mod" "Mean_obs" "Scat_mod" "Scat_obs" "Coef_cor" "Nrms_err" >> ${buoydir}/results_correl_${year}${month}_${var}.out
    list_file=$(find . -name "correl*_${year}${month}??T*.out")
    for file in ${list_file}
    do
      # Initialize variables
      mean_mod=""
      mean_obs=""
      scat_mod=""
      scat_obs=""
      coef_cor=""
      nrms_err=""
      print="yes"

      buoyID="$(basename $file | awk -F'_' '{print $2}')"
      buoyID=$(printf "%8s" $buoyID)
#      year="$(basename $file | awk -F'_' '{print $3}' | cut -c1-4)"
#      year=$(printf "%4d" $year)
#      month="$(basename $file | awk -F'_' '{print $3}' | cut -c5-6)"
#      month=$(printf "%2d" "${month#0}")
      day="$(basename $file | awk -F'_' '{print $5}' | cut -c7-8)"
      day=$(printf "%2d" "${day#0}")
      lat="$(basename $file | awk -F'_' '{print $6}')"
      lon="$(basename $file | awk -F'_' '{print $8}' | awk -F'.nc' '{print $1}')"
      echo -n "buoyID : $buoyID -- "
  #    echo "year : $year"
  #    echo "month : $month"
  #    echo "day : $day"
  #    echo "lat : $lat"
  #    echo "lon : $lon"

      # Re-format latitudes & longitudes
      if [ ! -z "$(echo $lat | grep N)" ]
      then
        lat="${lat%?}"
        lat=$(printf "%6.2f" $lat)
      elif [ ! -z "$(echo $lat | grep S)" ]
      then
        lat=-"${lat%?}"
        lat=$(printf "%6.2f" $lat)
      else
        echo "problem: latitude should be N or S"
        echo "latitude: $lat"
      fi
      if [ ! -z "$(echo $lon | grep E)" ]
      then
        lon="${lon%?}"
        lon=$(printf "%6.2f" $lon)
      elif [ ! -z "$(echo $lon | grep W)" ]
      then
        lon=-"${lon%?}"
        lon=$(printf "%6.2f" $lon)
      else
        echo "problem: longitude should be E or W"
        echo "longitude : $lon"
      fi

      # Read current file
      while read line; do
        if [ "$line" = "NetCDF: Variable not found" ]
        then
          echo "[WARNING] NetCDF: Variable not found -> line skipped."
          print="no"
        fi
        if [ "$line" = "[WARNING] not enough valid data to make correlation." ]
        then
          echo "[WARNING] Not enough data for buoy $buoyID at month $month -> line skipped."
          print="no"
        fi
        if [ "${line:0:10}" = "mean mod :" ]
        then
          mean_mod=$(echo $line | awk -F' ' '{print $4}' | cut -c1-6)    
          mean_mod=$(printf "%.4f\n" $mean_mod)
#          echo "mean_mod :" $mean_mod
        fi
        if [ "${line:0:10}" = "mean obs :" ]
        then
          mean_obs=$(echo $line | awk -F' ' '{print $4}' | cut -c1-6)    
          mean_obs=$(printf "%.4f\n" $mean_obs)
#          echo "mean_obs :" $mean_obs
        fi
        if [ "${line:0:10}" = "scat mod :" ]
        then
          scat_mod=$(echo $line | awk -F' ' '{print $4}' | cut -c1-6)    
          scat_mod=$(printf "%.4f\n" $scat_mod)
#          echo "scat_mod :" $scat_mod
        fi
        if [ "${line:0:10}" = "scat obs :" ]
        then
          scat_obs=$(echo $line | awk -F' ' '{print $4}' | cut -c1-6)    
          scat_obs=$(printf "%.4f\n" $scat_obs)
#          echo "scat_obs :" $scat_obs
        fi
        if [ "${line:0:10}" = "coef cor :" ]
        then
          coef_cor=$(echo $line | awk -F' ' '{print $4}')    
          coef_cor=$(printf "%.4f\n" $coef_cor)
#          echo "coef_cor :" $coef_cor
        fi
        if [ "${line:0:10}" = "nrms err :" ]
        then
          nrms_err=$(echo $line | awk -F' ' '{print $4}' | cut -c1-6)
          if [ "$nrms_err" != 'Infini' ]
          then    
            nrms_err=$(printf "%.4f\n" $nrms_err)
#           echo "nrms_err :" $nrms_err
          else
            echo "[WARNING] NRMS err for buoy $buoyID at month $month is Infinity"
            print='no'
          fi
        fi

      done < $file # line in $file
      echo "print : $print"
      if [ $print = "yes" ]
      then
  #      echo "$buoyID  $year  $month  $day  $lat  $lon  $mean_mod  $mean_obs  $scat_mod  $scat_obs  $coef_cor  $nrms_err"
        printf "%8s  %4d  %02d  %3d  %6.2f  %6.2f  %9.4f  %9.4f  %8.4f  %8.4f  %8.4f %8.4f\n" $buoyID  $year  ${month#0}  $day  $lat  $lon  $mean_mod  $mean_obs  $scat_mod  $scat_obs  $coef_cor  $nrms_err >> ${buoydir}/results_correl_${year}${month}_${var}.out
      fi

    done # file in $list_file
    nlines="$(wc -l ${buoydir}/results_correl_${year}${month}_${var}.out | cut -d ' ' -f1)"
    nlines=$(($nlines - 2))
    echo ""
    echo "[INFO] There are $nlines 'good' buoy files for month $month, for which correlation coefficients and NRMS err for $var were successfully calculated"
    echo ""
  done # month in $monthlist
  cd ..
done # var in $varlist


echo "*** END OF PROGRAM ***"

