#!/bin/bash -e

# -------------------------------------------------------------------- #
# Waverun toolbox : launcher                                           #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : CC BY-SA                                                   #
# Creation date : 01-Dec-2011                                          #
# Modification date : 20-Sep-2023                                      #
# -------------------------------------------------------------------- #

# wavepath environment
if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
else  echo 'no wavepath.env found in $PWD or $HOME' ; exit ; fi
echo ''
echo '*** load path ***'
echo "source $wavepath" && source $wavepath

# additional environment
if [ ! -z "$(echo $WAVERUN_PROFIL)" ] ; then
  echo ''
  echo '*** load profil ***'
  echo "source $WAVERUN_PROFIL" && source $WAVERUN_PROFIL
else
  WAVERUN_PROFIL=''
  email=''
fi

# wavesetup environment
echo ''
echo '*** load setup ***'
echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env




#startrun='20170314 000000'
#stoprun='20170315 000000'
#startrun="$(date -d '-12 hours' +%Y%m%d) 000000"
#stoprun="$(date -d '+ 5 days'  +%Y%m%d) 000000"

if [ $# -lt 2 ] ; then
  echo "need two arguments :"
  echo '  $1 : startdate [20161001 000000]'
  echo '  $2 : stopdate  [20161015 000000]'
  echo ''
  echo 'options available : (default value is yes for all except archive)'
  echo '-forc=no  : skip forcing'
  echo '-hind=no  : skip hindcast'
  echo '-post=no  : skip postpro'
  echo '-buoy=no  : skip buoy valid'
  echo '-sat=no  : skip sat valid'
  echo '-arch=yes : activate archiving'
  echo '-best=yes : activate best estim'
  echo ''
  echo '-forc=only : run only forcing'
  echo '-hind=only : run only hindcast'
  echo '-post=only : run only postpro'
  echo '-buoy=only : run only buoy valid'
  echo '-sat=only : run only sat valid'
  echo '-arch=only : run only archiving'
  echo '-best=only : run only best estim'
  exit
fi

# parse arguments
startrun="$1"
echo ''
echo '*** load date ***'
echo "startrun = $startrun"
stoprun="$2"
echo "stoprun = $stoprun"
forc='yes'; hind='yes'; post='yes'; buoy='yes'; sat='yes'; arch='no'; best='no'
while echo $3 | grep ^- > /dev/null 
do declare $( echo $3 | sed 's/-//g' | sed 's/=.*//g' | tr -d '\012')=$( echo $3 | sed 's/.*=//g' | tr -d '\012'); shift
done

if   [ $best == 'only' ] ; then
  forc='no'; hind='no'; post='no'; buoy='no'; sat='no'; arch='no'; best='yes'
elif [ $arch == 'only' ] ; then
  forc='no'; hind='no'; post='no'; buoy='no'; sat='no'; arch='yes'; best='no'
elif [ $sat == 'only' ] ; then
  forc='no'; hind='no'; post='no'; buoy='no'; sat='yes'; arch='no'; best='no'
elif [ $buoy == 'only' ] ; then
  forc='no'; hind='no'; post='no'; buoy='yes'; sat='no'; arch='no'; best='no'
elif [ $post == 'only' ] ; then
  forc='no'; hind='no'; post='yes'; buoy='no'; sat='no'; arch='no'; best='no'
elif [ $hind == 'only' ] ; then
  forc='no'; hind='yes'; post='no'; buoy='no'; sat='no'; arch='no'; best='no'
elif [ $forc == 'only' ] ; then
  forc='yes'; hind='no'; post='no'; buoy='no'; sat='no'; arch='no'; best='no'
fi

echo ''
echo '*** load process ***'
echo "forc = $forc"
echo "hind = $hind"
echo "post = $post"  
echo "buoy = $buoy"
echo "sat = $sat"
echo "arch = $arch"
echo "best = $best"


startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -u)"

stopyear="$(echo $stoprun | cut -c1-4)"
stopmonth="$(echo $stoprun | cut -c5-6)"
stopday="$(echo $stoprun | cut -c7-8)"
stophour="$(echo $stoprun | cut -c10-11)"

logdate="${startyear}-${startmonth}-${startday}T${starthour}_${stopyear}-${stopmonth}-${stopday}T${stophour}"

path_output="$HOME/RUNS/OUTPUT"
mkdir -p $path_output

lastwork="${work_path}/${run_tag}/work${logdate}"

tag=${run_tag:0:6}

###########
# FORCING #
###########
if [ $forc == 'yes' ] ; then
  echo '' && echo "*** run forcing from $startrun to $stoprun ***"
  cd $main_dir
  rm -f $path_o/ww3_shel*out $path_o/ww3_multi*out $path_o/xml*.out
  rm -f $path_o/ww3_ounf*out $path_o/ww3_ounp*out
  rm -f $path_output/${tag}forc_${logdate}.out
  echo "qsub -h -m ae -v startrun=${startrun},stoprun=${stoprun},WAVERUN_PROFIL=${WAVERUN_PROFIL} -N ${tag}forc -q mpi_1 -l select=1:ncpus=28:mpiprocs=28:mem=80G -l walltime=24:00:00 $waverun_path/process_forcing.sh >& $path_output/${tag}forc_${logdate}.out"
  qsub -h $email -m ae -v startrun="'${startrun}'",stoprun="'${stoprun}'",WAVERUN_PROFIL="'${WAVERUN_PROFIL}'" \
       -N ${tag}forc -q mpi_1 \
        -l select=1:ncpus=28:mpiprocs=28:mem=80G -l walltime=24:00:00 \
        $waverun_path/process_forcing.sh >& $path_output/${tag}forc_${logdate}.out
  jobid_forc=":$(cat $path_output/${tag}forc_${logdate}.out)"
else
  jobid_forc=""
fi

############
# HINDCAST #
############
if [ ! -z "$(echo $jobid_forc)" ] ; then
  depends="-W depend=afterok$jobid_forc"
elif [ $hind == 'yes' ] ; then
  depends="-h"
fi

if [ $hind == 'yes' ] ; then
  echo ''
  echo "*** run hindcast from $startrun to $stoprun ***"
  cd $main_dir
  rm -f $path_w/xml/log*xml
  rm -f $path_o/ww3_shel*out $path_o/ww3_multi*out $path_o/xml*.out
  rm -f $path_o/ww3_ounf*out $path_o/ww3_ounp*out $path_o/fill*out
  rm -f $path_output/${tag}hind_${logdate}.out
  echo "qsub -m ae $depends -v startrun=${startrun},stoprun=${stoprun},WAVERUN_PROFIL=${WAVERUN_PROFIL} -N ${tag}hind -q mpi_4 -l select=4:ncpus=28:mpiprocs=28:mem=115G -l walltime=48:00:00 $waverun_path/process_hindcast.sh >& $path_output/${tag}hind_${logdate}.out"
  qsub $email -m ae $depends \
       -v startrun="'${startrun}'",stoprun="'${stoprun}'",WAVERUN_PROFIL="'${WAVERUN_PROFIL}'" \
       -N ${tag}hind -q mpi_4 \
       -l select=4:ncpus=28:mpiprocs=28:mem=115G -l walltime=48:00:00 \
        $waverun_path/process_hindcast.sh >& $path_output/${tag}hind_${logdate}.out
  jobid_hind=":$(cat $path_output/${tag}hind_${logdate}.out)"
else
  jobid_hind=""
fi


###########
# POSTPRO #
###########
if [ ! -z "$(echo $jobid_forc$jobid_hind)" ] ; then
  depends="-W depend=afterok$jobid_forc$jobid_hind"
elif [ $post == 'yes' ] ; then
  depends="-h"
fi

if [ $post == 'yes' ] ; then
  echo '' && echo "*** run postpro from $startrun to $stoprun ***"
  cd $main_dir
  rm -f $path_o/ww3_ounf*out $path_o/ww3_ounp*out $path_o/ww3_gint*out $path_o/fill*out $path_o/interpolate*out $path_o/spectofreq*out
  rm -f $path_output/${tag}post_${logdate}.out
  echo "qsub -m ae $depends -v startrun=${startrun},stoprun=${stoprun},WAVERUN_PROFIL=${WAVERUN_PROFIL} -N ${tag}post -l mem=115G -l walltime=48:00:00 $waverun_path/process_postpro.sh >& $path_output/${tag}post_${logdate}.out"
  qsub $email -m ae $depends \
       -v startrun="'${startrun}'",stoprun="'${stoprun}'",WAVERUN_PROFIL="'${WAVERUN_PROFIL}'" \
       -N ${tag}post \
       -l mem=115G -l walltime=48:00:00 \
        $waverun_path/process_postpro.sh >& $path_output/${tag}post_${logdate}.out
  jobid_post=":$(cat $path_output/${tag}post_${logdate}.out)"
else
  jobid_post=""
fi

#################
# VALID STATION #
#################
if [ ! -z "$(echo $jobid_forc$jobid_hind$jobid_post)" ] ; then
  depends="-W depend=afterok$jobid_forc$jobid_hind$jobid_post"
elif [ $buoy == 'yes' ] ; then
  depends="-h"
fi

if [ $buoy == 'yes' ] ; then
  echo '' && echo "*** run buoy from $startrun to $stoprun ***"
  cd $main_dir
  rm -f $path_output/${tag}buoy_${logdate}.out
  echo "qsub -m ae $depends -v startrun=${startrun},stoprun=${stoprun},WAVERUN_PROFIL=${WAVERUN_PROFIL},max_thread=200 -N ${tag}buoy -l mem=20G -l walltime=04:00:00 $waverun_path/process_station.sh >& $path_output/${tag}buoy_${logdate}.out"
  qsub $email -m ae $depends \
       -v startrun="'${startrun}'",stoprun="'${stoprun}'",WAVERUN_PROFIL="'${WAVERUN_PROFIL}'",max_thread=200 \
       -N ${tag}buoy -l mem=20G -J 1-200 -l walltime=04:00:00 \
        $waverun_path/process_station.sh >& $path_output/${tag}buoy_${logdate}.out
  jobid_buoy=":$(cat $path_output/${tag}buoy_${logdate}.out)"
else
  jobid_buoy=""
fi

#############
# VALID SAT #
#############
if [ ! -z "$(echo $jobid_forc$jobid_hind$jobid_post)" ] ; then
  depends="-W depend=afterok$jobid_forc$jobid_hind$jobid_post"
elif [ $sat == 'yes' ] ; then
  depends="-h"
fi

if [ $sat == 'yes' ] ; then
  echo '' && echo "*** run sat from $startrun to $stoprun ***"
  cd $main_dir
  rm -f $path_output/${tag}sat_${logdate}.out
  rm -rf $lastwork/SAT
  echo "qsub -m ae $depends -v startrun=${startrun},stoprun=${stoprun},WAVERUN_PROFIL=${WAVERUN_PROFIL} -N ${tag}sat -l mem=40G -J 1-10 -l walltime=04:00:00 $waverun_path/process_sat.sh >& $path_output/${tag}sat_${logdate}.out"
  qsub $email -m ae $depends \
       -v startrun="'${startrun}'",stoprun="'${stoprun}'",WAVERUN_PROFIL="'${WAVERUN_PROFIL}'" \
       -N ${tag}sat -l mem=40G -J 1-10 -l walltime=04:00:00 \
        $waverun_path/process_sat.sh >& $path_output/${tag}sat_${logdate}.out
  jobid_sat=":$(cat $path_output/${tag}sat_${logdate}.out)"
else
  jobid_sat=""
fi

##############
# ARCHIVING  #
##############
if [ ! -z "$(echo $jobid_forc$jobid_hind$jobid_post$jobid_buoy$jobid_sat)" ] ; then
  depends="-W depend=afterok$jobid_forc$jobid_hind$jobid_post$jobid_buoy$jobid_sat"
elif [ $arch == 'yes' ] ; then
  depends="-h"
fi

if [ $arch == 'yes' ] ; then
  echo '' && echo "*** run archive from $startrun to $stoprun ***"
  cd $main_dir
  rm -f $path_output/${tag}pack_${logdate}.out
  rm -f $path_output/${tag}sync_${logdate}.out
  rm -rf $lastwork/${run_tag}
  echo "qsub -m ae $depends -v startrun=${startrun},stoprun=${stoprun},WAVERUN_PROFIL=${WAVERUN_PROFIL} -N ${tag}pack -l mem=8G -l walltime=02:00:00 $waverun_path/process_pack.sh >& $path_output/${tag}pack_${logdate}.out"
  qsub $email -m ae $depends \
       -v startrun="'${startrun}'",stoprun="'${stoprun}'",WAVERUN_PROFIL="'${WAVERUN_PROFIL}'" \
       -N ${tag}pack -l mem=8G -l walltime=02:00:00 \
        $waverun_path/process_pack.sh >& $path_output/${tag}pack_${logdate}.out
  jobid_pack=":$(cat $path_output/${tag}pack_${logdate}.out)"

  depends="-W depend=afterok$jobid_forc$jobid_hind$jobid_post$jobid_buoy$jobid_sat$jobid_pack"
  echo "qsub -m ae $depends -N ${tag}sync -lmem=12G -l walltime=06:00:00 $waverun_path/process_sync.sh >& $path_output/${tag}sync_${logdate}.out"
  qsub $email -m ae $depends \
       -N ${tag}sync -lmem=12G -l walltime=06:00:00 \
        $waverun_path/process_sync.sh >& $path_output/${tag}sync_${logdate}.out
  jobid_sync=":$(cat $path_output/${tag}sync_${logdate}.out)"
else
  jobid_pack=""
  jobid_sync=""
fi


###############
# BEST ESTIM  #
###############
if [ ! -z "$(echo $jobid_forc$jobid_hind$jobid_post)" ] ; then
  depends="-W depend=afterok$jobid_forc$jobid_hind$jobid_post"
elif [ $best == 'yes' ] ; then
  depends="-h"
fi

if [ $best == 'yes' ] ; then
  echo '' && echo "*** run best estim from $startrun to $stoprun ***"
  cd $main_dir
  rm -f $path_output/${tag}best_${logdate}.out
  echo "qsub -m ae $depends -v startrun=${startrun},stoprun=${stoprun},WAVERUN_PROFIL=${WAVERUN_PROFIL} -N ${tag}best -l mem=20G -l walltime=04:00:00 $waverun_path/process_best.sh >& $path_output/${tag}best_${logdate}.out"
  qsub $email -m ae $depends \
       -v startrun="'${startrun}'",stoprun="'${stoprun}'",WAVERUN_PROFIL="'${WAVERUN_PROFIL}'" \
       -N ${tag}best -l mem=20G -l walltime=04:00:00 \
        $waverun_path/process_best.sh >& $path_output/${tag}best_${logdate}.out
  jobid_best=":$(cat $path_output/${tag}best_${logdate}.out)"
else
  jobid_best=""
fi








###############
# RELEASE JOB #
###############
if [ $forc == 'yes' ] ; then
  qrls ${jobid_forc:1}
elif [ $hind == 'yes' ] ; then
  qrls ${jobid_hind:1}
elif [ $post == 'yes' ] ; then
  qrls ${jobid_post:1}
elif [ $buoy == 'yes' ] ; then
  qrls ${jobid_buoy:1}
elif [ $sat == 'yes' ] ; then
  qrls ${jobid_sat:1}
elif [ $arch == 'yes' ] ; then
  qrls ${jobid_pack:1}
  qrls ${jobid_sync:1}
elif [ $best == 'yes' ] ; then
  qrls ${jobid_best:1}
fi

# show the jobs list
echo ''
echo '*** show jobs ***'
qstat -u $USER
echo ''

