#!/bin/bash -e

# -------------------------------------------------------------------- #
# Waverun toolbox : process model hindcast or forecast                 #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : GPLv3                                                      #
# Creation date : 01-Dec-2011                                          #
# Modification date : 07-Jun-2021                                      #
# -------------------------------------------------------------------- #


###########################################################################################
#   process hindcast for a selected time span defined in argument                         #
#                                                                                         #
# need to be in main_dir to execute the script                                            #
#                                                                                         #
# example :                                                                               #
#  qsub -v startrun='20161101 000000',stoprun='20161201 000000'                           #
#       -q mpi_4 -l mem=20G -l walltime=24:00:00                                          #
#       -N GLOBhind ~/TOOLS/SCRIPTS/HINDCAST/process_hindcast.sh                          #
#                                                                                         #
# additional bashrc file can be sourced if defined by $WAVERUN_PROFIL (with -v for qsub)  #
###########################################################################################


#----------------------
## 1. INPUT ARGUMENTS
#----------------------

# shell arguments
if [ $# -ge 2 ] ; then
  startrun=$1  # '20170314 000000'
  stoprun=$2   # '20170320 000000'
fi

# qsub arguments
if [ -z "$(echo $startrun)" ] || [ -z "$(echo $stoprun)" ]; then
  echo ''
  echo '[ERROR] missing startrun,stoprun arguments in qsub command'
  echo "[EXAMPLE] qsub -v startrun='20161101 000000',stoprun='20161201 000000' process_hindcast.sh"
  exit 1
fi

# start and stop dates
echo "startrun: $startrun"
echo "stoprun: $stoprun"

startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -uR)"

stopyear="$(echo $stoprun | cut -c1-4)"
stopmonth="$(echo $stoprun | cut -c5-6)"
stopday="$(echo $stoprun | cut -c7-8)"
stophour="$(echo $stoprun | cut -c10-11)"
stopmin="$(echo $stoprun | cut -c12-13)"
stopsec="$(echo $stoprun | cut -c14-15)"
stoprunformat="$(date -d "${stopyear}-${stopmonth}-${stopday} ${stophour}:${stopmin}:${stopsec}" -uR)"



#----------------------
## 2. ENVIRONMENT
#----------------------

# pbs environment
if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  cd $PBS_O_WORKDIR
fi

# wavepath environment
if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
else  echo 'no wavepath.env found in $PWD or $HOME' ; ls $PWD; exit 1 ; fi
echo "source $wavepath" && source $wavepath

# additional environment
if [ ! -z "$(echo $WAVERUN_PROFIL)" ] ; then
  echo "source $WAVERUN_PROFIL" && source $WAVERUN_PROFIL
fi

# mpi environment
if [ -z "$(echo $MPI_LAUNCH)" ] ; then
  MPI_LAUNCH=mpiexec
fi
echo "MPI_LAUNCH : $(which $MPI_LAUNCH)"

# wavesetup environment
echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env

# Error message function
errmsg ()
{
  echo "" 2>&1
  while [ $# != 0 ]
  do
    echo "ERROR: $1" 2>&1
    shift
  done
  echo "" 2>&1
}


#----------------------
## 3. INIT & CHECK
#----------------------

# check wavesetup.env
$waverun_path/check_setup.sh $main_dir

# set dates
startout=$startrun
logdate="${startyear}-${startmonth}-${startday}T${starthour}_${stopyear}-${stopmonth}-${stopday}T${stophour}"

# check run dates consistency
if [ "$startyear" == "$stopyear" ] ; then
  year=$startyear
else
  if [ -e $main_dir/date_cycle ] ; then
    echo "[INFO] no check done on overlapping years for forecast system"
    year=$stopyear
  else
    if [ $(expr $stopyear - $startyear) -gt 2 ] ; then
      echo "   [ERROR] It is not possible to run over more than one year. Reduce the duration."
      exit 1
    elif [ $(expr $stopyear - $startyear) -eq 2 ] || [ $startmonth -le 11 ] ; then
      if [ $stopmonth -ne 1 ] || [ $stopday -ne 1 ] ; then
        echo "   [ERROR] It is not possible to run over two different years. Reduce the duration."
        exit 1
      fi
    fi
    # set year for forcing
    if [ $stopmonth -eq 1 ] && [ $stopday -eq 1 ] ; then
      year=$(expr $stopyear - 1)
    else
      year=$stopyear
    fi
  fi
fi

# make work directory
mkdir -p $path_w
ln -sfn $path_w $run_path/$run_tag/work

# create output path
echo "   [INFO] output path : $path_o"
echo "   [INFO] work path : $path_w"
mkdir -p $path_o $path_w
cd $path_w

#check if some errors occured during last process
echo "*** check past log ***"
if [ ! -z "$(ls -A $path_o 2> /dev/null)" ] ; then
  $waverun_path/check_error.sh $path_o
fi

#----------------------
## 4. PROGRAM
#----------------------

###########################################################
# Making of ww3_shel.nml                                  #
###########################################################

function make_shel() {

echo "   [INFO] RUN    :" $startrun,$stoprun
echo "   [INFO] OUTPUT :" $startout,$stopout
echo "   [INFO] RESTART:" $startres,$stopres

cat > ww3_shel.nml << EOF

! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_shel.nml - single-grid model                     !
! -------------------------------------------------------------------- !


! -------------------------------------------------------------------- !
! Define top-level model parameters via DOMAIN_NML namelist
!
! * IOSTYP defines the output server mode for parallel implementation.
!             0 : No data server processes, direct access output from
!                 each process (requires true parallel file system).
!             1 : No data server process. All output for each type 
!                 performed by process that performs computations too.
!             2 : Last process is reserved for all output, and does no
!                 computing.
!             3 : Multiple dedicated output processes.
!
! * namelist must be terminated with /
! * definitions & defaults:
!     DOMAIN%IOSTYP =  1                 ! Output server type
!     DOMAIN%START  = '19680606 000000'  ! Start date for the entire model 
!     DOMAIN%STOP   = '19680607 000000'  ! Stop date for the entire model
! -------------------------------------------------------------------- !
&DOMAIN_NML
  DOMAIN%START  = '$startrun'
  DOMAIN%STOP   = '$stoprun'
/



! -------------------------------------------------------------------- !
! Define each forcing via the INPUT_NML namelist
!
! * The FORCING flag can be  : 'F' for "no forcing"
!                              'T' for "external forcing file"
!                              'H' for "homogeneous forcing input"
!                              'C' for "coupled forcing field"
!
! * homogeneous forcing is not available for ICE_CONC
!
! * The ASSIM flag can :  'F' for "no forcing"
!                         'T' for "external forcing file"
!
! * namelist must be terminated with /
! * definitions & defaults:
!     INPUT%FORCING%WATER_LEVELS  = 'F'
!     INPUT%FORCING%CURRENTS      = 'F'
!     INPUT%FORCING%WINDS         = 'F'
!     INPUT%FORCING%ATM_MOMENTUM  = 'F'
!     INPUT%FORCING%AIR_DENSITY   = 'F'
!     INPUT%FORCING%ICE_CONC      = 'F'
!     INPUT%FORCING%ICE_PARAM1    = 'F'
!     INPUT%FORCING%ICE_PARAM2    = 'F'
!     INPUT%FORCING%ICE_PARAM3    = 'F'
!     INPUT%FORCING%ICE_PARAM4    = 'F'
!     INPUT%FORCING%ICE_PARAM5    = 'F'
!     INPUT%FORCING%MUD_DENSITY   = 'F'
!     INPUT%FORCING%MUD_THICKNESS = 'F'
!     INPUT%FORCING%MUD_VISCOSITY = 'F'
!     INPUT%ASSIM%MEAN            = 'F'
!     INPUT%ASSIM%SPEC1D          = 'F'
!     INPUT%ASSIM%SPEC2D          = 'F'
! -------------------------------------------------------------------- !
&INPUT_NML
EOF
if [ "$ice1" != no ] ; then
  if [ ! -z "$(echo $ice1 | grep ice1)" ] ; then
    echo "  INPUT%FORCING%ICE_PARAM1    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%ICE_PARAM1    = H" >> ww3_shel.nml
  fi
fi
if [ "$ice2" != no ] ; then
  if [ ! -z "$(echo $ice2 | grep ice2)" ] ; then
    echo "  INPUT%FORCING%ICE_PARAM2    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%ICE_PARAM2    = H" >> ww3_shel.nml
  fi
fi
if [ "$ice3" != no ] ; then
  if [ ! -z "$(echo $ice3 | grep ice3)" ] ; then
    echo "  INPUT%FORCING%ICE_PARAM3    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%ICE_PARAM3    = H" >> ww3_shel.nml
  fi
fi
if [ "$ice4" != no ] ; then
  if [ ! -z "$(echo $ice4 | grep ice4)" ] ; then
    echo "  INPUT%FORCING%ICE_PARAM4    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%ICE_PARAM4    = H" >> ww3_shel.nml
  fi
fi
if [ "$ice5" != no ] ; then
  if [ ! -z "$(echo $ice5 | grep ice5)" ] ; then
    echo "  INPUT%FORCING%ICE_PARAM5    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%ICE_PARAM5    = H" >> ww3_shel.nml
  fi
fi
if [ "$mud1" != no ] ; then
  if [ ! -z "$(echo $mud1 | grep mud1)" ] ; then
    echo "  INPUT%FORCING%MUD_DENSITY    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%MUD_DENSITY    = H" >> ww3_shel.nml
  fi
fi
if [ "$mud2" != no ] ; then
  if [ ! -z "$(echo $mud2 | grep mud2)" ] ; then
    echo "  INPUT%FORCING%MUD_THICKNESS    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%MUD_THICKNESS    = H" >> ww3_shel.nml
  fi
fi
if [ "$mud3" != no ] ; then
  if [ ! -z "$(echo $mud3 | grep mud3)" ] ; then
    echo "  INPUT%FORCING%MUD_VISCOSITY    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%MUD_VISCOSITY    = H" >> ww3_shel.nml
  fi
fi
if [ "$lev" != no ] ; then
  if [ ! -z "$(echo $lev | grep lev)" ] ; then
    echo "  INPUT%FORCING%WATER_LEVELS    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%WATER_LEVELS    = H" >> ww3_shel.nml
  fi
fi
if [ "$cur" != no ] ; then
  if [ ! -z "$(echo $cur | grep cur)" ] ; then
    echo "  INPUT%FORCING%CURRENTS    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%CURRENTS    = H" >> ww3_shel.nml
  fi
fi
if [ "$wind" != no ] ; then
  if [ ! -z "$(echo $wind | grep wind)" ] ; then
    echo "  INPUT%FORCING%WINDS    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%WINDS    = H" >> ww3_shel.nml
  fi
fi
if [ "$ice" != no ] ; then
  if [ ! -z "$(echo $ice | grep ice)" ] ; then
    echo "  INPUT%FORCING%ICE_CONC    = T" >> ww3_shel.nml
  else
    echo "  INPUT%FORCING%ICE_CONC    = H" >> ww3_shel.nml
  fi
fi


cat >> ww3_shel.nml << EOF
/




! -------------------------------------------------------------------- !
! Define the output types point parameters via OUTPUT_TYPE_NML namelist
!
! * the point file is a space separated values per line :
!   longitude latitude 'name' (C*40)
!
! * the full list of field names is : 
!  All parameters listed below are available in output file of the types
!  ASCII and NetCDF. If selected output file types are grads or grib, 
!  some parameters may not be available. The first two columns in the
!  table below identify such cases by flags, cols 1 (GRB) and 2 (GXO)
!  refer to grib (ww3_grib) and grads (gx_outf), respectively.
!
! Columns 3 and 4 provide group and parameter numbers per group.
! Columns 5, 6 and 7 provide:
!   5 - code name (internal)
!   6 - output tags (names used is ASCII file extensions, NetCDF 
!       variable names and namelist-based selection
!   7 - Long parameter name/definition
!
!  G  G
!  R  X Grp  Param Code     Output  Parameter/Group
!  B  O Numb Numbr Name        Tag  Definition 
!  --------------------------------------------------
!        1                          Forcing Fields
!   -------------------------------------------------
!  T  T  1     1   DW         DPT   Water depth.
!  T  T  1     2   C[X,Y]     CUR   Current velocity.
!  T  T  1     3   UA         WND   Wind speed.
!  T  T  1     4   AS         AST   Air-sea temperature difference.
!  T  T  1     5   WLV        WLV   Water levels.
!  T  T  1     6   ICE        ICE   Ice concentration.
!  T  T  1     7   IBG        IBG   Iceberg-induced damping.
!  T  T  1     8   D50        D50   Median sediment grain size.
!  T  T  1     9   IC1        IC1   Ice thickness.
!  T  T  1    10   IC5        IC5   Ice flow diameter.
!   -------------------------------------------------
!        2                          Standard mean wave Parameters
!   -------------------------------------------------
!  T  T  2     1   HS         HS    Wave height.
!  T  T  2     2   WLM        LM    Mean wave length.
!  T  T  2     3   T02        T02   Mean wave period (Tm0,2).
!  T  T  2     4   T0M1       T0M1  Mean wave period (Tm0,-1).
!  T  T  2     5   T01        T01   Mean wave period (Tm0,1).
!  T  T  2     6   FP0        FP    Peak frequency.
!  T  T  2     7   THM        DIR   Mean wave direction.
!  T  T  2     8   THS        SPR   Mean directional spread.
!  T  T  2     9   THP0       DP    Peak direction.
!  T  T  2    10   HIG        HIG   Infragravity height
!  T  T  2    11   STMAXE     MXE   Max surface elev (STE)
!  T  T  2    12   STMAXD     MXES  St Dev of max surface elev (STE)
!  T  T  2    13   HMAXE      MXH   Max wave height (STE)
!  T  T  2    14   HCMAXE     MXHC  Max wave height from crest (STE)
!  T  T  2    15   HMAXD      SDMH  St Dev of MXC (STE)
!  T  T  2    16   HCMAXD     SDMHC St Dev of MXHC (STE)
!  F  T  2    17   WBT        WBT   Dominant wave breaking probability bT
!  F  F  2    18   FP0        TP    Peak period (from peak freq)
!   -------------------------------------------------
!        3                          Spectral Parameters (first 5)
!   -------------------------------------------------
!  F  F  3     1   EF         EF    Wave frequency spectrum
!  F  F  3     2   TH1M       TH1M  Mean wave direction from a1,b2
!  F  F  3     3   STH1M      STH1M Directional spreading from a1,b2
!  F  F  3     4   TH2M       TH2M  Mean wave direction from a2,b2
!  F  F  3     5   STH2M      STH2M Directional spreading from a2,b2
!  F  F  3     6   WN         WN    Wavenumber array
!   -------------------------------------------------
!        4                          Spectral Partition Parameters 
!   -------------------------------------------------
!  T  T  4     1   PHS        PHS   Partitioned wave heights.
!  T  T  4     2   PTP        PTP   Partitioned peak period.
!  T  T  4     3   PLP        PLP   Partitioned peak wave length.
!  T  T  4     4   PDIR       PDIR  Partitioned mean direction.
!  T  T  4     5   PSI        PSPR  Partitioned mean directional spread.
!  T  T  4     6   PWS        PWS   Partitioned wind sea fraction.
!  T  T  4     7   PDP        PDP   Peak wave direction of partition.
!  T  T  4     8   PQP        PQP   Goda peakdedness parameter of partition.
!  T  T  4     9   PPE        PPE   JONSWAP peak enhancement factor of partition.
!  T  T  4    10   PGW        PGW   Gaussian frequency width of partition.
!  T  T  4    11   PSW        PSW   Spectral width of partition.
!  T  T  4    12   PTM1       PTM10 Mean wave period (m-1,0) of partition.
!  T  T  4    13   PT1        PT01  Mean wave period (m0,1) of partition.
!  T  T  4    14   PT2        PT02  Mean wave period (m0,2) of partition.
!  T  T  4    15   PEP        PEP   Peak spectral density of partition.
!  T  T  4    16   PWST       TWS   Total wind sea fraction.
!  T  T  4    17   PNR        PNR   Number of partitions.
!   -------------------------------------------------
!        5                          Atmosphere-waves layer
!   -------------------------------------------------
!  T  T  5     1   UST        UST   Friction velocity.
!  F  T  5     2   CHARN      CHA   Charnock parameter
!  F  T  5     3   CGE        CGE   Energy flux
!  F  T  5     4   PHIAW      FAW   Air-sea energy flux
!  F  T  5     5   TAUWI[X,Y] TAW   Net wave-supported stress
!  F  T  5     6   TAUWN[X,Y] TWA   Negative part of the wave-supported stress
!  F  F  5     7   WHITECAP   WCC   Whitecap coverage
!  F  F  5     8   WHITECAP   WCF   Whitecap thickness
!  F  F  5     9   WHITECAP   WCH   Mean breaking height
!  F  F  5    10   WHITECAP   WCM   Whitecap moment
!  F  F  5    11   FWS        FWS   Wind sea mean period
!   -------------------------------------------------
!        6                          Wave-ocean layer 
!   -------------------------------------------------
!  F  F  6     1   S[XX,YY,XY] SXY  Radiation stresses.
!  F  F  6     2   TAUO[X,Y]  TWO   Wave to ocean momentum flux
!  F  F  6     3   BHD        BHD   Bernoulli head (J term) 
!  F  F  6     4   PHIOC      FOC   Wave to ocean energy flux
!  F  F  6     5   TUS[X,Y]   TUS   Stokes transport
!  F  F  6     6   USS[X,Y]   USS   Surface Stokes drift
!  F  F  6     7   [PR,TP]MS  P2S   Second-order sum pressure 
!  F  F  6     8   US3D       USF   Spectrum of surface Stokes drift
!  F  F  6     9   P2SMS      P2L   Micro seism  source term
!  F  F  6    10   TAUICE     TWI   Wave to sea ice stress
!  F  F  6    11   PHICE      FIC   Wave to sea ice energy flux
!  F  F  6    12   USSP       USP   Partitioned surface Stokes drift
!   -------------------------------------------------
!        7                          Wave-bottom layer 
!   -------------------------------------------------
!  F  F  7     1   ABA        ABR   Near bottom rms amplitides.
!  F  F  7     2   UBA        UBR   Near bottom rms velocities.
!  F  F  7     3   BEDFORMS   BED   Bedforms
!  F  F  7     4   PHIBBL     FBB   Energy flux due to bottom friction 
!  F  F  7     5   TAUBBL     TBB   Momentum flux due to bottom friction
!   -------------------------------------------------
!        8                          Spectrum parameters
!   -------------------------------------------------
!  F  F  8     1   MSS[X,Y]   MSS   Mean square slopes
!  F  F  8     2   MSC[X,Y]   MSC   Spectral level at high frequency tail
!  F  F  8     3   WL02[X,Y]  WL02  East/X North/Y mean wavelength compon
!  F  F  8     4   ALPXT      AXT   Correl sea surface gradients (x,t)
!  F  F  8     5   ALPYT      AYT   Correl sea surface gradients (y,t)
!  F  F  8     6   ALPXY      AXY   Correl sea surface gradients (x,y)
!   -------------------------------------------------
!        9                          Numerical diagnostics  
!   -------------------------------------------------
!  T  T  9     1   DTDYN      DTD   Average time step in integration.
!  T  T  9     2   FCUT       FC    Cut-off frequency.
!  T  T  9     3   CFLXYMAX   CFX   Max. CFL number for spatial advection. 
!  T  T  9     4   CFLTHMAX   CFD   Max. CFL number for theta-advection. 
!  F  F  9     5   CFLKMAX    CFK   Max. CFL number for k-advection. 
!   -------------------------------------------------
!        10                         User defined          
!   -------------------------------------------------
!  F  F  10    1              U1    User defined #1. (requires coding ...)
!  F  F  10    2              U2    User defined #1. (requires coding ...)
!   -------------------------------------------------
!
!     Section 4 consist of a set of fields, index 0 = wind sea, index
!     1:NOSWLL are first NOSWLL swell fields.
!
!
! * output track file formatted (T) or unformated (F)
!
! * coupling fields exchanged list is :
!   - Sent fields by ww3:
!       - Ocean model : T0M1 OCHA OHS DIR BHD TWO UBR FOC TAW TUS USS LM DRY
!       - Atmospheric model : ACHA AHS TP (or FP) FWS
!       - Ice model : IC5 TWI
!   - Received fields by ww3:
!       - Ocean model : SSH CUR
!       - Atmospheric model : WND
!       - Ice model : ICE IC1 IC5
!
! * Coupling at T+0 (extra fields in the restart needed)
!
! * extra fields to be written to the restart:
!   - The list includes all fields sent by coupling exchange only
!
! * namelist must be terminated with /
! * definitions & defaults:
!     TYPE%FIELD%LIST         =  'unset'
!     TYPE%POINT%FILE         =  'points.list'
!     TYPE%TRACK%FORMAT       =  T
!     TYPE%PARTITION%X0       =  0
!     TYPE%PARTITION%XN       =  0
!     TYPE%PARTITION%NX       =  0
!     TYPE%PARTITION%Y0       =  0
!     TYPE%PARTITION%YN       =  0
!     TYPE%PARTITION%NY       =  0
!     TYPE%PARTITION%FORMAT   =  T
!     TYPE%COUPLING%SENT      = 'unset'
!     TYPE%COUPLING%RECEIVED  = 'unset'
!     TYPE%COUPLING%COUPLET0  = .FALSE.
!     TYPE%RESTART%EXTRA      = 'unset'
!
! -------------------------------------------------------------------- !
&OUTPUT_TYPE_NML
EOF

if [ "$dt_po" != "0" ]
then
  if [ ! -e $path_d/$pt_file ]
  then
    echo "   [ERROR] no $pt_file in $path_d"
    exit 1
  else
    cp $path_d/$pt_file $path_w/
  fi
  cat >> ww3_shel.nml << EOF
    TYPE%POINT%FILE     = '$pt_file'
EOF
fi

cat >> ww3_shel.nml << EOF
  TYPE%FIELD%LIST     = '$fields'
/
EOF

cat >> ww3_shel.nml << EOF


! -------------------------------------------------------------------- !
! Define output dates via OUTPUT_DATE_NML namelist
!
! * start and stop times are with format 'yyyymmdd hhmmss'
! * if time stride is equal '0', then output is disabled
! * time stride is given in seconds
!
! * namelist must be terminated with /
! * definitions & defaults:
!     DATE%FIELD%START         =  '19680606 000000'
!     DATE%FIELD%STRIDE        =  '0'
!     DATE%FIELD%STOP          =  '19680607 000000'
!     DATE%POINT%START         =  '19680606 000000'
!     DATE%POINT%STRIDE        =  '0'
!     DATE%POINT%STOP          =  '19680607 000000'
!     DATE%TRACK%START         =  '19680606 000000'
!     DATE%TRACK%STRIDE        =  '0'
!     DATE%TRACK%STOP          =  '19680607 000000'
!     DATE%RESTART%START       =  '19680606 000000'
!     DATE%RESTART%STRIDE      =  '0'
!     DATE%RESTART%STOP        =  '19680607 000000'
!     DATE%BOUNDARY%START      =  '19680606 000000'
!     DATE%BOUNDARY%STRIDE     =  '0'
!     DATE%BOUNDARY%STOP       =  '19680607 000000'
!     DATE%PARTITION%START     =  '19680606 000000'
!     DATE%PARTITION%STRIDE    =  '0'
!     DATE%PARTITION%STOP      =  '19680607 000000'
!     DATE%COUPLING%START      =  '19680606 000000'
!     DATE%COUPLING%STRIDE     =  '0'
!     DATE%COUPLING%STOP       =  '19680607 000000'
!
!     DATE%RESTART             =  '19680606 000000' '0' '19680607 000000'
! -------------------------------------------------------------------- !
&OUTPUT_DATE_NML
  DATE%FIELD               = '$startout' '$dt_mwp' '$stopout'
  DATE%POINT               = '$startout' '$dt_po'  '$stopout'
  DATE%TRACK               = '$startout' '$dt_oat' '$stopout'
  DATE%RESTART             = '$startres' '$dt_rst' '$stopres'
  DATE%BOUNDARY            = '$startout' '$dt_bd'  '$stopout'
  DATE%PARTITION           = '$startout' '$dt_p'   '$stopout'
/



! -------------------------------------------------------------------- !
! Define homogeneous input via HOMOG_COUNT_NML and HOMOG_INPUT_NML namelist
!
! * the number of each homogeneous input is defined by HOMOG_COUNT
! * the total number of homogeneous input is automatically calculated
! * the homogeneous input must start from index 1 to N
! * if VALUE1 is equal 0, then the homogeneous input is desactivated
! * NAME can be IC1, IC2, IC3, IC4, IC5, MDN, MTH, MVS, LEV, CUR, WND, ICE, MOV
! * each homogeneous input is defined over a maximum of 3 values detailled below :
!     - IC1 is defined by thickness
!     - IC2 is defined by viscosity
!     - IC3 is defined by density
!     - IC4 is defined by modulus
!     - IC5 is defined by floe diameter
!     - MDN is defined by density
!     - MTH is defined by thickness
!     - MVS is defined by viscosity
!     - LEV is defined by height
!     - CUR is defined by speed and direction
!     - WND is defined by speed, direction and airseatemp
!     - ICE is defined by concentration
!     - MOV is defined by speed and direction
!
! * namelist must be terminated with /
! * definitions & defaults:
!     HOMOG_COUNT%N_IC1            =  0
!     HOMOG_COUNT%N_IC2            =  0
!     HOMOG_COUNT%N_IC3            =  0
!     HOMOG_COUNT%N_IC4            =  0
!     HOMOG_COUNT%N_IC5            =  0
!     HOMOG_COUNT%N_MDN            =  0
!     HOMOG_COUNT%N_MTH            =  0
!     HOMOG_COUNT%N_MVS            =  0
!     HOMOG_COUNT%N_LEV            =  0
!     HOMOG_COUNT%N_CUR            =  0
!     HOMOG_COUNT%N_WND            =  0
!     HOMOG_COUNT%N_ICE            =  0
!     HOMOG_COUNT%N_MOV            =  0
!
!     HOMOG_INPUT(I)%NAME           =  'unset'
!     HOMOG_INPUT(I)%DATE           =  '19680606 000000'
!     HOMOG_INPUT(I)%VALUE1         =  0
!     HOMOG_INPUT(I)%VALUE2         =  0
!     HOMOG_INPUT(I)%VALUE3         =  0
! -------------------------------------------------------------------- !
&HOMOG_COUNT_NML
/ 


&HOMOG_INPUT_NML
/


! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF


chmod 775 ww3_shel.nml
}


###########################################################
# Making of ww3_shel.inp                                  #
###########################################################

function make_shel_inp() {

echo "   [INFO] RUN    :" $startrun,$stoprun
echo "   [INFO] OUTPUT :" $startout,$stopout
echo "   [INFO] RESTART:" $startres,$stopres

cat > ww3_shel.inp << EOF
$ -------------------------------------------------------------------- $
$ WAVEWATCH III shell input file                                       $
$ -------------------------------------------------------------------- $
$ Define input to be used with flag for use and flag for definition
$ as a homogeneous field (first three only); seven input lines.
$
EOF

#if [ "$ice1" != 'no' ] || [ "$ice2" != 'no' ] ||[ "$ice3" != 'no' ] || [ "$ice4" != 'no' ] || [ "$ice5" != 'no' ] || [ "$mud1" != 'no' ] || [ "$mud2" != 'no' ] || [ "$mud3" != 'no' ] ; then

  if [ "$ice1" == 'no' ] ; then
    echo " F F     Ice parameter 1"   >> ww3_shel.inp
  elif [ ! -z "$(echo $ice1 | grep ice1)" ] ; then
    echo " T F     Ice parameter 1"   >> ww3_shel.inp
  else
    echo " T T     Ice parameter 1"   >> ww3_shel.inp
  fi
  if [ "$ice2" == 'no' ] ; then
    echo " F F     Ice parameter 2"   >> ww3_shel.inp
  elif [ ! -z "$(echo $ice2 | grep ice2)" ] ; then
    echo " T F     Ice parameter 2"   >> ww3_shel.inp
  else
    echo " T T     Ice parameter 2"   >> ww3_shel.inp
  fi
  if [ "$ice3" == 'no' ] ; then
    echo " F F     Ice parameter 3"   >> ww3_shel.inp
  elif [ ! -z "$(echo $ice3 | grep ice3)" ] ; then
    echo " T F     Ice parameter 3"   >> ww3_shel.inp
  else
    echo " T T     Ice parameter 3"   >> ww3_shel.inp
  fi
  if [ "$ice4" == 'no' ] ; then
    echo " F F     Ice parameter 4"   >> ww3_shel.inp
  elif [ ! -z "$(echo $ice4 | grep ice4)" ] ; then
    echo " T F     Ice parameter 4"   >> ww3_shel.inp
  else
    echo " T T     Ice parameter 4"   >> ww3_shel.inp
  fi
  if [ "$ice5" == 'no' ] ; then
    echo " F F     Ice parameter 5"   >> ww3_shel.inp
  elif [ ! -z "$(echo $ice5 | grep ice5)" ] ; then
    echo " T F     Ice parameter 5"   >> ww3_shel.inp
  else
    echo " T T     Ice parameter 5"   >> ww3_shel.inp
  fi
  if [ "$mud1" == 'no' ] ; then
    echo " F F     Mud parameter 1"   >> ww3_shel.inp
  elif [ ! -z "$(echo $mud1 | grep mud1)" ] ; then
    echo " T F     Mud parameter 1"   >> ww3_shel.inp
  else
    echo " T T     Mud parameter 1"   >> ww3_shel.inp
  fi
  if [ "$mud2" == 'no' ] ; then
    echo " F F     Mud parameter 2"   >> ww3_shel.inp
  elif [ ! -z "$(echo $mud2 | grep mud2)" ] ; then
    echo " T F     Mud parameter 2"   >> ww3_shel.inp
  else
    echo " T T     Mud parameter 2"   >> ww3_shel.inp
  fi
  if [ "$mud3" == 'no' ] ; then
    echo " F F     Mud parameter 3"   >> ww3_shel.inp
  elif [ ! -z "$(echo $mud3 | grep mud3)" ] ; then
    echo " T F     Mud parameter 3"   >> ww3_shel.inp
  else
    echo " T T     Mud parameter 3"   >> ww3_shel.inp
  fi
#fi
if [ "$lev" != 'no' ] ; then
  echo " T F     Water levels"   >> ww3_shel.inp
else 
  echo " F F     Water levels"   >> ww3_shel.inp
fi
if [ "$cur" != 'no' ] ; then
  echo " T F     Currents"   >> ww3_shel.inp
else 
  echo " F F     Currents"   >> ww3_shel.inp
fi
if [ "$wind" != 'no' ] ; then
  echo " T F     Winds"   >> ww3_shel.inp
else 
  echo " F F     Winds"   >> ww3_shel.inp
fi
if [ "$ice" != 'no' ] ; then
  echo " T F     Ice concentrations"   >> ww3_shel.inp
else 
  echo " F F     Ice concentrations"   >> ww3_shel.inp    
fi
if [ "$mean" != 'no' ] ; then
  echo " T       Assimilation data : Mean parameters"   >> ww3_shel.inp
else 
  echo " F       Assimilation data : Mean parameters"   >> ww3_shel.inp
fi
if [ "$spec1d" != 'no' ] ; then
  echo " T       Assimilation data : 1-D spectra"   >> ww3_shel.inp
else 
  echo " F       Assimilation data : 1-D spectra"   >> ww3_shel.inp
fi
if [ "$spec2d" != 'no' ] ; then
  echo " T       Assimilation data : 2-D spectra"   >> ww3_shel.inp
else 
  echo " F       Assimilation data : 2-D spectra"   >> ww3_shel.inp
fi

cat >> ww3_shel.inp << EOF
$
$
   $startrun
   $stoprun
$    IOSTYP = 0 : No data server processes, direct access output from
$                 each process (requirese true parallel file system).
$             1 : No data server process. All output for each type 
$                 performed by process that performes computations too.
$             2 : Last process is reserved for all output, and does no
$                 computing.
$             3 : Multiple dedicated output processes.
$
 1
$
$
   $startout  $dt_mwp   $stopout
$ -------------------------------------------------------------------- $
$ Output request flags identifying fields as in ww3_shel input and
$ section 2.4 of the manual. If the first flag is 'N' then a namelist
$ is read.
$
 N
 $fields
$
$ -------------------------------------------------------------------- $
$
   $startout  $dt_po   $stopout
$
$ output points for $run_tag
$
EOF
   
if [ "$dt_po" != "0" ]
then
  if [ ! -e $path_d/$pt_file ]
  then
    echo "   [ERROR] no $pt_file in $path_d"
    exit 1
  else
    cat $path_d/$pt_file >> ww3_shel.inp
    echo -e  " 0.00    0.00  'STOPSTRING'" >> ww3_shel.inp
  fi
fi

cat >> ww3_shel.inp << EOF
$
   $startout $dt_oat  $stopout
EOF
if [ "$dt_oat" != "0" ]
then
  echo "  T  " >> ww3_shel.inp
fi
cat >> ww3_shel.inp << EOF
   $startres $dt_rst  $stopres
   $startout $dt_bd   $stopout
   $startout $dt_p    $stopout
EOF
if [ "$dt_p" != "0" ]
then
  echo "      0 9999 1 0 9999 1 T  " >> ww3_shel.inp
fi
cat >> ww3_shel.inp << EOF
$
$
EOF
if [ "$ice1" != 'no' ] && [ -z "$(echo $ice1 | grep ice1)" ] ; then
  echo "  'IC1' $startrun $ice1"   >> ww3_shel.inp
fi
if [ "$ice2" != 'no' ] && [ -z "$(echo $ice2 | grep ice2)" ] ; then
  echo "  'IC2' $startrun $ice2"   >> ww3_shel.inp
fi
if [ "$ice3" != 'no' ] && [ -z "$(echo $ice3 | grep ice3)" ] ; then
  echo "  'IC3' $startrun $ice3"   >> ww3_shel.inp
fi
if [ "$ice4" != 'no' ] && [ -z "$(echo $ice4 | grep ice4)" ] ; then
  echo "  'IC4' $startrun $ice4"   >> ww3_shel.inp
fi
if [ "$ice5" != 'no' ] && [ -z "$(echo $ice5 | grep ice5)" ] ; then
  echo "  'IC5' $startrun $ice5"   >> ww3_shel.inp
fi
if [ "$mud1" != 'no' ] && [ -z "$(echo $mud1 | grep mud1)" ] ; then
  echo "  'MDN' $startout $mud1"   >> ww3_shel.inp
fi
if [ "$mud2" != 'no' ] && [ -z "$(echo $mud2 | grep mud2)" ] ; then
  echo "  'MTH' $startout $mud2"   >> ww3_shel.inp
fi
if [ "$mud3" != 'no' ] && [ -z "$(echo $mud3 | grep mud3)" ] ; then
  echo "  'MVS' $startout $mud3"   >> ww3_shel.inp
fi
if [ "$mean" != 'no' ] && [ -z "$(echo $mean | grep mean)" ] ; then
  echo "  'DT0' $startout $mean"   >> ww3_shel.inp
fi
if [ "$spec1d" != 'no' ] && [ -z "$(echo $spec1d | grep spec1d)" ] ; then
  echo "  'DT1' $startout $spec1d"   >> ww3_shel.inp
fi
if [ "$spec2d" != 'no' ] && [ -z "$(echo $spec2d | grep spec2d)" ] ; then
  echo "  'DT2' $startout $spec2d"   >> ww3_shel.inp
fi
cat >> ww3_shel.inp << EOF
  'STP'
$
$ ------------------------------------
$ End of input file
$ ------------------------------------
EOF
chmod 775 ww3_shel.inp
} 



# dedicated grid or not for spectral output points
# initialize at TRUE
UPTS='T'
if [ "$dt_po" == "0" ]
then
  UPTS='F'
  buoys='no'
fi
if [ "$buoys" == "no" ]
then
	UPTS='F'
fi

# number of zooms
NR=`echo $mods | wc -w | awk '{print $1}'` 

###########################################################
# Making of ww3_multi.nml                                 #
###########################################################

function make_multi_inp() {
echo "nothing done"
}

function make_multi() {

forc="wind ice lev cur ice1 ice2 ice3 ice4 ice5 mud1 mud2 mud3"
NRI=0
for forcing in $forc
do
  val=$(echo $(eval echo "\${$forcing}"))

  if [ "$val" != 'no' ]
  then
    export NRI=`expr $NRI + 1`
  fi
done

echo "   [INFO] RUN    :" $startrun,$stoprun
echo "   [INFO] OUTPUT :" $startout,$stopout
echo "   [INFO] RESTART:" $startres,$stopres


cat > ww3_multi.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_multi.nml - multi-grid model                     !
! -------------------------------------------------------------------- !


! -------------------------------------------------------------------- !
! Define top-level model parameters via DOMAIN_NML namelist
!
! * IOSTYP defines the output server mode for parallel implementation.
!             0 : No data server processes, direct access output from
!                 each process (requires true parallel file system).
!             1 : No data server process. All output for each type 
!                 performed by process that performs computations too.
!             2 : Last process is reserved for all output, and does no
!                 computing.
!             3 : Multiple dedicated output processes.
!
! * namelist must be terminated with /
! * definitions & defaults:
!     DOMAIN%NRINP  =  0  ! Number of grids defining input fields.
!     DOMAIN%NRGRD  =  1  ! Number of wave model grids.
!     DOMAIN%UNIPTS =  F  ! Flag for using unified point output file.
!     DOMAIN%IOSTYP =  1  ! Output server type
!     DOMAIN%UPPROC =  F  ! Flag for dedicated process for unified point output.
!     DOMAIN%PSHARE =  F  ! Flag for grids sharing dedicated output processes.
!     DOMAIN%FLGHG1 =  F  ! Flag for masking computation in two-way nesting
!     DOMAIN%FLGHG2 =  F  ! Flag for masking at printout time
!     DOMAIN%START  = '19680606 000000'  ! Start date for the entire model 
!     DOMAIN%STOP   = '19680607 000000'  ! Stop date for the entire model
! -------------------------------------------------------------------- !
&DOMAIN_NML
  DOMAIN%NRINP  = $NRI
  DOMAIN%NRGRD  = $NR
  DOMAIN%UNIPTS = $UPTS
  DOMAIN%IOSTYP = 2
  DOMAIN%UPPROC = T
  DOMAIN%PSHARE = T
  DOMAIN%START  = '$startrun'
  DOMAIN%STOP   = '$stoprun'
/



! -------------------------------------------------------------------- !
! Define each input grid via the INPUT_GRID_NML namelist
!
! * index I must match indexes from 1 to DOMAIN%NRINP
! * INPUT(I)%NAME must be set for each active input grid I
!
! * namelist must be terminated with /
! * definitions & defaults:
!     INPUT(I)%NAME                  = 'unset'
!     INPUT(I)%FORCING%WATER_LEVELS  = F
!     INPUT(I)%FORCING%CURRENTS      = F
!     INPUT(I)%FORCING%WINDS         = F
!     INPUT(I)%FORCING%ICE_CONC      = F
!     INPUT(I)%FORCING%ICE_PARAM1    = F
!     INPUT(I)%FORCING%ICE_PARAM2    = F
!     INPUT(I)%FORCING%ICE_PARAM3    = F
!     INPUT(I)%FORCING%ICE_PARAM4    = F
!     INPUT(I)%FORCING%ICE_PARAM5    = F
!     INPUT(I)%FORCING%MUD_DENSITY   = F
!     INPUT(I)%FORCING%MUD_THICKNESS = F
!     INPUT(I)%FORCING%MUD_VISCOSITY = F
!     INPUT(I)%ASSIM%MEAN            = F
!     INPUT(I)%ASSIM%SPEC1D          = F
!     INPUT(I)%ASSIM%SPEC2D          = F
! -------------------------------------------------------------------- !
&INPUT_GRID_NML
EOF
iri=0
if [ "$ice1" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$ice1'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%ICE_PARAM1    = T" >> ww3_multi.nml
fi
if [ "$ice2" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$ice2'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%ICE_PARAM2    = T" >> ww3_multi.nml
fi
if [ "$ice3" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$ice3'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%ICE_PARAM3    = T" >> ww3_multi.nml
fi
if [ "$ice4" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$ice4'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%ICE_PARAM4    = T" >> ww3_multi.nml
fi
if [ "$ice5" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$ice5'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%ICE_PARAM5    = T" >> ww3_multi.nml
fi
if [ "$mud1" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$mud1'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%MUD_DENSITY   = T" >> ww3_multi.nml
fi
if [ "$mud2" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$mud2'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%MUD_THICKNESS = T" >> ww3_multi.nml
fi
if [ "$mud3" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$mud3'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%MUD_VISCOSITY = T" >> ww3_multi.nml
fi
if [ "$lev" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$lev'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%WATER_LEVELS  = T" >> ww3_multi.nml
fi
if [ "$cur" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$cur'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%CURRENTS      = T" >> ww3_multi.nml
fi
if [ "$wind" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$wind'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%WINDS         = T" >> ww3_multi.nml
fi
if [ "$ice" != no ] ; then
  iri=$(expr $iri + 1)
  echo "  INPUT($iri)%NAME                  = '$ice'" >> ww3_multi.nml
  echo "  INPUT($iri)%FORCING%ICE_CONC      = T" >> ww3_multi.nml
fi

if [ $iri -ne $NRI ] ; then
  echo "ERROR : number of input flag $iri is not consistent with input grid number $NRI"
  exit 1
fi

cat >> ww3_multi.nml << EOF
/



! -------------------------------------------------------------------- !
! Define each model grid via the MODEL_GRID_NML namelist
!
! * index I must match indexes from 1 to DOMAIN%NRGRD
! * MODEL(I)%NAME must be set for each active model grid I
! * FORCING can be set as : 
!    - 'no'          : This input is not used.
!    - 'native'      : This grid has its own input files, e.g. grid
!                      grdX (mod_def.grdX) uses ice.grdX.
!    - 'INPUT%NAME'  : Take input from the grid identified by
!                      INPUT%NAME.
! * RESOURCE%RANK_ID : Rank number of grid (internally sorted and reassigned).
! * RESOURCE%GROUP_ID : Group number (internally reassigned so that different
!                                     ranks result in different group numbers).
! * RESOURCE%COMM_FRAC : Fraction of communicator (processes) used for this grid.
! * RESOURCE%BOUND_FLAG : Flag identifying dumping of boundary data used by this
!                         grid. If true, the file nest.MODID is generated.
!
! * Limitations relevant to irregular (curvilinear) grids:
!   1) Equal rank is not supported when one or more is an irregular
!       grid. Use non-equal rank instead. (see wmgridmd.ftn)
!   2) Non-native input grids: feature is not supported when either
!      an input grid or computational grids is irregular.
!      (see wmupdtmd.ftn)
!   3) Irregular grids with unified point output: This is supported
!      but the feature has not been verified for accuracy.
!      (see wmiopomd.ftn)
!
! * namelist must be terminated with /
! * definitions & defaults:
!     MODEL(I)%NAME                  = 'unset'
!     MODEL(I)%FORCING%WATER_LEVELS  = 'no'
!     MODEL(I)%FORCING%CURRENTS      = 'no'
!     MODEL(I)%FORCING%WINDS         = 'no'
!     MODEL(I)%FORCING%ICE_CONC      = 'no'
!     MODEL(I)%FORCING%ICE_PARAM1    = 'no'
!     MODEL(I)%FORCING%ICE_PARAM2    = 'no'
!     MODEL(I)%FORCING%ICE_PARAM3    = 'no'
!     MODEL(I)%FORCING%ICE_PARAM4    = 'no'
!     MODEL(I)%FORCING%ICE_PARAM5    = 'no'
!     MODEL(I)%FORCING%MUD_DENSITY   = 'no'
!     MODEL(I)%FORCING%MUD_THICKNESS = 'no'
!     MODEL(I)%FORCING%MUD_VISCOSITY = 'no'
!     MODEL(I)%ASSIM%MEAN            = 'no'
!     MODEL(I)%ASSIM%SPEC1d          = 'no'
!     MODEL(I)%ASSIM%SPEC2d          = 'no'
!     MODEL(I)%RESOURCE%RANK_ID      = I
!     MODEL(I)%RESOURCE%GROUP_ID     = 1
!     MODEL(I)%RESOURCE%COMM_FRAC    = 0.00,1.00
!     MODEL(I)%RESOURCE%BOUND_FLAG   = F
!
!     MODEL(4)%FORCING = 'no' 'no' 'no' 'no' 'no' 'no'
!
!     MODEL(2)%RESOURCE = 1 1 0.00 1.00 F
! -------------------------------------------------------------------- !
&MODEL_GRID_NML
EOF

incmod=0
for grid in $mods
do
  incmod=$(expr $incmod + 1)
  # name
  echo "MODEL($incmod)%NAME                  = '$grid'" >> ww3_multi.nml
  # forcing
  forcing=$(eval echo "\${forcing_$incmod}")
  if [ -z "$(echo $forcing)" ] ; then
    echo "   [ERROR] no forcing_$incmod defined in wavesetup.env"
    exit 1
  fi
  if [ ! -z "$(echo $forcing | grep -w 'lev')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%WATER_LEVELS  = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%WATER_LEVELS  = '$lev'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'cur')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%CURRENTS      = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%CURRENTS      = '$cur'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'wind')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%WINDS         = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%WINDS         = '$wind'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'ice')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%ICE_CONC      = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%ICE_CONC      = '$ice'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'ice1')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%ICE_PARAM1    = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%ICE_PARAM1    = '$ice1'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'ice2')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%ICE_PARAM2    = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%ICE_PARAM2    = '$ice2'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'ice3')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%ICE_PARAM3    = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%ICE_PARAM3    = '$ice3'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'ice4')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%ICE_PARAM4    = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%ICE_PARAM4    = '$ice4'" >> ww3_multi.nml
    fi
  fi
  if [ ! -z "$(echo $forcing | grep -w 'ice5')" ] ; then
    if [ ${native[$incmod]} = true ] ; then
      echo "  MODEL($incmod)%FORCING%ICE_PARAM5    = 'native'" >> ww3_multi.nml
    else
      echo "  MODEL($incmod)%FORCING%ICE_PARAM5    = '$ice5'" >> ww3_multi.nml
    fi
  fi
  # resource
  resource=$(eval echo "\${resource_$incmod}")
  if [ -z "$(echo $forcing)" ] ; then
    echo "   [ERROR] no resource_$incmod defined wavesetup.env"
    exit 1
  fi
  echo "  MODEL($incmod)%RESOURCE              = $resource" >> ww3_multi.nml
done

cat >> ww3_multi.nml << EOF
/


! -------------------------------------------------------------------- !
! Define the output types point parameters via OUTPUT_TYPE_NML namelist
!
! * index I must match indexes from 1 to DOMAIN%NRGRD
! * ALLTYPE will apply the output types for all the model grids
! * ITYPE(I) will apply the output types for the model grid number I
! * need DOMAIN%UNIPTS equal true to use a unified point output file
! * the point file is a space separated values per line : lon lat 'name'
! * the full list of field names is : 
!  DPT CUR WND AST WLV ICE IBG D50 IC1 IC5 HS LM T02 T0M1 T01 FP DIR SPR
!  DP HIG EF TH1M STH1M TH2M STH2M WN PHS PTP PLP PDIR PSPR PWS TWS PNR
!  UST CHA CGE FAW TAW TWA WCC WCF WCH WCM SXY TWO BHD FOC TUS USS P2S
!  USF P2L TWI FIC ABR UBR BED FBB TBB MSS MSC DTD FC CFX CFD CFK U1 U2 
! * output track file formatted (T) or unformated (F)
!
! * namelist must be terminated with /
! * definitions & defaults:
!     ALLTYPE%FIELD%LIST         =  'unset'
!     ALLTYPE%POINT%NAME         =  'unset'
!     ALLTYPE%POINT%FILE         =  'points.list'
!     ALLTYPE%TRACK%FORMAT       =  T
!     ALLTYPE%PARTITION%X0       =  0
!     ALLTYPE%PARTITION%XN       =  0
!     ALLTYPE%PARTITION%NX       =  0
!     ALLTYPE%PARTITION%Y0       =  0
!     ALLTYPE%PARTITION%YN       =  0
!     ALLTYPE%PARTITION%NY       =  0
!     ALLTYPE%PARTITION%FORMAT   =  T
!
!     ITYPE(3)%TRACK%FORMAT      =  F
! -------------------------------------------------------------------- !
&OUTPUT_TYPE_NML
EOF
if [ "$buoys" != 'no' ] ; then
  echo "  ALLTYPE%POINT%NAME     = '$buoys'" >> ww3_multi.nml
fi
if [ "$dt_po" != "0" ]
then
  if [ ! -e $path_d/$pt_file ]
  then
    echo "   [ERROR] no $pt_file in $path_d"
    exit 1
  else
    cp $path_d/$pt_file $path_w/
  fi
fi
cat >> ww3_multi.nml << EOF
  ALLTYPE%POINT%FILE     = '$pt_file'
  ALLTYPE%FIELD%LIST     = '$fields'
/
EOF

incmod=0
for grid in $mods
do
  incmod=$(expr $incmod + 1)
  # field
  field=$(eval echo "\${fields_$incmod}")
  if [ ! -z "$(echo $field)" ] ; then
    echo "ITYPE($incmod)%FIELD%LIST    = '$field'" >> ww3_multi.nml
  fi
done

cat >> ww3_multi.nml << EOF
! -------------------------------------------------------------------- !
! Define output dates via OUTPUT_DATE_NML namelist
!
! * index I must match indexes from 1 to DOMAIN%NRGRD
! * ALLDATE will apply the output dates for all the model grids
! * IDATE(I) will apply the output dates for the model grid number i
! * start and stop times are with format 'yyyymmdd hhmmss'
! * if time stride is equal '0', then output is disabled
! * time stride is given in seconds
! * it is possible to overwrite a global output date for a given grid
!
! * namelist must be terminated with /
! * definitions & defaults:
!     ALLDATE%FIELD%START         =  '19680606 000000'
!     ALLDATE%FIELD%STRIDE        =  '0'
!     ALLDATE%FIELD%STOP          =  '19680607 000000'
!     ALLDATE%POINT%START         =  '19680606 000000'
!     ALLDATE%POINT%STRIDE        =  '0'
!     ALLDATE%POINT%STOP          =  '19680607 000000'
!     ALLDATE%TRACK%START         =  '19680606 000000'
!     ALLDATE%TRACK%STRIDE        =  '0'
!     ALLDATE%TRACK%STOP          =  '19680607 000000'
!     ALLDATE%RESTART%START       =  '19680606 000000'
!     ALLDATE%RESTART%STRIDE      =  '0'
!     ALLDATE%RESTART%STOP        =  '19680607 000000'
!     ALLDATE%BOUNDARY%START      =  '19680606 000000'
!     ALLDATE%BOUNDARY%STRIDE     =  '0'
!     ALLDATE%BOUNDARY%STOP       =  '19680607 000000'
!     ALLDATE%PARTITION%START     =  '19680606 000000'
!     ALLDATE%PARTITION%STRIDE    =  '0'
!     ALLDATE%PARTITION%STOP      =  '19680607 000000'
!     
!     ALLDATE%RESTART             =  '19680606 000000' '0' '19680607 000000'
!
!     IDATE(3)%PARTITION%START    =  '19680606 000000' 
! -------------------------------------------------------------------- !
&OUTPUT_DATE_NML
  ALLDATE%FIELD               = '$startout' '$dt_mwp' '$stopout'
  ALLDATE%POINT               = '$startout' '$dt_po'  '$stopout'
  ALLDATE%TRACK               = '$startout' '$dt_oat' '$stopout'
  ALLDATE%RESTART             = '$startres' '$dt_rst' '$stopres'
  ALLDATE%BOUNDARY            = '$startout' '$dt_bd'  '$stopout'
  ALLDATE%PARTITION           = '$startout' '$dt_p'   '$stopout'
/



! -------------------------------------------------------------------- !
! Define homogeneous input via HOMOG_COUNT_NML and HOMOG_INPUT_NML namelist
!
! * the number of each homogeneous input is defined by HOMOG_COUNT
! * the total number of homogeneous input is automatically calculated
! * the homogeneous input must start from index 1 to N
! * if VALUE1 is equal 0, then the homogeneous input is desactivated
! * NAME can only be MOV
! * each homogeneous input is defined over a maximum of 3 values detailled below :
!     - MOV is defined by speed and direction
!
! * namelist must be terminated with /
! * definitions & defaults:
!     HOMOG_COUNT%N_MOV             =  0
!
!     HOMOG_INPUT(I)%NAME           =  'unset'
!     HOMOG_INPUT(I)%DATE           =  '19680606 000000'
!     HOMOG_INPUT(I)%VALUE1         =  0
!     HOMOG_INPUT(I)%VALUE2         =  0
!     HOMOG_INPUT(I)%VALUE3         =  0
! -------------------------------------------------------------------- !
&HOMOG_COUNT_NML
/

&HOMOG_INPUT_NML
/


! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !

EOF
chmod 775 ww3_multi.nml
}


###########################################################
# Prepares restart file and start/stop dates              #
###########################################################

  # rename restart in old and restart001 in restart
  cd $path_w
  if [ ! -z "$(ls -1 restart001.* 2>/dev/null )" ] ; then
    reslist=$(ls -1 restart001.*)
    for res001 in $reslist
    do
      ext=$(echo $res001 | cut -d"." -f2)
      if [ -e restart.$ext ] ; then
        mv restart.$ext restart_old.$ext
      fi
      mv $res001 restart.$ext
    done
  fi

  # save startrun 
  startrunformatini=$startrunformat

  # loop on mods to check mod_def and restart files consistency
  # and define the start date of the run
  for mod in $mods
  do
    # force mod to mod1
    if [ "$run_type" == "shel" ]
    then
      mod="$(echo $mods | cut -d ' ' -f1)"
    fi

    echo "*** restart $mod ***"
    cd $path_w
    rm -f restart.ww3

    # initialize startrun for mod
    startrunformat=$startrunformatini

    # link mod_def.ww3 if necessary
    if [ ! -e mod_def.$mod ]
    then
      echo "   [ERROR] no mod_def.$mod found"
      exit 1
    else
      if [ "$run_type" == "shel" ]
      then
        ln -sfn mod_def.$mod mod_def.ww3
      fi
    fi

    # existing restart file
    if [ -e restart.$mod ]
    then
      echo "   [INFO] mod_def.$mod and restart.$mod found"
      # get revision number
      rev=$(echo ${path_e##*/} | cut -d '_' -f2 | cut -c4-8)
      # check if rev is a number
      if [ $(echo $rev | egrep -q '^[0-9]+$') ] 
      then
        if [ $rev -lt 1420 ]
        then
          oldrestart=$($path_te/get_restart_date $mod)
        else
          oldrestart=$($path_te/get_restart_date_new_rev $mod)
        fi
      else
        oldrestart=$($path_te/get_restart_date_new_rev $mod)
      fi
      oldrestart="${oldrestart:0:8} $(printf "%06d\n" ${oldrestart:9:15})"
      echo "   [INFO] oldrestart = $oldrestart"

      # shift the startrun 15min before to have initialized values at output start date   
      startrun=$(date -u -d "$startrunformat -15min" +%Y%m%d\ %H%M%S)
      startrunformat=$(date -d "$startrunformat -15min" -uR)
      echo "   [INFO] startrun = $startrun"

      if [ "${oldrestart}" != "${startrun}" ] ; then
        echo "   [WARNING] restart date $oldrestart different from start date $startrun"
        echo "             The bad restart file will be removed and the run will start a few days before"
        rm -f restart.$mod restart.ww3
      else
        if [ "$run_type" == "shel" ] ; then
          cp restart.$mod restart.ww3
        fi
      fi

    fi

    # no restart file
    if [ ! -e restart.$mod ]
    then
      echo "   [WARNING] no restart.$mod"
      echo "             The run will start a few days before"
      # spin up days if calm conditions
      startrun=$(date -u -d "$startrunformat -${spinup_days}days" +%Y%m%d\ %H%M%S)
      startrunformat=$(date -d "$startrunformat -${spinup_days}days" -uR)
      echo "   [INFO] startrun = $startrun"
    fi

  done # loop on mods

  echo "*** dt_rst $dt_rst ***"
  # shift the start restart to be consistent with the next start run
  if [ $dt_rst -eq 0 ] ; then
    startres=$startout
  elif [ $dt_rst -lt 0 ] ; then
    startres=$(date -u -d "$stoprunformat ${dt_rst}sec" +%Y%m%d\ %H%M%S)
    dt_rst=$(echo $dt_rst | cut -c2-)
  elif [ $dt_rst -gt 0 ] ; then
    startres=$(date -u -d "$startrunformatini +${dt_rst}sec" +%Y%m%d\ %H%M%S)
  fi
  if [ "$unique_rst" == 'yes' ] ; then
    stopres=$startres
  else
    stopres=$stopout
  fi


  # exclusive stop run and output - remove one minute
  stoprun=$(date -u -d "$stoprunformat -1min" +%Y%m%d\ %H%M%S)
  stoprunformat=$(date -d "$stoprunformat -1min" -uR)
  stopout=$stoprun





###########################################################
# Checks for track input file                             #
###########################################################

  cd $path_w
  if [ $dt_oat -gt 0 ] 
  then
    mod1="$(echo $mods | cut -d ' ' -f1)"
    echo "*** copy track_i.$mod1 in work dir ***"
    if [ -e $path_d/track_i.$mod1 ]
    then
      if [ "$run_type" == "multi" ] ; then
        cp $path_d/track_i.$mod1  $path_w/track_i.$mod1
      elif [ "$run_type" == "shel" ] ; then
        cp $path_d/track_i.$mod1  $path_w/track_i.ww3
      fi 
    else
      echo "   [ERROR] no track_i.$mod1 in $path_d"
      exit 1
    fi
  fi

###########################################################
# Processes hindcast                                      #
###########################################################

  #check for native preprocessing (CURV or UNST in multi)
  declare -A native
  imod=1
  for mod in $mods
  do
    native[$imod]=false
    if [ "$run_type" == "multi" ] ; then
#      if [ ! -z "$(grep 'GRID%TYPE' $path_w/ww3_grid.*.$mod | grep 'CURV')" ] || \
#         [ ! -z "$(grep 'GRID%TYPE' $path_w/ww3_grid.*.$mod | grep 'UNST')" ] ; then
        native[$imod]=true
#      fi
    fi
    echo "native $imod : ${native[$imod]}"
    imod=$(($imod+1))
  done

  # making of ww3_shel.inp/ww3_multi.nml
  echo "*** ww3_$run_type ***"
  cd $path_w
  rm -f ww3_$run_type && cp $path_e/ww3_$run_type . && chmod 775 ww3_$run_type
  rm -f ww3_$run_type.inp
  if [ -e $path_d/ww3_${run_type}.nml ] ; then
    cp $path_d/ww3_${run_type}.nml .
  else
    eval make_$run_type
    eval make_${run_type}_inp
  fi
  echo -n "ww3_${run_type}... "
  if ! $MPI_LAUNCH ./ww3_$run_type >& $path_o/ww3_${run_type}_${logdate}.out
  then errmsg "Error occured during ww3_${run_type}";  exit 1;
  else echo "OK"; fi

  # rename output ww3 rst grd pnt files
  if [ "$run_type" == "shel" ] ; then
    mod1="$(echo $mods | cut -d ' ' -f1)"
    if [ -e $path_w/out_grd.ww3 ] ; then
      mv $path_w/out_grd.ww3 $path_w/out_grd.$mod1
    fi
    if [ -e $path_w/out_pnt.ww3 ] ; then
      mv $path_w/out_pnt.ww3 $path_w/out_pnt.$buoys
    fi
    if [ -e $path_w/restart.ww3 ] ; then
      mv $path_w/restart.ww3 $path_w/restart.$mod1
    fi
    if [ -e $path_w/restart001.ww3 ] ; then
      mv $path_w/restart001.ww3 $path_w/restart001.$mod1
    fi
    if [ -e $path_w/track_o.ww3 ] ; then
      mv $path_w/track_o.ww3 $path_w/track_o.$mod1
    fi
  fi

###########################################################
# Manages errors                                          #
###########################################################

  # creates a xml directory
  mkdir -p $path_w/xml && rm -rf $path_w/xml/*

  # stores environment variables for xml generation
  echo $path_w        > $path_w/xml/varfile    #path work
  echo $path_w/xml   >> $path_w/xml/varfile    #out dir
  echo $run_tag      >> $path_w/xml/varfile    #zone
  echo $dt_mwp       >> $path_w/xml/varfile    #delta time step output grid
  echo $dt_po        >> $path_w/xml/varfile    #delta time step output point
  echo $dt_rst       >> $path_w/xml/varfile    #delta time step restart


  #log filenames
  if [ "$run_type" == "multi" ] ; then
    mod1=$(echo $mods | cut -d ' ' -f1)
    logWW3=log.$mod1
  elif [ "$run_type" == "shel" ] ; then
    logWW3=log.ww3
  fi
  if [ ! -e $path_w/$logWW3 ]
  then
    echo "   [ERROR] log file $logWW3 does not exist in $path_w"
    exit 1
  fi

  # parses log WW3 file
  echo "*** check log file ***"
  conda activate $tool_path/conda-env/waverun
  python $tool_py_path/xml_logWW3.py $path_w/xml/varfile $logWW3 >& $path_o/xml_logWW3.out
  conda deactivate

  # gets xml output file
  logXML=$(ls -1 $path_w/xml/log*.xml)
  echo "   [INFO] logXML : $logXML"

  # gets status result
  statusline=$(cat $logXML | grep status)

  # checks if status is ok or not
  echo "*** check xml ***"
  if [ ! -z "$(echo $statusline | grep 'KO')" ]
  then
   echo "   [ERROR] xml status is KO "
   echo "           see $logXML"
   exit 1
  fi

  #check if some errors occured during process
  echo "*** check log ***"
  $waverun_path/check_error.sh $path_o

  # moves results to a final work directory
  echo "*** transfert results to ${path_w}${logdate} ***"
  rm -rf ${path_w}${logdate}
  mkdir ${path_w}${logdate}
  cp -r output ${path_w}${logdate}/
  #rm output/*${logdate}*.out
  cp mod_def.* ${path_w}${logdate}/
  cp *.inp* ${path_w}${logdate}/
  cp *.nml* ${path_w}${logdate}/
  mv $logXML ${path_w}${logdate}/
  mv log.* ${path_w}${logdate}/
  mv out_* ${path_w}${logdate}/
  for mod in $mods; do
    if [ -e restart001.$mod ] ; then
      startrestmp=$(echo ${startres} | sed 's/ //g')
      mv restart001.$mod ${path_w}${logdate}/restart_${startrestmp}.$mod
    fi
    if [ -e restart.$mod ] ; then
      startrestmp=$(echo ${startres} | sed 's/ //g')
      mv restart.$mod ${path_w}${logdate}/restart_${startrestmp}.$mod
    fi
  done
  if [ $dt_oat -gt 0 ]
  then
    cp track_i.* ${path_w}${logdate}/
    mv track_o.* ${path_w}${logdate}/
  fi



echo "!END OF HINDCAST!"
