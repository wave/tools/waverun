#!/bin/bash -e


path_o=$1
exitcode=0
for log in $(find ${path_o}/ -type f -name "*.o*")
do
  #echo "   $log"
  if [ ! -z "$(grep -ai 'error ' $log)" ] || \
     [ ! -z "$(grep -ai 'No Such ' $log)" ] || \
     [ ! -z "$(grep -ai 'SIGSEGV ' $log)" ] || \
     [ ! -z "$(grep -ai 'illegal ' $log)" ] || \
     [ ! -z "$(grep -ai 'Permission ' $log)" ] || \
     [ ! -z "$(grep -ai 'cannot ' $log)" ] || \
     [ ! -z "$(grep -ai 'exceeds ' $log)" ] || \
     [ ! -z "$(grep -ai 'severe ' $log)" ] || \
     [ ! -z "$(grep -ai 'killed ' $log)" ] || \
     [ ! -z "$(grep -ai 'abort ' $log)" ] || \
     [ ! -z "$(grep -ai 'does not exist ' $log)" ] || \
     [ ! -z "$(grep -ai 'aborting ' $log)" ]
  then
    echo " ERROR : check $log for more info"
    exitcode=1
  fi
done

if [ $exitcode -eq 1 ]; then
  exit 1
else
  echo 'all good'
fi
