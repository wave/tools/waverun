#!/bin/bash -e

# Script to be run before any of the toolbox's steps.
# Checks the settings in wavesetup.env

#input argument
if [ $# -lt 1 ]
then
  echo 'need one argument:'
  echo '$1 : path to wavesetup.env'
  exit 1
else
  dir=$1
fi

source $dir/wavesetup.env

if [ "$ncvers" != "3" ] && [ "$ncvers" != "4" ]
then
  echo "[ERROR] : in wavesetup.env,'ncvers' must be equal to 3 or 4. Please choose a netcdf version for your output files"
  exit 1
fi



if [ "$mods" = "ww3" ]
then
  echo "[ERROR] : in wavesetup.env,'mods' must be different to 'ww3'. Please choose another name for your model domain"
  exit 1
fi

if [ "$buoys" = "ww3" ]
then
  echo "[ERROR] : in wavesetup.env,'buoys' must be different to 'ww3'. Please choose another model file extension"
  exit 1
fi


if [ "$run_type" = "multi" ] 
then
  for forcing in wind ice level current ice1 ice2 ice3 ice4 ice5 mud1 mud2 mud3
  do
    val=$(echo $(eval echo "\${$forcing}"))
    if [ ! -z "$(echo $val | grep '^[0-9][0-9]*[.][0-9][0-9]*$')" ]
    then
      echo "[ERROR] : in wavesetup.env,'$forcing' must not be a constant in multi grid implementation. Please choose another forcing field or 'no'"
      exit 1
    fi
  done
fi

if [ "$betamax" = '' ]
then
  echo "[ERROR] : in wavesetup.env, you must set a value for 'betamax'."
  echo "If the variable does not exist in your wavesetup.env file yet, create it:"
  echo "below the line 'run_date=...', write:"
  echo 'betamax= [VALUE]            # value for BETAMAX in ww3_grid.inp.$mod'
  exit 1
fi

if [ "unique_rst" = '' ]
then
  echo "[ERROR] : in wavesetup.env, you must set 'yes' or 'no' for unique_rst."
  echo "If the restart is unique it will produce only one restart file at time stride define by dt_rst"
  exit 1
fi
