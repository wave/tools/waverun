#!/bin/bash -e

# -------------------------------------------------------------------- #
# Waverun toolbox : process model forcing preprocessing                #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : GPLv3                                                      #
# Creation date : 01-Dec-2011                                          #
# Modification date : 07-Jun-2021                                      #
# -------------------------------------------------------------------- #


###########################################################################################
#   process forcing for a selected time span defined in argument                          #
#                                                                                         #
# need to be in main_dir to execute the script                                            #
#                                                                                         #
# example :                                                                               #
#  qsub -v startrun='20161101 000000',stoprun='20161201 000000'                           #
#       -lmem=20G -l walltime=04:00:00                                                    #
#       -N GLOBforc ~/TOOLS/SCRIPTS/HINDCAST/process_forcing.sh                           #
#                                                                                         #
# additional bashrc file can be sourced if defined by $WAVERUN_PROFIL (with -v for qsub)  #
###########################################################################################


#----------------------
## 1. INPUT ARGUMENTS
#----------------------

# shell arguments
if [ $# -ge 2 ] ; then
  startrun=$1  # '20170314 000000'
  stoprun=$2   # '20170320 000000'
fi

# qsub arguments
if [ -z "$(echo $startrun)" ] || [ -z "$(echo $stoprun)" ]; then
  echo ''
  echo '[ERROR] missing startrun,stoprun arguments in qsub command'
  echo "[EXAMPLE] qsub -v startrun='20161101 000000',stoprun='20161201 000000' process_forcing.sh"
  exit 1
fi

# start and stop dates
echo "startrun: $startrun"
echo "stoprun: $stoprun"

startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -uR)"

stopyear="$(echo $stoprun | cut -c1-4)"
stopmonth="$(echo $stoprun | cut -c5-6)"
stopday="$(echo $stoprun | cut -c7-8)"
stophour="$(echo $stoprun | cut -c10-11)"
stopmin="$(echo $stoprun | cut -c12-13)"
stopsec="$(echo $stoprun | cut -c14-15)"
stoprunformat="$(date -d "${stopyear}-${stopmonth}-${stopday} ${stophour}:${stopmin}:${stopsec}" -uR)"



#----------------------
## 2. ENVIRONMENT
#----------------------

# pbs environment
if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  cd $PBS_O_WORKDIR
fi

# wavepath environment
if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
else  echo 'no wavepath.env found in $PWD or $HOME' ; ls $PWD ; exit 1 ; fi
echo "source $wavepath" && source $wavepath

# additional environment
if [ ! -z "$(echo $WAVERUN_PROFIL)" ] ; then
  echo "source $WAVERUN_PROFIL" && source $WAVERUN_PROFIL
fi

# mpi environment
if [ -z "$(echo $MPI_LAUNCH)" ] ; then
  MPI_LAUNCH=mpiexec
fi
echo "MPI_LAUNCH : $(which $MPI_LAUNCH)"

# wavesetup environment
echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env

# Error message function
errmsg ()
{
  echo "" 2>&1
  while [ $# != 0 ]
  do
    echo "ERROR: $1" 2>&1
    shift
  done
  echo "" 2>&1
}


#----------------------
## 3. INIT & CHECK
#----------------------

# check wavesetup.env
$waverun_path/check_setup.sh $main_dir

# spin up days from calm conditions
startrun=$(date -u -d "$startrunformat -${spinup_days}days" +%Y%m%d\ %H%M%S)
startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -uR)"
echo "updated startrun: $startrun"

# set dates
logdate="${startyear}-${startmonth}-${startday}T${starthour}_${stopyear}-${stopmonth}-${stopday}T${stophour}"

# check run dates consistency
if [ "$startyear" == "$stopyear" ] ; then
  year=$startyear
else
  if [ -e $main_dir/date_cycle ] ; then
    echo "[INFO] no check done on overlapping years for forecast system"
    year=$stopyear
  else
    if [ $(expr $stopyear - $startyear) -gt 2 ] ; then
      echo "   [ERROR] It is not possible to run over more than one year. Reduce the duration."
      exit 1
    elif [ $(expr $stopyear - $startyear) -eq 2 ] || [ $startmonth -le 11 ] ; then
      if [ $stopmonth -ne 1 ] || [ $stopday -ne 1 ] ; then
        echo "   [ERROR] It is not possible to run over two different years. Reduce the duration."
        exit 1
      fi
    fi
    # set year for forcing
    if [ $stopmonth -eq 1 ] && [ $stopday -eq 1 ] ; then
      year=$(expr $stopyear - 1)
    else
      year=$stopyear
    fi
  fi
fi

# make work directory
mkdir -p $path_w
ln -sfn $path_w $run_path/$run_tag/work

# create output path
echo "   [INFO] output path : $path_o"
echo "   [INFO] work path : $path_w"
mkdir -p $path_o $path_w
cd $path_w

#check if some errors occured during last process
echo "*** check past log ***"
if [ ! -z "$(ls -A $path_o 2> /dev/null)" ] ; then
  $waverun_path/check_error.sh $path_o
fi

#----------------------
## 4. PROGRAM
#----------------------

# remove online nest
for nestdir in $nests_ww3
do
 rm -f $work_path/$nestdir/work/nest.ww3
done

#wind forcing provider
if [ "$wind" == "wind_cfsr" ] 
then
  WIND="$CFSR"
  if [ $year -le 2010 ]
  then
    reswind="_031"
  else
    reswind="_020"
  fi
elif [ "$wind" == "wind_gfs" ]
then
	WIND="$GFS"
elif [ "$wind" == "wind_ecmwf" ]
then
  WIND="$ECMWF"
  if [ $year -le 2008 ]
  then
    reswind="_050"
  elif [ $year -le 2019 ]
  then
    reswind="_025"
  else
    reswind="_010"
  fi
elif [ "$wind" == "wind_ecmwf_op" ]
then
  WIND="$ECMWF_OP"
  reswind="_025"
elif [ "$wind" == "wind_arome" ]
then
  WIND="$AROME"
  reswind="_002"
elif [ "$wind" == "wind_er20c" ]
then
  WIND="$ER20C"
  reswind="_112"
elif [ "$wind" == "wind_era5" ]
then
  WIND="$ERA5"
  reswind="_025"
elif [ "$wind" == "wind_blended" ]
then
  WIND="$BLENDED"
  reswind="_025"
elif [ "$wind" == "wind_blended3h" ]
then
  WIND="$BLENDED3H"
  reswind="_025"
elif [ "$wind" == "wind_cmip6_aer" ]
then
  WIND="$CMIP6_AER"
  reswind="no"
elif [ "$wind" == "wind_cmip6_ctl" ]
then
  WIND="$CMIP6_CTL"
  reswind="no"
elif [ "$wind" == "wind_cmip6_nat" ]
then
  WIND="$CMIP6_NAT"
  reswind="no"
elif [ "$wind" == "wind_cmip6_ghg" ]
then
  WIND="$CMIP6_GHG"
  reswind="no"
elif [ "$wind" == "wind_cmip6_all" ]
then
  WIND="$CMIP6_ALL"
  reswind="no"
elif [ "$wind" == "wind_local" ]
then
  WIND="$path_d"
  reswind=""
elif [ ! -z "$(echo $wind | grep '^[0-9][0-9]*[.][0-9][0-9]*$')" ]
then
  WIND="cst"
  reswind="no"
elif [ "$wind" == "no" ]
then
  WIND="no"
  reswind="no"
else
  echo "[ERROR] wind value in wavesetup.env not recognized $wind [no|wind_ecmwf|wind_ecmwf_op|wind_cfsr|wind_gfs|wind_arome|wind_er20c|wind_local]"
  exit 1
fi


#ice forcing provider 
if [ "$ice" == "ice_cersat" ] 
then
  ICE="$CERSAT"
  resice="_008"
elif [ "$ice" == "ice_cfsr" ]
then
  ICE="$CFSR"
  if [ $year -le 2009 ]
  then
    resice="_031"
  elif [ $year -eq 2010 ]  
  then
    resice="_093"
  elif [ $year -ge 2011 ]
  then
    resice="_020"
  fi
elif [ "$ice" == "ice_ncep_omb" ]
then
  ICE="$NCEP_OMB"
  resice="_008"
elif [ "$ice" == "ice_topaz4" ]
then
  ICE="$TOPAZ4"
    resice=""
elif [ "$ice" == "ice_ssmi" ]
then
  ICE="$SSMI"
  resice=""
elif [ "$ice" == "ice_er20c" ]
then
  ICE="$ER20C"
  resice="_112"
elif [ "$ice" == "ice_era5" ]
then
  ICE="$ERA5"
  resice="_025"
elif [ "$ice" == "ice_glob_merca_for" ]
then
  ICE="$MERCA_GLOB_PHY_FOR"
  resice="_008"
elif [ "$ice" == "ice_mitgcm_glob" ]
then
  ICE="$MITGCM_GLOB"
elif [ "$ice" == "ice_mitgcm_23w" ]
then
  ICE="$MITGCM_23W"
elif [ "$ice" == "ice_mitgcm_agulhas" ]
then
  ICE="$MITGCM_AGULHAS"
elif [ "$ice" == "ice_mitgcm_bfr" ]
then
  ICE="$MITGCM_BFR"
elif [ "$ice" == "ice_mitgcm_brnt" ]
then
  ICE="$MITGCM_brnt"
elif [ "$ice" == "ice_mitgcm_drake" ]
then
  ICE="$MITGCM_DRAKE"
elif [ "$ice" == "ice_mitgcm_equa" ]
then
  ICE="$MITGCM_EQUA"
elif [ "$ice" == "ice_mitgcm_fram" ]
then
  ICE="$MITGCM_FRAM"
elif [ "$ice" == "ice_mitgcm_gasc" ]
then
  ICE="$MITGCM_GASC"
elif [ "$ice" == "ice_mitgcm_gulfz" ]
then
  ICE="$MITGCM_GULFZ"
elif [ "$ice" == "ice_cmip6_aer" ]
then
  ICE="$CMIP6_AER"
elif [ "$ice" == "ice_cmip6_ctl" ]
then
  ICE="$CMIP6_CTL"
elif [ "$ice" == "ice_cmip6_nat" ]
then
  ICE="$CMIP6_NAT"
elif [ "$ice" == "ice_cmip6_ghg" ]
then
  ICE="$CMIP6_GHG"
elif [ "$ice" == "ice_cmip6_all" ]
then
  ICE="$CMIP6_ALL"
elif [ "$ice" == "ice_local" ]
then
  ICE="$path_d"
  resice=""
elif [ "$ice" == "no" ]
then
  ICE="no"
  resice="no"
else
  echo "[ERROR] ice value in wavesetup.env not recognized $ice [no|ice_cersat|ice_cfsr|ice_ncep_omb|ice_topaz4|ice_ssmi|ice_er20c|ice_era5|ice_local]"
  exit 1
fi

#ice1 forcing provider 
if [ "$ice1" == "ice1_topaz4" ]
then
  ICE1="$TOPAZ4"
  resice1=""
elif [ "$ice1" == "ice1_smos" ]
then
  ICE1="$SMOS"
  resice1="_012"
elif [ "$ice1" == "ice1_mitgcm_glob" ]
then
  ICE1="$MITGCM_GLOB"
elif [ "$ice1" == "ice1_mitgcm_23w" ]
then
  ICE1="$MITGCM_23W"
elif [ "$ice1" == "ice1_mitgcm_agulhas" ]
then
  ICE1="$MITGCM_AGULHAS"
elif [ "$ice1" == "ice1_mitgcm_bfr" ]
then
  ICE1="$MITGCM_BFR"
elif [ "$ice1" == "ice1_mitgcm_brnt" ]
then
  ICE1="$MITGCM_brnt"
elif [ "$ice1" == "ice1_mitgcm_drake" ]
then
  ICE1="$MITGCM_DRAKE"
elif [ "$ice1" == "ice1_mitgcm_equa" ]
then
  ICE1="$MITGCM_EQUA"
elif [ "$ice1" == "ice1_mitgcm_fram" ]
then
  ICE1="$MITGCM_FRAM"
elif [ "$ice1" == "ice1_mitgcm_gasc" ]
then
  ICE1="$MITGCM_GASC"
elif [ "$ice1" == "ice1_mitgcm_gulfz" ]
then
  ICE1="$MITGCM_GULFZ"
elif [ "$ice1" == "ice1_local" ]
then
  ICE1="$path_d"
  resice1=""
elif [ ! -z "$(echo $ice1 | grep '^[0-9][0-9]*[.][0-9][0-9]*$')" ]
then
  ICE1="cst"
  resice1="no"
elif [ "$ice1" == "no" ]
then
  ICE1="no"
  resice1="no"
else
  echo "[ERROR] ice1 value in wavesetup.env not recognized $ice1 [no|ice1_topaz4|ice1_smos|ice1_local|0.30]"
  exit 1
fi


#cur forcing provider
if [ "$cur" == "cur_har" ]
then
  CUR="$HARMONICS"
elif [ "$cur" == "cur_rscd" ]
then
  CUR="$RSCD/$mods"
elif [ "$cur" == "cur_fes" ]
then
  CUR="$FES2014/$mods"
elif [ "$cur" == "cur_mars" ]
then
  CUR="$MARS"
elif [ "$cur" == "cur_croco" ]
then
  CUR="$CROCO"
elif [ "$cur" == "cur_mercator" ]
then
  CUR="$MERCATOR"
  rescur="_033"
elif [ "$cur" == "cur_cmemsgc" ]
then
  CUR="$CMEMSGLOBCUR"
  rescur="_025"
elif [ "$cur" == "cur_cmemsgcnrt" ]
then
  CUR="$CMEMSGLOBCURNRT"
  rescur="_025"
elif [ "$cur" == "cur_gcgeo" ]
then
  CUR="$GLOBCURGEO"
  rescur="_025"
elif [ "$cur" == "cur_gctot" ]
then
  CUR="$GLOBCURTOT"
  rescur="_025"
elif [ "$cur" == "cur_mctot" ]
then
  CUR="$MEDCURTOT"
  rescur="_012"
elif [ "$cur" == "cur_glob_merca_for" ]
then
  CUR="$MERCA_GLOB_PHY_FOR"
  rescur="_033"
elif [ "$cur" == "cur_mitgcm_glob" ]
then
  CUR="$MITGCM_GLOB"
elif [ "$cur" == "cur_mitgcm_23w" ]
then
  CUR="$MITGCM_23W"
elif [ "$cur" == "cur_mitgcm_agulhas" ]
then
  CUR="$MITGCM_AGULHAS"
elif [ "$cur" == "cur_mitgcm_bfr" ]
then
  CUR="$MITGCM_BFR"
elif [ "$cur" == "cur_mitgcm_brnt" ]
then
  CUR="$MITGCM_brnt"
elif [ "$cur" == "cur_mitgcm_drake" ]
then
  CUR="$MITGCM_DRAKE"
elif [ "$cur" == "cur_mitgcm_equa" ]
then
  CUR="$MITGCM_EQUA"
elif [ "$cur" == "cur_mitgcm_fram" ]
then
  CUR="$MITGCM_FRAM"
elif [ "$cur" == "cur_mitgcm_gasc" ]
then
  CUR="$MITGCM_GASC"
elif [ "$cur" == "cur_mitgcm_gulfz" ]
then
  CUR="$MITGCM_GULFZ"
elif [ "$cur" == "cur_glorys" ]
then
  CUR="$GLORYS"
elif [ "$cur" == "cur_local" ]
then
  CUR="$path_d"
elif [ "$cur" == "no" ]
then
  CUR="no"
else
  echo "[ERROR] cur value in wavesetup.env not recognized $cur [no|cur_har|cur_fes|cur_mars|cur_croco|cur_mercator|cur_gcgeo|cur_gctot|cur_mctot|cur_local]"
  exit 1
fi


#lev forcing provider
if [ "$lev" == "lev_har" ]
then
  LEV="$HARMONICS"
elif [ "$lev" == "lev_rscd" ]
then
  LEV="$RSCD/$mods"
elif [ "$lev" == "lev_fes" ]
then
  LEV="$FES2014/$mods"
elif [ "$lev" == "lev_mars" ]
then
  LEV="$MARS"
elif [ "$lev" == "lev_croco" ]
then
  LEV="$CROCO"
elif [ "$lev" == "lev_mercator" ]
then
  LEV="$MERCATOR"
elif [ "$lev" == "lev_mitgcm_glob" ]
then
  LEV="$MITGCM_GLOB"
elif [ "$lev" == "lev_mitgcm_23w" ]
then
  LEV="$MITGCM_23W"
elif [ "$lev" == "lev_mitgcm_agulhas" ]
then
  LEV="$MITGCM_AGULHAS"
elif [ "$lev" == "lev_mitgcm_bfr" ]
then
  LEV="$MITGCM_BFR"
elif [ "$lev" == "lev_mitgcm_brnt" ]
then
  LEV="$MITGCM_brnt"
elif [ "$lev" == "lev_mitgcm_drake" ]
then
  LEV="$MITGCM_DRAKE"
elif [ "$lev" == "lev_mitgcm_equa" ]
then
  LEV="$MITGCM_EQUA"
elif [ "$lev" == "lev_mitgcm_fram" ]
then
  LEV="$MITGCM_FRAM"
elif [ "$lev" == "lev_mitgcm_gasc" ]
then
  LEV="$MITGCM_GASC"
elif [ "$lev" == "lev_mitgcm_gulfz" ]
then
  LEV="$MITGCM_GULFZ"
elif [ "$lev" == "lev_glorys" ]
then
  LEV="$GLORYS"
elif [ "$lev" == "lev_local" ]
then
  LEV="$path_d"
elif [ "$lev" == "no" ]
then
  LEV="no"
else
  echo "[ERROR] lev value in wavesetup.env not recognized $lev [no|lev_har|lev_fes|lev_mars|lev_croco|lev_mercator|lev_local]"
  exit 1
fi


#buoys output
if [ "$buoys" != 'no' ]
then
  BUOYS="$path_d"
fi


# calculates year - 1
yearm1=$(expr $year - 1 )
yearm1=$(printf "%04d\n" $yearm1)

# calculates year + 1
yearp1=$(expr $year + 1 )
yearp1=$(printf "%04d\n" $yearp1)

# select SPEC dir for forcing
if [ "$spec_zone" == "glob" ]
then
  SPEC_DIR=$GLOB_DIR
  case $wind in
    wind_cfsr)
      wtype="_CFSR"
      ;;
    wind_ecmwf)
      wtype="_ECMWF"
      ;;
    wind_era5)
      wtype="_ERA5"
      ;;
    *)
      echo "forcing $wind for global SPEC not recognized [CFSR|ECMWF|ERA5]"
      wtype=""
      ;;
  esac
  pastyear_specdir="$SPEC_DIR/${yearm1}${wtype}"
  curyear_specdir="$SPEC_DIR/${year}${wtype}"
  newyear_specdir="$SPEC_DIR/${yearp1}${wtype}"
  nextyear_specdir="$SPEC_DIR/${yearp1}${wtype}"

elif  [ "$spec_zone" == "med" ]
then
  SPEC_DIR=$MED_DIR
  wtype=""
  pastyear_specdir="$SPEC_DIR/${yearm1}${wtype}"
  curyear_specdir="$SPEC_DIR/${year}${wtype}"
  newyear_specdir="$SPEC_DIR/${yearp1}${wtype}"
  nextyear_specdir="$SPEC_DIR/${yearp1}${wtype}"

elif  [ "$spec_zone" == "local" ]
then
  if [ ! -z $(echo $LOCAL_DIR) ] ; then
    SPEC_DIR=$LOCAL_DIR
  else
    SPEC_DIR=$path_d/NEST
  fi
  wtype=""
  pastyear_specdir="$SPEC_DIR"
  curyear_specdir="$SPEC_DIR"
  newyear_specdir="$SPEC_DIR"
  nextyear_specdir="$SPEC_DIR"

elif  [ "$spec_zone" == "online" ]
then
  SPEC_DIR=""
  wtype=""
fi


# check glob or med spectral boundaries directories
#if [ "$spec_zone" == "glob" ] || [ "$spec_zone" == "med" ]
#then
#  if [ \( -d $pastyear_specdir/SPEC_NC \) -o \
#         \( -d $pastyear_specdir/SPEC_SE \) ] && \
#       [ \( -d $curyear_specdir/SPEC_NC \) -o \
#         \( -d $curyear_specdir/SPEC_SE \) ]
#  then
#    echo '[INFO] netcdf spectral boundaries'
#  else
#    echo '[ERROR] no SPEC_* for forcing the boundary !'
#    echo "  $SPEC_DIR/${yearm1}${wtype}/SPEC_* may be empty"
#    echo "  $SPEC_DIR/${year}${wtype}/SPEC_* may be empty"
#    exit 1
#  fi
#fi # glob || med




# initialize spectral boundaries
if [ "$spec_zone" != "no" ] && [ "$spec_zone" != "online" ] 
then 
  if [ ! -e $SPEC_DIR ] ; then
    echo "[ERROR] $SPEC_DIR does not exist"
    exit 1
  fi
  mkdir -p $path_w/NEST; rm -f $path_w/NEST/*
  mod1=$(echo $mods | cut -d ' ' -f1)

  # define ww3_bounc.inp / ww3_bounc.nml
  if [ -e $path_d/spec_${mod1}.list ]
  then
    echo "[INFO] spec_${mod1}.list used from $path_d"
    sed -e "s/<daterun>/${logdate}/" -e "s/<project>/$project/" $path_d/spec_${mod1}.list > $path_w/NEST/spec_${mod1}.list
    if [ -e $path_d/ww3_bounc.nml.$mod1 ]
    then
      echo "[INFO] ww3_bounc.nml copied from $path_d/ww3_bounc.nml.$mod1"
      cp $path_d/ww3_bounc.nml.$mod1 $path_w/NEST/ww3_bounc.nml
    else
      echo "[INFO] ww3_bounc.nml automatically created from scratch"
      cd $path_w/NEST
      cat > ww3_bounc.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III ww3_bounc.nml - Boundary input post-processing         !
! -------------------------------------------------------------------- !

! -------------------------------------------------------------------- !
! Define the input boundaries to preprocess via BOUND_NML namelist
!
! * namelist must be terminated with /
! * definitions & defaults:
!     BOUND%MODE                 = 'WRITE'            ! ['WRITE'|'READ']
!     BOUND%INTERP               = 2                  ! interpolation [1(nearest),2(linear)]
!     BOUND%VERBOSE              = 1                  ! [0|1|2]
!     BOUND%FILE                 = 'spec.list'        ! input _spec.nc listing file
! -------------------------------------------------------------------- !
&BOUND_NML
  BOUND%FILE                 = 'spec_${mod1}.list'
/


! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF
    fi
  elif [ -e $path_d/ww3_bounc.inp.$mod1 ]
  then
    echo "[INFO] ww3_bounc.inp copied from $path_d/ww3_bounc.inp.$mod1"
    cp  $path_d/ww3_bounc.inp.$mod1  $path_w/NEST/ww3_bounc.inp
  else
    echo "[INFO] ww3_bounc.inp automatically created from all spec.nc file from $SPEC_DIR"
    cd $path_w/NEST
    cat > ww3_bounc.inp << EOF
$ -------------------------------------------------------------------- $
$ WAVEWATCH III Boundary input processing                              $
$ -------------------------------------------------------------------- $
$ boundary option: READ or WRITE
 WRITE
$
$ Interpolation method: 1: nearest
$                       2: linear interpolation
 2
$
$ verbose mode [0-1]
 0
$
$ list of spectra files:
$ list of spectra files. These netCDF files use the WAVEWATCH III format
$ examples of such files can be found at (for example):
$   http://tinyurl.com/iowagaftp/HINDCAST/NORGASUG/2010_ECMWF/SPEC_NC
$
EOF
          find $SPEC_DIR/ -name "*spec.nc" >> ww3_bounc.inp
          cat >> ww3_bounc.inp << EOF
$
'STOPSTRING'
$
$ -------------------------------------------------------------------- $
$ End of input file                                                    $
$ -------------------------------------------------------------------- $
EOF
  fi
fi


# get the list of spectral boundaries
if [ "$spec_zone" == "local" ] || [ "$spec_zone" == "glob" ] || [ "$spec_zone" == "med" ]
then
  if [ -e $path_d/spec_${mod1}.list ]
  then
    list_spec=$(cat $path_d/spec_${mod1}.list)
  elif [ -e $path_d/ww3_bounc.inp.$mod1 ]
  then
    list_spec=$(cat $path_d/ww3_bounc.inp.$mod1 | grep '\_spec.nc')
  fi

  # get specfiles to NEST directory (concat)
  for specfile in $list_spec
  do
    specfile=$(basename $specfile)

    # force tmprun at day1 of the month since +1month adds 31days
    # even if the month is only 30days so it can skip a month
    tmprunformat="$(date -d "${startyear}-${startmonth}-01 00:00:00" -uR)"
    tmprun=$(date -u -d "$tmprunformat" +%Y%m%d\ %H%M%S)
    tmpyear="$(echo $tmprun | cut -c1-4)"
    tmpmonth="$(echo $tmprun | cut -c5-6)"
    echo "init : $tmpyear$tmpmonth / $stopyear$stopmonth"

    allspeclist=''
    while [ $tmpyear$tmpmonth -le $stopyear$stopmonth ]
    do
      #echo $tmpyear$tmpmonth / $stopyear$stopmonth
      monthlyspecfiles=$(echo $specfile | sed -e s/\<daterun\>/${tmpyear}${tmpmonth}*/ | sed -e s/\<project\>/$project/)
      logdatespecfile=$(echo $specfile | sed -e s/\<daterun\>/$logdate/ | sed -e s/\<project\>/$project/)
      #echo "monthlyspecfiles : $monthlyspecfiles"
      if [ $tmpyear -eq $yearm1 ]
      then
        echo "concat past $tmpyear"
        allspeclist="$allspeclist $(find $pastyear_specdir/ -name "${monthlyspecfiles}" | sort -u)"
      elif [ $tmpyear -eq $year ]
      then
        echo "concat cur $tmpyear"
        allspeclist="$allspeclist $(find $curyear_specdir/ -name "${monthlyspecfiles}" | sort -u)"
        allspeclist="$allspeclist $(find $newyear_specdir/ -name "${monthlyspecfiles}" | sort -u)"
      elif [ $tmpyear -eq $yearp1 ]
      then
        echo "concat next $tmpyear"
        allspeclist="$allspeclist $(find $nextyear_specdir/ -name "${monthlyspecfiles}" | sort -u)"
      fi

      tmprunformat=$(date -u -d "$tmprunformat +1month" -uR)
      tmprun=$(date -u -d "$tmprunformat" +%Y%m%d\ %H%M%S)
      tmpyear="$(echo $tmprun | cut -c1-4)"
      tmpmonth="$(echo $tmprun | cut -c5-6)"
      echo "update : $tmpyear$tmpmonth -le $stopyear$stopmonth"
    done
    if [ ! -z "$(echo $allspeclist)" ]
    then
      ncrcat -h $allspeclist $path_w/NEST/$logdatespecfile
    else
      echo "[WARNING] no spec for boundaries $specfile"
    fi
  done # specfile


  # get specfiles to NEST directory (link or concat)
#  for specfile in $list_spec
#  do
#    specfile=$(basename $specfile)
#    pastyearmonthlyspecfiles=$(echo $specfile | sed -e s/DATERUN/${yearm1}12*/ | sed -e s/\<daterun\>/${yearm1}12*/ | sed -e s/\<project\>/$project/)
#    curyearmonthlyspecfiles=$(echo $specfile | sed -e s/DATERUN/${year}*/ | sed -e s/\<daterun\>/${year}*/ | sed -e s/\<project\>/$project/)
#    yearlyspecfile=$(echo $specfile | sed -e s/DATERUN/${year}/ | sed -e s/\<daterun\>/${year}/ | sed -e s/\<project\>/$project/)
#    echo "curyearmonthlyspecfiles : $curyearmonthlyspecfiles"
#    echo "yearlyspecfile : $yearlyspecfile"
#
#    # if yearly spec file exists, link it
#    echo "spec dir : $curyear_specdir/"
#    if [ ! -z "$(find $curyear_specdir/ -name "${yearlyspecfile}")" ]
#    then
#      echo "linking ${yearlyspecfile}"
#      ln -sfn $(find $curyear_specdir/ -name "${yearlyspecfile}") $path_w/NEST/
#      file $path_w/NEST/${yearlyspecfile}
#    # else concat monthly spec files
#    elif [ ! -z "$(find $curyear_specdir/ -name "${curyearmonthlyspecfiles}")" ]
#    then
#      echo "concatenating ${pastyearmonthlyspecfiles} ${curyearmonthlyspecfiles}"
#      ncrcat -h $(find $pastyear_specdir/ -name "${pastyearmonthlyspecfiles}" | sort) $(find $curyear_specdir/ -name "${curyearmonthlyspecfiles}" | sort)  $path_w/NEST/${yearlyspecfile}
#      file $path_w/NEST/${yearlyspecfile}
#    else
#    echo "[WARNING] no file ${curyearmonthlyspecfiles} found in $curyear_specdir/"
#    fi
#  done

fi



############
# ww3_grid #
############

# copy WW3 executable
rm -f $path_w/ww3_grid
cp $path_e/ww3_grid $path_w/
chmod 775 $path_w/ww3_grid
cd $path_w

# ww3_grid on model grids
for mod in $mods
do
  echo "*** ww3_grid $mod ***"
  if [ -e mod_def.$mod ] ; then
    echo "[INFO] mod_def.$mod already exists. It will not be regenerated"
  else
    if [ ! -z $mod ] && [ "$mod" != "no" ] ; then
      # input grid/mesh files
      if [ ! -z "$(ls -A $path_d/$mod.* 2> /dev/null)" ] ; then
        cp $path_d/$mod.* $path_w
        if [ -e $path_d/namelists_${mod}.nml ]; then
          sed -e "s/BETAMAX=betamax/BETAMAX = $betamax/" \
              -e "s/BETAMAX = betamax/BETAMAX = $betamax/"\
              -e "s/BETAMAX=<betamax>/BETAMAX = $betamax/"\
              -e "s/BETAMAX = <betamax>/BETAMAX = $betamax/"\
              $path_d/namelists_${mod}.nml > $path_w/namelists_${mod}.nml
          cp $path_d/ww3_grid.nml.$mod $path_w/ww3_grid.nml
        elif [ -e $path_d/ww3_grid.inp.$mod ] ; then
          sed -e "s/BETAMAX=betamax/BETAMAX = $betamax/" \
              -e "s/BETAMAX = betamax/BETAMAX = $betamax/"\
              -e "s/BETAMAX =<betamax>/BETAMAX = $betamax/"\
              -e "s/BETAMAX = <betamax>/BETAMAX = $betamax/"\
              $path_d/ww3_grid.inp.$mod > $path_w/ww3_grid.inp
        else
          echo "no ww3_grid.nml or ww3_grid.inp found for $mod"
          exit 1
        fi
        rm -f mod_def.ww3 
        echo ""
        echo "   Process $mod"  
        echo "   Screen output routed to $path_o/ww3_grid.$mod.out"
        echo -n "   $path_e/ww3_grid... "
        if ! $path_w/ww3_grid >& $path_o/ww3_grid.$mod.out
        then errmsg "Error occured during ww3_grid";  exit 1;
        else echo "OK"; fi
        rm -f mapsta.ww3 mask.ww3
        if [ -e ww3_grid.inp ]; then 
          mv ww3_grid.inp ww3_grid.inp.$mod
        else
          mv ww3_grid.nml ww3_grid.nml.$mod
        fi
        mv mod_def.ww3 mod_def.$mod
      else
        echo "[ERROR] : no grid/mesh data available in the data folder for domain $mod"
        exit 1
      fi # $path_d/$mod
    fi # ! -z $mod
  fi # mod_def.$mod
done


# ww3_grid on input grids
if  [ "$run_type" == "multi" ]
then
  for forc in buoys wind ice ice1 cur lev
  do
    echo "*** ww3_grid $forc ***"
    forcing=$(echo $(eval echo "\${$forc}"))
    resforc=$(echo "$(eval echo "\${res$forc}")")
    FORC="$(echo $forc | tr '[:lower:]' '[:upper:]')"
    FORCING=$(echo $(eval echo "\${$FORC}"))
    if [ "$forcing" != 'no' ] && [ "$forcing" != 'cst' ] && [ -z "$(echo $forcing | grep '^[0-9][0-9]*[.][0-9][0-9]*$')" ]
    then
      if [ -e mod_def.$forcing ] ; then
        echo "[INFO] mod_def.$forcing already exists. Il will not be regenerated."
      else
        if [ -e $path_d/namelists_${forcing}.nml ]; then
          cp $path_d/${forcing}.* $path_w
          sed -e "s/BETAMAX=betamax/BETAMAX = $betamax/" \
              -e "s/BETAMAX = betamax/BETAMAX = $betamax/"\
              -e "s/BETAMAX=<betamax>/BETAMAX = $betamax/"\
              -e "s/BETAMAX = <betamax>/BETAMAX = $betamax/"\
              $path_d/namelists_${forcing}.nml > $path_w/namelists_${forcing}.nml
        fi
        if [ -e $FORCING/namelists_${forcing}.nml ]; then
          cp $FORCING/${forcing}${resforc}.* $path_w
          sed -e "s/BETAMAX=betamax/BETAMAX = $betamax/" \
              -e "s/BETAMAX = betamax/BETAMAX = $betamax/"\
              -e "s/BETAMAX=<betamax>/BETAMAX = $betamax/"\
              -e "s/BETAMAX = <betamax>/BETAMAX = $betamax/"\
              $FORCING/namelists_${forcing}.nml > $path_w/namelists_${forcing}.nml
        fi
        if [ -e $path_d/ww3_grid.nml.${forcing} ] ; then
          cp $path_d/ww3_grid.nml.$forcing $path_w/ww3_grid.nml
        elif [ -e $FORCING/ww3_grid.nml.${forcing}${resforc} ] ; then
          cp $FORCING/ww3_grid.nml.${forcing}${resforc} $path_w/ww3_grid.nml
        elif [ -e $path_d/ww3_grid.inp.${forcing} ] ; then
          sed -e "s/BETAMAX=betamax/BETAMAX = $betamax/" \
              -e "s/BETAMAX = betamax/BETAMAX = $betamax/"\
              -e "s/BETAMAX =<betamax>/BETAMAX = $betamax/"\
              -e "s/BETAMAX = <betamax>/BETAMAX = $betamax/"\
              $path_d/ww3_grid.inp.${forcing} > ww3_grid.inp
        elif [ -e $FORCING/ww3_grid.inp.${forcing}${resforc} ] ; then
          sed -e "s/BETAMAX=betamax/BETAMAX = $betamax/" \
              -e "s/BETAMAX = betamax/BETAMAX = $betamax/"\
              -e "s/BETAMAX =<betamax>/BETAMAX = $betamax/"\
              -e "s/BETAMAX = <betamax>/BETAMAX = $betamax/"\
              $FORCING/ww3_grid.inp.${forcing}${resforc} > ww3_grid.inp
        else
          echo "no ww3_grid.nml or ww3_grid.inp found for $forcing"
          exit 1
        fi
        rm -f mod_def.ww3 
        echo ""
        echo "   Process $forcing"  
        echo "   Screen output routed to $path_o/ww3_grid.${forcing}.out"
        echo -n "   $path_e/ww3_grid... "
        if ! $path_w/ww3_grid >& $path_o/ww3_grid.${forcing}.out
        then errmsg "Error occured during ww3_grid";  exit 1;
        else echo "OK"; fi
        rm -f mapsta.ww3 mask.ww3
        if [ -e ww3_grid.inp ]; then 
          mv ww3_grid.inp ww3_grid.inp.$forcing
        else
          mv ww3_grid.nml ww3_grid.nml.$forcing
        fi
        mv mod_def.ww3 mod_def.$forcing
      fi
    fi
  done
fi

#############
# ww3_bounc #
#############

# generates nest.ww3
if [ "$spec_zone" == "online" ] ; then
  echo "*** ww3_bounc $spec_zone ***"
  mod1=$(echo $mods | cut -d ' ' -f1)
  rm -f mod_def.ww3
  cp mod_def.$mod1 mod_def.ww3
  while [ ! -e $path_w/nest.ww3 ]
  do
    echo "waiting for nest.ww3 in $path_w"
    sleep 120
  done
  rm -f mod_def.ww3

elif [ "$spec_zone" == "glob" ] || [ "$spec_zone" == "med" ] || [ "$spec_zone" == "local" ]
then
  cd $path_w/NEST
  echo "*** ww3_bounc $spec_zone ***"
  mod1=$(echo $mods | cut -d ' ' -f1)
  cp $path_w/mod_def.$mod1 mod_def.ww3
  # ascii
  if [ "$wind" == "wind_cfsr" ] && [ $year -lt 2012 ]
  then
    cp $path_d/ww3_bound.inp.$mod1 $path_w/NEST/ww3_bound.inp
    cp $path_e/ww3_bound $path_w/NEST
    chmod 775 $path_w/NEST/ww3_bound
    echo -n "ww3_bound... "
    if ! $path_w/NEST/ww3_bound >& $path_o/ww3_bound.out
    then errmsg "Error occured during ww3_bound";  exit 1;
    else echo "OK"; fi
    cp $path_w/NEST/ww3_bound.inp $path_w/ww3_bound.inp.$mod1
  # netcdf
  else
    if [ -e $path_d/ww3_bounc.inp.$mod1 ] ; then 
      #sed -e "s/DATERUN/${year}/g" -e "s/<daterun>/${year}/g" $path_d/ww3_bounc.inp.$mod1 > $path_w/NEST/ww3_bounc.inp
      sed -e "s/DATERUN/${logdate}/g" -e "s/<daterun>/${logdate}/g" $path_d/ww3_bounc.inp.$mod1 > $path_w/NEST/ww3_bounc.inp
    elif [ -e $path_d/ww3_bounc.nml.$mod1 ] ; then
      sed -e "s/DATERUN/${logdate}/g" -e "s/<daterun>/${logdate}/g" $path_d/ww3_bounc.nml.$mod1 > $path_w/NEST/ww3_bounc.nml
      sed -e "s/DATERUN/${logdate}/g" -e "s/<daterun>/${logdate}/g" $path_d/spec_${mod1}.list > $path_w/NEST/spec_${mod1}.list
    fi
    cp $path_e/ww3_bounc $path_w/NEST
    chmod 775 $path_w/NEST/ww3_bounc
    echo -n "ww3_bounc... "
    if ! $path_w/NEST/ww3_bounc >& $path_o/ww3_bounc.out
    then errmsg "Error occured during ww3_bounc";  exit 1;
    else echo "OK"; fi
    if [ -e $path_w/NEST/ww3_bounc.inp ]; then
      cp $path_w/NEST/ww3_bounc.inp $path_w/ww3_bounc.inp.$mod1
    elif [ -e $path_w/NEST/ww3_bounc.nml ]; then
      cp $path_w/NEST/ww3_bounc.nml $path_w/ww3_bounc.nml.$mod1
      cp $path_w/NEST/spec_${mod1}.list $path_w/
    fi
  fi
  rm -f mod_def.ww3
  mv $path_w/NEST/nest.ww3 $path_w/
fi

############
# ww3_prnc #
############

# copy WW3 executables
cd $path_w
cp $path_e/ww3_prnc .
chmod 775 ww3_prnc
if [ -e $path_e/ww3_prnc.MPI ] ; then
  cp $path_e/ww3_prnc.MPI .
  chmod 775 ww3_prnc.MPI
fi
if [ -e $path_e/ww3_prtide ] ; then
  cp $path_e/ww3_prtide .
  chmod 775 ww3_prtide
fi

#ww3_prnc for wind
if [ "$wind" != "no" ] && [ "$wind" != "cst" ]
then
  WIND_INP="$WIND/ww3_prnc.inp.$wind"
  WIND_NML="$WIND/ww3_prnc.nml.$wind"
  WIND_FILE="$WIND/wind_${year}.nc"
fi
###
if [ ! -z "$(echo $run_tag | grep 'CFSR_modif')" ]
then
	WIND_FILE="$WIND/MOD/wind_${year}mod.nc"
fi

echo "WIND_FILE: $WIND_FILE"
###

#ww3_prnc for ice (or isi)
if [ "$ice" != "no" ] && [ "$ice" != "cst" ]
then
 ICE_INP="$ICE/ww3_prnc.inp.$ice"
 ICE_NML="$ICE/ww3_prnc.nml.$ice"
 ICE_FILE="$ICE/ice_${year}.nc"
fi

#ww3_prnc for ice1
if [ "$ice1" != "no" ] && [ "$ice1" != "cst" ]
then
  ICE1_INP="$ICE1/ww3_prnc.inp.$ice1"
  ICE1_NML="$ICE1/ww3_prnc.nml.$ice1"
  ICE1_FILE="$ICE1/ice1_${year}.nc"
fi

#ww3_prnc/ww3_prtide for cur
if [ "$cur" != "no" ] && [ "$cur" != "cst" ]
then
  if [ "$cur" == "cur_har" ] || [ "$cur" == "cur_fes" ] || [ "$cur" == "cur_rscd" ]
  then
    CUR_INP="$CUR/ww3_prtide.inp.$cur"
    CUR_NML="$CUR/ww3_prtide.nml.$cur"
    CUR_FILE="$CUR/current.ww3_tide"
  else
    CUR_INP="$CUR/ww3_prnc.inp.$cur"
    CUR_NML="$CUR/ww3_prnc.nml.$cur"
    CUR_FILE="$CUR/cur_${year}.nc"
  fi
fi

#ww3_prnc/ww3_prtide for lev
if [ "$lev" != "no" ] && [ "$lev" != "cst" ]
then
  if [ "$lev" == "lev_har" ] || [ "$lev" == "lev_fes" ] || [ "$lev" == "lev_rscd" ]
  then
    LEV_INP="$LEV/ww3_prtide.inp.$lev"
    LEV_NML="$LEV/ww3_prtide.nml.$lev"
    LEV_FILE="$LEV/level.ww3_tide"
  else
    LEV_INP="$LEV/ww3_prnc.inp.$lev"
    LEV_NML="$LEV/ww3_prnc.nml.$lev"
    LEV_FILE="$LEV/lev_$year.nc"
  fi
fi


#check for native preprocessing (CURV or UNST in multi)
declare -A native
imod=1
for mod in $mods
do
  native[$imod]=false
  if [ "$run_type" == "multi" ] ; then
#    if [ ! -z "$(grep 'GRID%TYPE' $path_w/ww3_grid.*.$mod | grep 'CURV')" ] || \
#       [ ! -z "$(grep 'GRID%TYPE' $path_w/ww3_grid.*.$mod | grep 'UNST')" ] ; then
      native[$imod]=true
#    fi
  fi
  echo "native $imod : ${native[$imod]}"
  imod=$(($imod+1))
done


#FORCINGS
for forc in wind ice ice1 cur lev
do
  echo "*** ww3_prnc $forc ***"
  forcing=$(echo $(eval echo "\${$forc}"))
  echo "$forc : $forcing"

  FORC="$(echo $forc | tr '[:lower:]' '[:upper:]')"
  FORCING=$(echo $(eval echo "\${$FORC}"))
  echo "$FORC : $FORCING"

  FORC_FILE="$(echo $(eval echo "\${${FORC}_FILE}"))"
  echo "file : $FORC_FILE"
  FORC_INP="$(echo $(eval echo "\${${FORC}_INP}"))"
  echo "inp : $FORC_INP"
  FORC_NML="$(echo $(eval echo "\${${FORC}_NML}"))"
  echo "nml : $FORC_NML"

  if [ "$forcing" != 'no' ] && [ "$FORCING" != "cst" ] ;  then
    if [ ! -f $FORC_FILE ] ; then
      echo "no $forc forcing : $FORC_FILE"
      exit 1
    fi

    # forecast condition: remove past forcing fields
    if [ -e $main_dir/date_cycle ] ; then
      for mod in $mods
      do
        if  [ "$forc" == "cur" ] ; then
          rm -f current.$mod current.$forcing current.ww3
        elif  [ "$forc" == "lev" ] ; then
          rm -f level.$mod level.$forcing level.ww3
        else
          rm -f $forc.$mod $forc.$forcing ${forc}.ww3
        fi
      done
    fi


    imod=1
    for mod in $mods
    do
      echo "mod : $mod"
      rm -f $forc.nc mod_def.ww3
      if [ "$run_type" == "shel" ] ; then
        mod1=$(echo $mods | cut -d ' ' -f1)
        ln -sfn mod_def.$mod1 mod_def.ww3
      elif [ "$run_type" == "multi" ] ; then
        if [ ${native[$imod]} = true ] ; then
          ln -sfn mod_def.$mod mod_def.ww3
        else
          ln -sfn mod_def.$forcing mod_def.ww3
        fi
      fi
      echo "$forc processing ..."

      # prtide
      forc_ext="$(echo $forcing | cut -d '_' -f2)"
      echo $forc_ext
      if [ "$forc_ext" == 'har' ] || \
         [ "$forc_ext" == 'fes' ] || \
         [ "$forc_ext" == 'rscd' ] ; then
        echo "[INFO] processing $FORC_FILE"
        if [ "$run_type" == "multi" ] && [ ${native[$imod]} = true ] && [ -e $path_w/$forc.$mod ]; then
              echo "[INFO] $forc.$mod already exists. It will not be regenerated"
        elif [ "$run_type" == "multi" ] && [ ${native[$imod]} = false ] && [ -e $path_w/$forc.$forcing ]; then
            echo "[INFO] $forc.$forcing already exists. It will not be regenerated"
        elif [ "$run_type" == "single" ] && [ -e $path_w/${forc}.ww3 ] ; then
          echo "[INFO] ${forc}.ww3 already exists. It will not be regenerated"
        else
          ln -sfn $FORC_FILE $path_w/
          sed -e "s:DATE:$startrun 1800 $stoprun:" \
              -e "s:<timestart>:$startrun:" \
              -e "s:<timestop>:$stoprun:" \
                 $FORC_INP > $path_w/ww3_prtide.inp
          echo "   Screen output routed to $path_o/ww3_prtide.${forcing}_${mod}.out"
          echo -n "$path_e/ww3_prtide... "
          #ww3_prtide >& $path_o/ww3_prtide.$forcing.out
          if ! $MPI_LAUNCH ww3_prtide >& $path_o/ww3_prtide.${forcing}_${mod}.out
          then errmsg "Error occured during ww3_prtide";  exit 1;
          else echo "OK"; fi
          mv ww3_prtide.inp ww3_prtide.inp.${forcing}_${mod}
        fi

      # prnc
      else
        if [ "$run_type" == "multi" ] && [ ${native[$imod]} = true ] && [ -e $path_w/$forc.$mod ]; then
          echo "[INFO] $forc.$mod already exists. It will not be regenerated"
        elif [ "$run_type" == "multi" ] && [ ${native[$imod]} = false ] && [ -e $path_w/$forc.$forcing ]; then
          echo "[INFO] $forc.$forcing already exists. It will not be regenerated"
      elif [ "$run_type" == "multi" ] && [ ${native[$imod]} = true ] && [ "$forc" == "cur" ] && [ -e $path_w/current.$mod ] ; then
          echo "[INFO] current.$mod already exists. It will not be regenerated"
      elif [ "$run_type" == "multi" ] && [ ${native[$imod]} = false ] && [ "$forc" == "cur" ] && [ -e $path_w/current.$forcing ] ; then
          echo "[INFO] current.$forcing already exists. It will not be regenerated"
      elif [ "$run_type" == "multi" ] && [ ${native[$imod]} = true ] && [ "$forc" == "lev" ] && [ -e $path_w/level.$mod ] ; then
          echo "[INFO] level.$mod already exists. It will not be regenerated"
      elif [ "$run_type" == "multi" ] && [ ${native[$imod]} = false ] && [ "$forc" == "lev" ] && [ -e $path_w/level.$forcing ] ; then
          echo "[INFO] level.$forcing already exists. It will not be regenerated"
        elif [ "$run_type" == "single" ] && [ -e $path_w/${forc}.ww3 ] ; then
          echo "[INFO] ${forc}.ww3 already exists. It will not be regenerated"
        else
          ln -sfn $FORC_FILE $path_w/${forc}.nc
          if [ -e $FORC_NML ] ; then
            sed -e "s/<timestart>/'$startrun'/" -e "s/<timestop>/'$stoprun'/" $FORC_NML > $path_w/ww3_prnc.nml
          fi #else
            if [ -e $FORC_INP ] ; then
              cp $FORC_INP $path_w/ww3_prnc.inp
            fi
          #fi
          echo "   Screen output routed to $path_o/ww3_prnc.${forcing}_${mod}.out"
          if [ "$forc_ext" == 'har' ] || \
             [ "$forc_ext" == 'fes' ] || \
             [ "$forc_ext" == 'rscd' ] ; then
            if [ -e $path_e/ww3_prnc.MPI ] ; then
              echo -n "$path_e/ww3_prnc.MPI... "
              if ! $MPI_LAUNCH ww3_prnc.MPI >& $path_o/ww3_prnc.${forcing}_${mod}.out
              then errmsg "Error occured during ww3_prnc";  exit 1;
              else echo "OK"; fi
            else
              echo -n "$path_e/ww3_prnc... "
              if ! ./ww3_prnc >& $path_o/ww3_prnc.${forcing}_${mod}.out
              then errmsg "Error occured during ww3_prnc";  exit 1;
              else echo "OK"; fi
            fi
          else
            echo -n "$path_e/ww3_prnc... "
            if ! ./ww3_prnc >& $path_o/ww3_prnc.${forcing}_${mod}.out
            then errmsg "Error occured during ww3_prnc";  exit 1;
            else echo "OK"; fi
          fi
          if [ -e ww3_prnc.nml ] ; then 
            mv ww3_prnc.nml ww3_prnc.nml.${forcing}_${mod}
          fi
          if [ -e ww3_prnc.inp ] ; then
            mv ww3_prnc.inp ww3_prnc.inp.${forcing}_${mod}
          fi
        fi
      fi

      # rename forcing output file extension
      if [ "$run_type" == "multi" ] ; then
        if [ -e $path_w/${forc}.ww3 ] ; then
          if [ ${native[$imod]} = true ] ; then
            mv ${forc}.ww3 ${forc}.$mod
          else
            mv ${forc}.ww3 ${forc}.$forcing
          fi
        elif [ "$forc" == "cur" ] && [ -e $path_w/current.ww3 ] ; then
          if [ ${native[$imod]} = true ] ; then
            mv current.ww3 current.$mod
          else
            mv current.ww3 current.$forcing
          fi
        elif [ "$forc" == "lev" ] && [ -e $path_w/level.ww3 ] ; then
          if [ ${native[$imod]} = true ] ; then
            mv level.ww3 level.$mod
          else
            mv level.ww3 level.$forcing
          fi
        fi
      fi
      unlink mod_def.ww3

      imod=$(($imod+1))
    done # mods
  fi # forcing
done # forc

# copy spectra point file
if [ ! -z "$(echo $pt_file)" ] ; then
  cp $path_d/$pt_file  $path_w
fi

#check if some errors occured during process
$waverun_path/check_error.sh $path_o


echo "END OF FORCING"


