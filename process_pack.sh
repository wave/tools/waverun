#!/bin/bash -e

# -------------------------------------------------------------------- #
# Waverun toolbox : process model output packing for archive           #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : GPLv3                                                      #
# Creation date : 01-Dec-2011                                          #
# Modification date : 07-Jun-2021                                      #
# -------------------------------------------------------------------- #


###########################################################################################
#   process pack for a selected time span defined in argument                             #
#                                                                                         #
# need to be in main_dir to execute the script                                            #
#                                                                                         #
# example :                                                                               #
#  qsub -v startrun='20161101 000000',stoprun='20161201 000000'                           #
#       -l walltime=12:00:00                                                              #
#       -N GLOBpack ~/TOOLS/SCRIPTS/HINDCAST/process_pack.sh                              #
#                                                                                         #
# additional bashrc file can be sourced if defined by $WAVERUN_PROFIL (with -v for qsub)  #
###########################################################################################


#----------------------
## 1. INPUT ARGUMENTS
#----------------------

# shell arguments
if [ $# -ge 2 ] ; then
  startrun=$1  # '20170314 000000'
  stoprun=$2   # '20170320 000000'
fi

# qsub arguments
if [ -z "$(echo $startrun)" ] || [ -z "$(echo $stoprun)" ]; then
  echo ''
  echo '[ERROR] missing startrun,stoprun arguments in qsub command'
  echo "[EXAMPLE] qsub -v startrun='20161101 000000',stoprun='20161201 000000' process_archive.sh"
  exit 1
fi

# start and stop dates
echo "startrun: $startrun"
echo "stoprun: $stoprun"

startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -uR)"

stopyear="$(echo $stoprun | cut -c1-4)"
stopmonth="$(echo $stoprun | cut -c5-6)"
stopday="$(echo $stoprun | cut -c7-8)"
stophour="$(echo $stoprun | cut -c10-11)"
stopmin="$(echo $stoprun | cut -c12-13)"
stopsec="$(echo $stoprun | cut -c14-15)"
stoprunformat="$(date -d "${stopyear}-${stopmonth}-${stopday} ${stophour}:${stopmin}:${stopsec}" -uR)"



#----------------------
## 2. ENVIRONMENT
#----------------------

# pbs environment
if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  cd $PBS_O_WORKDIR
fi

# wavepath environment
if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
else  echo 'no wavepath.env found in $PWD or $HOME' ; ls $PWD; exit 1 ; fi
echo "source $wavepath" && source $wavepath

# additional environment
if [ ! -z "$(echo $WAVERUN_PROFIL)" ] ; then
  echo "source $WAVERUN_PROFIL" && source $WAVERUN_PROFIL
fi

# mpi environment
if [ -z "$(echo $MPI_LAUNCH)" ] ; then
  MPI_LAUNCH=mpiexec
fi
echo "MPI_LAUNCH : $(which $MPI_LAUNCH)"

# wavesetup environment
echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env

# Error message function
errmsg ()
{
  echo "" 2>&1
  while [ $# != 0 ]
  do
    echo "ERROR: $1" 2>&1
    shift
  done
  echo "" 2>&1
}


#----------------------
## 3. INIT & CHECK
#----------------------

# check wavesetup.env
$waverun_path/check_setup.sh $main_dir

# set dates
logdate="${startyear}-${startmonth}-${startday}T${starthour}_${stopyear}-${stopmonth}-${stopday}T${stophour}"

# check run dates consistency
if [ "$startyear" == "$stopyear" ] ; then
  year=$startyear
else
  if [ $(expr $stopyear - $startyear) -gt 2 ] ; then
    echo "   [ERROR] It is not possible to run over more than one year. Reduce the duration."
    exit 1
  elif [ $(expr $stopyear - $startyear) -eq 2 ] || [ $startmonth -le 11 ] ; then
    if [ $stopmonth -ne 1 ] || [ $stopday -ne 1 ] ; then
      echo "   [ERROR] It is not possible to run over two different years. Reduce the duration."
      exit 1
    fi
  fi
  # set year for forcing
  if [ $stopmonth -eq 1 ] && [ $stopday -eq 1 ] ; then
    year=$(expr $stopyear - 1)
  else
    year=$stopyear
  fi
fi

# make work directory
export path_w="$work_path/$run_tag/work${logdate}"
export path_o="$work_path/$run_tag/work${logdate}/output"
mkdir -p $path_w

# create output path
echo "   [INFO] output path : $path_o"
echo "   [INFO] work path : $path_w"
mkdir -p $path_o
if [ ! -e $path_w ] ; then
  echo "ERROR: the monthly work directory doesn't exist :"
  echo "$path_w"
  echo "       check at the end of process_hindcast.sh to know how to do it"
  exit 1
fi
cd $path_w

#check if some errors occured during last process
echo "*** check past log ***"
if [ ! -z "$(ls -A $path_o 2> /dev/null)" ] ; then
  $waverun_path/check_error.sh $path_o
fi


#----------------------
## 4. PROGRAM
#----------------------

  # loop on mods
  echo "*** link the results into an archive folder ***"
  for mod in $mods $buoys
  do
    echo "  mod : $mod"
    date
    arc_dir=${path_w}/${run_tag}/${mod}/${logdate}
    echo "  tmp arc dir : $arc_dir"
    mkdir -p ${arc_dir}

    # link field
    if [ $dt_mwp != 0 ] ; then
      echo "*** link FIELD ***"
      date
      for field in FIELD*
      do
        if [ "$(ls -A ${arc_dir}/$field 2> /dev/null)" ] ; then
          echo "[WARNING] ${arc_dir}/$field is NOT empty !"
          #exit 1
        fi
        mkdir -p ${arc_dir}/$field
        if [ -e $field/$mod ] && [ "$(ls -A $field/$mod 2> /dev/null)" ] ; then
          ln -rfsn $field/$mod/* ${arc_dir}/$field/
        fi
      done
    fi

    # link freq and spec
    if [ $dt_po != 0 ] ; then
      echo "*** link FREQ ***"
      date
      for freq in FREQ*
      do
        if [ "$(ls -A ${arc_dir}/$freq 2> /dev/null)" ] ; then
          echo "[WARNING] ${arc_dir}/$freq is NOT empty !"
          #exit 1
        fi
        mkdir -p ${arc_dir}/$freq
        if [ -e $freq/$mod ] && [ "$(ls -A $freq/$mod 2> /dev/null)" ] ; then
          ln -rfsn $freq/$mod/* ${arc_dir}/$freq/
        fi
      done
      echo "*** link SPEC ***"
      date
      for spec in SPEC*
      do
        if [ "$(ls -A ${arc_dir}/$spec 2> /dev/null)" ] ; then
          echo "[WARNING] ${arc_dir}/$spec is NOT empty !"
          #exit 1
        fi
        mkdir -p ${arc_dir}/$spec
        if [ -e $spec/$mod ] && [ "$(ls -A $spec/$mod 2> /dev/null)" ] ; then
          ln -rfsn $spec/$mod/* ${arc_dir}/$spec/
        fi
      done
    fi

    # link track
    if [ $dt_oat != 0 ] ; then
      echo "*** link track ***"
      date
      for track in TRACK*
      do
        if [ "$(ls -A ${arc_dir}/$track 2> /dev/null)" ] ; then
          echo "[WARNING] ${arc_dir}/$track is NOT empty !"
          #exit 1
        fi
        mkdir -p ${arc_dir}/$track
        if [ -e $track/$mod ] && [ "$(ls -A $track/$mod 2> /dev/null)" ] ; then
          ln -rfsn $track/$mod/* ${arc_dir}/$track/
        fi
      done
    fi

    # link output data wavesetup.env inp nml log to a temp folder
    echo "*** link for output, wavesetup.env, data, inp ***"
    date
    ln -rfsn output $path_d $main_dir/wavesetup.env ${arc_dir}/
    if  [ -e $main_dir/satsetup.env ]; then 
      ln -rfsn $main_dir/satsetup.env ${arc_dir}/ 
    fi
    if  [ -e $main_dir/stationsetup.env ]; then 
      ln -rfsn $main_dir/stationsetup.env ${arc_dir}/ 
    fi
    mkdir -p ${arc_dir}/work
    ln -rfsn log.* *inp* *nml* ${arc_dir}/work/

  done


  # loop on mods
  echo "*** link the alti and buoys into an archive folder ***"
  cd $path_w
  for mod in $mods
  do
    echo "  mod : $mod"
    date
    arc_dir=${path_w}/${run_tag}/${mod}/${logdate}

    # link sat
    if [ "$(ls -A ${arc_dir}/SAT 2> /dev/null)" ] ; then
      echo "[WARNING] ${arc_dir}/SAT is NOT empty !"
      #exit 1
    fi
    mkdir -p ${arc_dir}/SAT
    if [ -e SAT/$mod ] && [ "$(ls -A SAT/$mod 2> /dev/null)" ] ; then
      for dir in $(find SAT/$mod/ -type d) ; do
        mkdir -p ${arc_dir}/$dir
        for file in $(find $dir/ -type f -maxdepth 1) ; do
          ln -rfsn $file ${arc_dir}/$dir/
        done
      done
    fi

    # link station
    if [ "$(ls -A ${arc_dir}/STATION 2> /dev/null)" ] ; then
      echo "[WARNING] ${arc_dir}/STATION is NOT empty !"
      #exit 1
    fi
    mkdir -p ${arc_dir}/STATION
    if [ -e STATION ] && [ "$(ls -A STATION 2> /dev/null)" ] ; then
      for dir in $(find STATION/ -type d) ; do
        mkdir -p ${arc_dir}/$dir
        for file in $(find $dir/ -type f -maxdepth 1) ; do
          ln -rfsn $file ${arc_dir}/$dir/
        done
      done
    fi
  done

  # create folder on remote or localhost file system
  echo "*** create folder on remote or localhost file system ***"
  date
  cd $path_w/$run_tag
  pwd
  if [ ! -z "$(echo $path_a | grep ':')" ]
  then
    echo "   [INFO] remote file system"
    host="$(echo $path_a | cut -d ':' -f1)"
    filesystem="$(echo $path_a | cut -d ':' -f2)"
    echo "   [INFO] on $host"
    for folder in $(find -P . -mindepth 1 -xtype d -not -name "*.nc") ; do
      ssh $host mkdir -p $filesystem/$run_tag/$folder
    done
  else
    echo "   [INFO] local file system"
    for folder in $(find -P . -mindepth 1 -xtype d -not -name "*.nc") ; do
      echo "mkdir -p $path_a/$run_tag/$folder"
      mkdir -p $path_a/$run_tag/$folder
    done
  fi

  # remove empty directoriesin source and destination
  #echo "*** remove empty directories ***"
  #find $path_w/$run_tag -type d -empty -delete

  #copy temp folder in path_a
#  echo "*** copy results in $path_a ***"
#  cd $path_w
#  rsync -a ${run_tag}/ $path_a/${run_tag}/ >& $path_o/rsync_${run_tag}_${logdate}.out



#  find . -mindepth 1 -type f ! -name 'files.list' > files.list
#  max_thread=1020
#  echo "PBS_ARRAY_INDEX : $PBS_ARRAY_INDEX"
#  output=OUTPUT/$PBS_ARRAY_INDEX
#  mkdir -p $output && rm -f $output/*
#  i=0
#  for file in $(cat files.list)
#  do
#    i=$(($i + 1))
#    if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
#      rsync -a $file $path_a/${run_tag}/$(dirname $file)/
#    fi
#  done
  


#  while [ ! -z "$(grep -ai error $path_o/rsync_${run_tag}_${logdate}.out)" ]
#  do
#    echo "   [WARNING] problem to copy data into archive directory:"
#    echo "$path_a"
#    echo "    next try..."
#    sleep 10
#    rsync -a ${run_tag}/ $path_a/${run_tag}/ >& $path_o/rsync_${run_tag}_${logdate}.out
#  done


#  if [ -z "$(grep -ai error $path_o/rsync_${run_tag}_${logdate}.out)" ] ; then
#    echo "  remove ${run_tag}"
#    rm -rf ${run_tag}
#  else
#    echo "[ERROR] rsync not terminated correctly"
#    echo " $path_o/rsync_${run_tag}_${logdate}.out"
#    exit 1
#  fi



echo "!END OF ARCHIVING!"





