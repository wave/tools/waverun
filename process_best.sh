#!/bin/bash -e
#PBS -J 1-10
max_thread=10

# -------------------------------------------------------------------- #
# Waverun toolbox : process best estimate                              #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : GPLv3                                                      #
# Creation date : 01-Dec-2011                                          #
# Modification date : 12-Sept-2022                                     #
# -------------------------------------------------------------------- #

###########################################################################################
#   process best for a selected time span defined in argument                             #
#                                                                                         #
# need to be in main_dir to execute the script                                            #
#                                                                                         #
# example :                                                                               #
#  qsub -v startrun='20161101 000000',stoprun='20161201 000000'                           #
#       -lmem=20G -l walltime=2:00:00 -J 1-10                                             #
#       -N GLOBbest ~/TOOLS/SCRIPTS/HINDCAST/process_best.sh                              #
#                                                                                         #
# additional bashrc file can be sourced if defined by $WAVERUN_PROFIL (with -v for qsub)  #
###########################################################################################


#----------------------
## 1. INPUT ARGUMENTS
#----------------------

# shell arguments
if [ $# -ge 2 ] ; then
  startrun=$1  # '20170314 000000'
  stoprun=$2   # '20170320 000000'
fi

# qsub arguments
if [ -z "$(echo $startrun)" ] || [ -z "$(echo $stoprun)" ]; then
  echo ''
  echo '[ERROR] missing startrun,stoprun arguments in qsub command'
  echo "[EXAMPLE] qsub -v startrun='20161101 000000',stoprun='20161201 000000' -J 1-10 process_best.sh"
  exit 1
fi

# start and stop dates
echo "startrun: $startrun"
echo "stoprun: $stoprun"

startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -uR)"

stopyear="$(echo $stoprun | cut -c1-4)"
stopmonth="$(echo $stoprun | cut -c5-6)"
stopday="$(echo $stoprun | cut -c7-8)"
stophour="$(echo $stoprun | cut -c10-11)"
stopmin="$(echo $stoprun | cut -c12-13)"
stopsec="$(echo $stoprun | cut -c14-15)"
stoprunformat="$(date -d "${stopyear}-${stopmonth}-${stopday} ${stophour}:${stopmin}:${stopsec}" -uR)"



#----------------------
## 2. ENVIRONMENT
#----------------------

# pbs environment
if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  cd $PBS_O_WORKDIR
fi

# wavepath environment
if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
else  echo 'no wavepath.env found in $PWD or $HOME' ; ls $PWD; exit 1 ; fi
echo "source $wavepath" && source $wavepath

# additional environment
if [ ! -z "$(echo $WAVERUN_PROFIL)" ] ; then
  echo "source $WAVERUN_PROFIL" && source $WAVERUN_PROFIL
fi

# mpi environment
if [ -z "$(echo $MPI_LAUNCH)" ] ; then
  MPI_LAUNCH=mpiexec
fi
echo "MPI_LAUNCH : $(which $MPI_LAUNCH)"

# wavesetup environment
echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env

# Error message function
errmsg ()
{
  echo "" 2>&1
  while [ $# != 0 ]
  do
    echo "ERROR: $1" 2>&1
    shift
  done
  echo "" 2>&1
}


#----------------------
## 3. INIT & CHECK
#----------------------

# check wavesetup.env
$waverun_path/check_setup.sh $main_dir

# set dates
logdate="${startyear}-${startmonth}-${startday}T${starthour}_${stopyear}-${stopmonth}-${stopday}T${stophour}"
echo "logdate : $logdate"

# set day of today
#year=$(date +%Y)
#month=$(date +%m)
#day=$(date +%d)
#hour=$(cat $main_dir/date_cycle)
datecycle=$(cat $main_dir/date_cycle)
echo "datecycle : $datecycle"
cycleyear=$(echo ${datecycle:0:4})
cyclemonth=$(echo ${datecycle:4:2})
cycleday=$(echo ${datecycle:6:2})
cyclehour=$(echo ${datecycle:9:2})
echo 'curdate'
curdate=$(date -d "$cycleyear-$cyclemonth-$cycleday" +%Y%m%d)T$cyclehour
echo 'curdateformat'
curdateformat=$(date -d "$cycleyear-$cyclemonth-$cycleday $cyclehour:00:00" -u)
echo 'pastdate'
pastdate=$(date -d "$curdateformat -1 days" +%Y%m%d)
echo "best estimate for $mods at $curdate"



#----------------------
## 4. PROGRAM
#----------------------

  # mod_def out_grd out_pnt restart
  for mod in $mods
  do
    for data in mod_def out_grd out_pnt restart
    do
      data_dir=$path_b/$mod/$data
      mkdir -p $data_dir/best_estimate/$cycleyear

      echo "*** $data best estimate for ${run_tag} ***"
      echo "    sync $work_path/$run_tag/lastwork/ -name "${data}*.$mod""
      i=0
      for file in $(find $work_path/$run_tag/lastwork/ -type f -maxdepth 1 -name "${data}*.$mod")
      do
        i=$(($i + 1))
        if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
          echo -n "rsync... "
          if ! rsync -auv $file $data_dir/best_estimate/$cycleyear/
          then errmsg "Error occured during rsync";  exit 1;
          else echo "OK"; fi
        fi
      done
    done
  done


  # FIELD_NC
  for mod in $mods
  do
    for data in FIELD_NC
    do
      if [ ! -e $work_path/$run_tag/lastwork/$data ] ; then
        echo "[INFO] no $data in $work_path/$run_tag/lastwork. skipped"
        continue
      fi

      data_dir=$path_b/$mod/$data
      mkdir -p $data_dir/best_estimate/$cycleyear

      echo "*** $data best estimate for ${run_tag} ***"
      prefix=${project}${mod}
      echo "    sync $work_path/$run_tag/lastwork/$data/$mod -name ${prefix}_*nc"
      i=0
      for file in $(find $work_path/$run_tag/lastwork/$data/$mod -type f -name "${prefix}_*nc")
      do
        i=$(($i + 1))
        if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
          echo -n "rsync... "
          if ! rsync -auv $file $data_dir/best_estimate/$cycleyear/
          then errmsg "Error occured during rsync";  exit 1;
          else echo "OK"; fi
        fi
      done
    done
  done

  # out_pnt
  for buoy in $buoys
  do
    for data in out_pnt
    do
      data_dir=$path_b/$buoy/$data
      mkdir -p $data_dir/best_estimate/$cycleyear

      echo "*** $data best estimate for ${run_tag} ***"
      echo "    sync $work_path/$run_tag/lastwork/ -name "${data}*.$buoy""
      i=0
      for file in $(find $work_path/$run_tag/lastwork/ -type f -maxdepth 1 -name "${data}*.$buoy")
      do
        i=$(($i + 1))
        if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
          echo -n "rsync... "
          if ! rsync -auv $file $data_dir/best_estimate/$cycleyear/
          then errmsg "Error occured during rsync";  exit 1;
          else echo "OK"; fi
        fi
      done
    done
  done

  # SPEC_NC FREQ_NC
  for buoy in $buoys
  do
    for data in SPEC_NC FREQ_NC 
    do
      if [ ! -e $work_path/$run_tag/lastwork/$data ] ; then
        echo "[INFO] no $data in $work_path/$run_tag/lastwork. skipped"
        continue
      fi

      if [ "$data" == "SPEC_NC" ] ; then
        ext="spec.nc"
      else
        ext="freq.nc"
      fi

      mod1="$(echo $mods | cut -d' ' -f1)"
      data_dir=$path_b/$buoy/$data
      mkdir -p $data_dir/best_estimate_hourly/$cycleyear

      echo "*** $data best estimate for ${run_tag} ***"
      prefix=${project}${mod1}
      echo "sync $work_path/$run_tag/lastwork/$data/$buoy/ -name ${prefix}-*_*_${ext}"
      allfiles=$(find $work_path/$run_tag/lastwork/$data/$buoy/ -type f -name "${prefix}-*_*_${ext}")
      firstfile=$(echo $allfiles | awk -F ' ' '{print $1}')
      echo "firstfile : $firstfile"
      firstdate=$(echo $firstfile | sed -e "s/_${ext}//" | awk -F '_' '{print $NF}')
      echo "firstdate : $firstdate"
      i=0
      for dailyfile in $(find $work_path/$run_tag/lastwork/$data/$buoy/ -type f -name "${prefix}-*_${firstdate}_${ext}")
      do
        i=$(($i + 1))
        if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
          hourlyfile=$(basename $dailyfile | awk 'BEGIN{FS=OFS="_"}{NF--; print}')
          buoyname=$(echo $hourlyfile | awk -F'_' '{print $2}' | awk -F"${mod1}-" '{print $2}')
          echo "mod1 : " $mod1
          echo "buoyname : " $buoyname
          mkdir -p $data_dir/best_estimate_hourly/$cycleyear/${buoyname}
          echo -n "rsync... "
          if ! rsync -au $work_path/$run_tag/lastwork/$data/$buoy/${prefix}-${buoyname}_*_${ext} $data_dir/best_estimate_hourly/$cycleyear/${buoyname}/
          then errmsg "Error occured during rsync";  exit 1;
          else echo "OK"; fi
        fi
      done
    done
  done

  # specific case for waves and currents in Iroise Sea
  if [ "${run_tag}" == "NORGASUG" ] ; then

    for mod in FINIS-200M
    do
      for data in INTERP_NC
      do
        data_dir=$path_b/$mod/$data
        mkdir -p $data_dir/best_estimate/$cycleyear

        echo "*** $date best estimate for ${run_tag} ***"
        prefix=${project}${mod}
        echo "sync $work_path/$run_tag/lastwork/$data/$mod -name ${prefix}_*.nc"
        i=0
        for file in $(find $work_path/$run_tag/lastwork/$data/$mod -name "${prefix}_*nc")
        do
          i=$(($i + 1))
          if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
            echo -n "rsync... "
            if ! rsync -au $file $data_dir/best_estimate/$cycleyear/
            then errmsg "Error occured during rsync";  exit 1;
            else echo "OK"; fi
          fi
        done
      done 
    done
    # copy mars current best estimate for FINIS-250M
    mkdir -p $path_b/$mod/MARS/best_estimate/$cycleyear
    echo -n "rsync..."
    if ! rsync -au /home/ref-marc/l1_v9/MARC_L1-MARS2D-FINIS250/best_estimate/$cycleyear/*.nc $path_b/$mod/MARS/best_estimate/$cycleyear/
    then errmsg "Error occured during ww3_grid";  exit 1;
    else echo "OK"; fi
  fi

echo ""
echo -e "\n END OF BEST ESTIM!"
