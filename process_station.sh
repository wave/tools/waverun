#!/bin/bash -e

# -------------------------------------------------------------------- #
# Waverun toolbox : process station comparison                         #
#                                                                      #
# Author : Mickael ACCENSI - IFREMER                                   #
# License : GPLv3                                                      #
# Creation date : 01-Dec-2011                                          #
# Modification date : 23-Nov-2023                                      #
# -------------------------------------------------------------------- #


###########################################################################################
#   process station for a selected time span defined in argument                          #
#                                                                                         #
# need to be in main_dir to execute the script                                            #
#                                                                                         #
# example :                                                                               #
#  qsub -v startrun='20161101 000000',stoprun='20161201 000000',max_thread=100            #
#       -lmem=2G -l walltime=04:00:00 -J 1-100                                            #
#       -N GLOBstation ~/TOOLS/SCRIPTS/HINDCAST/process_station.sh                        #
#                                                                                         #
# additional bashrc file can be sourced if defined by $WAVERUN_PROFIL (with -v for qsub)  #
###########################################################################################


#----------------------
## 1. INPUT ARGUMENTS
#----------------------

# shell arguments
if [ $# -ge 2 ] ; then
  startrun=$1  # '20170314 000000'
  stoprun=$2   # '20170320 000000'
fi

# qsub arguments
if [ -z "$(echo $startrun)" ] || [ -z "$(echo $stoprun)" ]; then
  echo ''
  echo '[ERROR] missing startrun,stoprun arguments in qsub command'
  echo "[EXAMPLE] qsub -v startrun='20161101 000000',stoprun='20161201 000000',max_thread=100 -lmem=2G -lwalltime=04:00:00 -J 1-100 process_station.sh"
  exit 1
fi

# start and stop dates
echo "startrun: $startrun"
echo "stoprun: $stoprun"

startyear="$(echo $startrun | cut -c1-4)"
startmonth="$(echo $startrun | cut -c5-6)"
startday="$(echo $startrun | cut -c7-8)"
starthour="$(echo $startrun | cut -c10-11)"
startmin="$(echo $startrun | cut -c12-13)"
startsec="$(echo $startrun | cut -c14-15)"
startrunformat="$(date -d "${startyear}-${startmonth}-${startday} ${starthour}:${startmin}:${startsec}" -uR)"

stopyear="$(echo $stoprun | cut -c1-4)"
stopmonth="$(echo $stoprun | cut -c5-6)"
stopday="$(echo $stoprun | cut -c7-8)"
stophour="$(echo $stoprun | cut -c10-11)"
stopmin="$(echo $stoprun | cut -c12-13)"
stopsec="$(echo $stoprun | cut -c14-15)"
stoprunformat="$(date -d "${stopyear}-${stopmonth}-${stopday} ${stophour}:${stopmin}:${stopsec}" -uR)"

# set dates
logdate="${startyear}-${startmonth}-${startday}T${starthour}_${stopyear}-${stopmonth}-${stopday}T${stophour}"
year=$startyear


#----------------------
## 2. ENVIRONMENT
#----------------------

# pbs environment
if [ ! -z $(echo $PBS_O_WORKDIR) ] ; then
  cd $PBS_O_WORKDIR
fi

# wavepath environment
#if [ -e $PWD/wavepath.env ] ; then wavepath="$PWD/wavepath.env" ;
#elif [ -e $HOME/wavepath.env ] ; then wavepath="$HOME/wavepath.env" ;
#else  echo 'no wavepath.env found in $PWD or $HOME' ; exit ; fi
#echo "source $wavepath" && source $wavepath

# wavesetup environment
#echo "source $PWD/wavesetup.env" && source $PWD/wavesetup.env
echo "source $PWD/stationsetup.env" && source $PWD/stationsetup.env

# additional environment
if [ ! -z "$(echo $WAVERUN_PROFIL)" ] ; then
  echo "source $WAVERUN_PROFIL" && source $WAVERUN_PROFIL
fi

# mpi environment
if [ -z "$(echo $MPI_LAUNCH)" ] ; then
  MPI_LAUNCH=mpiexec
fi
echo "MPI_LAUNCH : $(which $MPI_LAUNCH)"

# Error message function
errmsg ()
{
  echo "" 2>&1
  while [ $# != 0 ]
  do
    echo "ERROR: $1" 2>&1
    shift
  done
  echo "" 2>&1
}


#----------------------
## 3. INIT & CHECK
#----------------------

# check wavesetup.env
#$waverun_path/check_setup.sh $main_dir


# create output path
#export path_w="$work_path/$run_tag/work${logdate}"
#export path_o="$work_path/$run_tag/work${logdate}/output"
#echo "   [INFO] output path : $path_o"
#echo "   [INFO] work path : $path_w"

#check if some errors occured during last process
#echo "*** check past log ***"
#if [ ! -z "$(ls -A $outdir 2> /dev/null)" ] ; then
#  $waverun_path/check_error.sh $outdir
#fi

# obs variables
obsvarlist=''
hs=0; mss=0; wnd=0; cur=0;
if [ ! -z "$(echo $obshs)"  ] ; then hs=1; obsvarlist="$obsvarlist $obshs"; fi
if [ ! -z "$(echo $obsmss)" ] ; then mss=1; obsvarlist="$obsvarlist $obsmss"; fi
if [ ! -z "$(echo $obswnd)" ] ; then wnd=1 obsvarlist="$obsvarlist $obswnd"; fi
if [ ! -z "$(echo $obscur)" ] ; then cur=1 obsvarlist="$obsvarlist $obscur"; fi
statobsvarlist="$obsvarlist"
obsvarlist="$obsvarlist $obsaddvar"
obsvarlist=$(echo $obsvarlist)

#mod variables
modvarlist=''
statmodvarlist=''
if [ $hs -eq 1 ] ; then modvarlist="$modvarlist hs"; statmodvarlist="$statmodvarlist hs"; fi 
if [ $mss -eq 1 ] ; then modvarlist="$modvarlist mssd mssu mssc"; statmodvarlist="$statmodvarlist mss"; fi 
if [ $wnd -eq 1 ] ; then modvarlist="$modvarlist uwnd vwnd"; statmodvarlist="$statmodvarlist wnd"; fi 
if [ $cur -eq 1 ] ; then modvarlist="$modvarlist ucur vcur"; statmodvarlist="$statmodvarlist cur"; fi 
modvarlist="$modvarlist $modaddvar"
modvarlist=$(echo $modvarlist)

# check mod and obs directories
#if [ ! -e $moddir ] ; then
#  echo "[ERROR] $moddir does not exist"
#  exit 1
#fi
if [ -z "$(echo $obsdir)" ] || [ ! -e $obsdir ] ; then
  echo "[ERROR] $obsdir does not exist"
  exit 1
fi

# loop on obs
obsarray=()
while IFS=  read -r -d $'\0'; do
  obsarray+=("$REPLY")
done < <(find ${obsdir}/ -mindepth 1 -maxdepth 1 -name "$obslisting" -type f -print0)

numobs=${#obsarray[@]}
echo "number of obs :  $numobs"

obs_full_list=""
for elem in "${obsarray[@]}"; do
  obs_full_list="$obs_full_list $elem"
done


# seq job full list
if [ -z $(echo $PBS_ARRAY_INDEX) ] ; then
  PBS_ARRAY_INDEX=1
  obs_list=$obs_full_list
fi

# create obs list per thread
obs_list=""
i=0
for elem in "${obsarray[@]}"; do
  i=$(($i + 1))
  if [ $(($i%$max_thread)) -eq $(($PBS_ARRAY_INDEX%$max_thread)) ] ; then
    obs_list="$obs_list $elem"
  fi
done



if [ -z "$(echo $obs_list)" ] ; then
  echo "thread $PBS_ARRAY_INDEX is processing nothing"
  exit 0
else
  echo "thread $PBS_ARRAY_INDEX is processing $obs_list"
fi


#----------------------
## 4. PROGRAM
#----------------------

#-------------------------------------------------------
# 4.1. Loop on model domains (GLOB-30M, ATNE-10M, , etc.)
#-------------------------------------------------------

for mod in $mods
do
  echo "loop on model : $mod"

  # define path_w and main_dir from wavesetup.env
  if [ -e $moddir/$mod ]; then
    mod_dir="$moddir/$mod"
    dir_out="$outdir/$mod"
  else
    mod_dir="$moddir"
    dir_out="$outdir"
  fi


  echo "model path : $mod_dir"
  echo "out   path : $dir_out"

  # result folder
  mkdir -p $dir_out
  cd $dir_out



  # get grid closure, lon/lat min/max, dx/dy
  # from env file if defined
  if [ ! -z "$(echo $lon_min)" ] ; then
    echo 'grid type and closure'
    echo $gtype $closure
    echo 'lon_min lon_max lat_min lat_max'
    echo "$lon_min $lon_max $lat_min $lat_max"

  # else retrieve them from nml file
  else
    nmlfile=$path_d/ww3_grid.nml.$mod
    inpfile=$path_d/ww3_grid.inp.$mod
    if [ ! -e $nmlfile ] && [ ! -e $inpfile ] ; then
      echo "[ERROR] no $nmlfile / $inpfile found"
      exit 1
    fi
    # nml
    if [ -e $nmlfile ] ; then
      echo "read $nmlfile"
      tmpfile=$(basename $nmlfile).$PBS_ARRAY_INDEX
      rm -f $tmpfile
      cat $nmlfile | while read line; do
        if [ -z "$(echo $line | grep '\!')" ] ; then
          echo $line >> $tmpfile
        fi
      done
      pwd
      echo "read $tmpfile"
      closure="$(grep 'GRID%CLOS' $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
      gtype="$(grep 'GRID%TYPE' $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
      echo 'grid type and closure'
      echo $gtype $closure
      if [ "$gtype" == "UNST" ] ; then
        echo '[WARNING] grid type is UNST'
        #echo '          dx/dy set to 0.5'
        nb_nodes=$(sed -n 5p ${path_d}/${mod}.msh | awk -F' ' '{print $1}')
        echo "nodes : $nb_nodes"
        sed -n 6,$(($nb_nodes + 5))p ${path_d}/${mod}.msh | awk -F' ' '{print $2}'  | sort -n > lon.txt.$PBS_ARRAY_INDEX
        sed -n 6,$(($nb_nodes + 5))p ${path_d}/${mod}.msh | awk -F' ' '{print $3}'  | sort -n > lat.txt.$PBS_ARRAY_INDEX
        lon_min="$(head -n1 lon.txt.$PBS_ARRAY_INDEX)"
        lon_max="$(tail -n1 lon.txt.$PBS_ARRAY_INDEX)"
        lat_min="$(head -n1 lat.txt.$PBS_ARRAY_INDEX)"
        lat_max="$(tail -n1 lat.txt.$PBS_ARRAY_INDEX)"
        rm lat.txt.$PBS_ARRAY_INDEX lon.txt.$PBS_ARRAY_INDEX
        echo 'lon_min lon_max lat_min lat_max'
        echo "$lon_min $lon_max $lat_min $lat_max"
      elif [ "$gtype" == "RECT" ] ; then
        nlon="$(grep "$gtype%NX" $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
        nlat="$(grep "$gtype%NY" $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
        reslon="$(grep "$gtype%SX" $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
        reslat="$(grep "$gtype%SY" $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
        if [ ! -z "$(grep "$gtype%SF " $tmpfile)" ] ; then factor="$(grep "$gtype%SF " $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"; else factor=1.; fi
        lon_min="$(grep "$gtype%X0" $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
        lat_min="$(grep "$gtype%Y0" $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"
        if [ ! -z "$(grep "$gtype%SF0" $tmpfile)" ] ; then factor0="$(grep "$gtype%SF0" $tmpfile | awk -F'=' '{print $2}' | cut -d \' -f2)"; else factor0=1.; fi
        echo 'nlon nlat reslon reslat factor lon_min lat_min factor0'
        echo $nlon $nlat $reslon $reslat $factor $lon_min $lat_min $factor0
        lon_min=$( echo "scale=4;$lon_min / ${factor0}" | bc)
        lat_min=$(echo "scale=4;$lat_min / ${factor0}" | bc)
        reslon=$(echo "scale=4;$reslon / $factor" |bc)
        reslat=$(echo "scale=4;$reslat / $factor" |bc)
        lon_max=$( echo "(${lon_min}) + ($nlon - 1) * $reslon" | bc)
        #lon_max=$(echo "$lon_max + ($reslon * 0.9999)" | bc)
        lat_max=$(echo "(${lat_min}) + ($nlat - 1) * $reslat" | bc)
        echo 'reslon reslat lon_min lon_max lat_min lat_max'
        echo $reslon $reslat $lon_min $lon_max $lat_min $lat_max
      elif [ "$gtype" == "CURV" ] ; then
        lonfile="$(grep "CURV%XCOORD%FILENAME" $tmpfile  | awk -F'=' '{print $2}' | cut -d \' -f2)"
        latfile="$(grep "CURV%YCOORD%FILENAME" $tmpfile  | awk -F'=' '{print $2}' | cut -d \' -f2)"
        cat $path_d/$lonfile | tr " " "\n" | sort -n > lon.txt.$PBS_ARRAY_INDEX
        cat $path_d/$latfile | tr " " "\n" | sort -n > lat.txt.$PBS_ARRAY_INDEX
        sed -i '/^$/d' lon.txt.$PBS_ARRAY_INDEX
        sed -i '/^$/d' lat.txt.$PBS_ARRAY_INDEX
        lon_min="$(head -n1 lon.txt.$PBS_ARRAY_INDEX)"
        lon_max="$(tail -n1 lon.txt.$PBS_ARRAY_INDEX)"
        lat_min="$(head -n1 lat.txt.$PBS_ARRAY_INDEX)"
        lat_max="$(tail -n1 lat.txt.$PBS_ARRAY_INDEX)"
        rm lat.txt.$PBS_ARRAY_INDEX lon.txt.$PBS_ARRAY_INDEX
        echo 'lon_min lon_max lat_min lat_max'
        echo "$lon_min $lon_max $lat_min $lat_max"
      fi
      rm -f $tmpfile
      # inp  
    elif [ -e $inpfile ] ; then
      tmpfile=$(basename $inpfile).$PBS_ARRAY_INDEX
      rm -f $tmpfile
      write=0
      cat $inpfile | while read line; do
        if [ -z "$(echo $line | grep '\$')" ] ; then
          if [ $write -eq 1 ] ; then
            echo $line >> $tmpfile
          fi
          if [ "$(echo $line)" == "END OF NAMELISTS" ] ; then
            write=1
          fi
        fi
      done
      gtype=$(sed -n '1p' $tmpfile | cut -d ' ' -f1 | cut -d "'" -f2)
      if [ "$gtype" != "RECT" ] ; then
        echo '[ERROR] grid type is not regular'
             ' - must set the lon_min/max lat_min/max and dx/dy -'
      fi
      closure=$(sed -n '1p' $tmpfile | cut -d ' ' -f3 | cut -d "'" -f2)
      echo "closure : $closure"
      nlon=$(sed -n '2p' $tmpfile | cut -d ' ' -f1)
      nlat=$(sed -n '2p' $tmpfile | cut -d ' ' -f2)
      echo "nlon nlat : $nlon $nlat"
      reslon=$(sed -n '3p' $tmpfile | cut -d ' ' -f1)
      reslat=$(sed -n '3p' $tmpfile | cut -d ' ' -f2)
      factor=$(sed -n '3p' $tmpfile | cut -d ' ' -f3)
      echo "reslon reslat factor : $reslon $reslat $factor"
      lon_min=$(sed -n '4p' $tmpfile | cut -d ' ' -f1)
      lat_min=$(sed -n '4p' $tmpfile | cut -d ' ' -f2)
      echo "lon_min lat_min : $lon_min $lat_min"
      reslon=$(echo "scale=4;$reslon / $factor" |bc)
      reslat=$(echo "scale=4;$reslat / $factor" |bc)
      lon_max=$(echo "$lon_min + ($nlon - 1) * $reslon" | bc)
      #lon_max=$(echo "$lon_max + ($reslon * 0.9999)" | bc)
      lat_max=$(echo "$lat_min + ($nlat - 1) * $reslat" | bc)
      echo 'reslon reslat lon_max lat_max'
      echo $reslon $reslat $lon_max $lat_max
      rm -f $tmpfile
    fi
  fi

  echo "dx dy : $dx $dy"
  xrange=$(echo "scale=4;$lon_max - $lon_min + 1"|bc)
  yrange=$(echo "scale=4;$lat_max - $lat_min + 1"|bc)
  echo "xrange yrange : $xrange $yrange"

  #
  # Link model files
  #

  # define file base
  if [ "$ndates" = "4" ] ; then
#filebase="${project}${mod}?${year}"
    filebase="${project}${mod}?????"
  elif [ "$ndates" = "6" ] ; then
#filebase="${project}${mod}?${year}??"
    filebase="${project}${mod}???????"
  elif [ "$ndates" = "8" ] ; then
#filebase="${project}${mod}?${year}????"
    filebase="${project}${mod}?????????"
  elif [ "$ndates" = "10" ] ; then
#filebase="${project}${mod}?${year}????T??Z"
    filebase="${project}${mod}?????????T??Z"
  fi

  # clean up result folder
#  mkdir -p ${dir_out}/all
#  for obs in $obs_list
#  do
#    rm -f ${dir_out}/all/track.list_$(basename $obs)
#    sleep 10
#  done

  #-------------------------------------------------------
  # 4.2. Loop on obs files
  #-------------------------------------------------------

  for obs in $obs_list
  do

    echo "processing $obs"

    # compute diffperiod in seconds
    if [ "$statperiod" == "all" ] ; then
      diffperiod=1
    else
      diff=$(echo $(( $(date -d "$stoprunformat" +%s) - $(date -d "$startrunformat" +%s) )) )
      echo "diff sec : $diff"
      if [ "$statperiod" == "day" ]; then
        diffperiod=$(($diff/86400))
        echo "diff day : $(($diff/86400))"
      elif [ "$statperiod" == "week" ]; then
        diffperiod=$(($diff/86400/7))
        echo "diff week : $(($diff/86400/7))"
      elif [ "$statperiod" == "month" ]; then
        diffperiod=$(($diff/86400/30))
        echo "diff month : $(($diff/86400/30))"
      elif [ "$statperiod" == "year" ]; then
        diffperiod=$(($diff/86400/365))
        echo "diff year : $(($diff/86400/365))"
      fi
    fi

    #-------------------------------------------------------
    # 4.3. Loop over stat period # [day|week|month|year|all]
    #-------------------------------------------------------

    for iperiod in $(seq $diffperiod)
    do
      if [ "$statperiod" == "all" ] ; then
        tmpstartrunformat="$startrunformat"
        tmpstartrun="$startrun"
        tmpstoprunformat="$stoprunformat"
        tmpstoprun="$stoprun"
      else
        tmpstartrunformat="$(date -d "$startrunformat +$(($iperiod-1)) $statperiod" -uR)"
        tmpstartrun="$(date -d "$tmpstartrunformat" +%Y%m%d) 000000"
        tmpstoprunformat="$(date -d "$tmpstartrunformat +1 $statperiod" -uR)"
        tmpstoprun="$(date -d "$tmpstoprunformat" +%Y%m%d) 000000"
        # do not exceed the last time of the run
        if [ $(( $(date -d "$tmpstoprunformat" +%s) - $(date -d "$stoprunformat" +%s) )) -gt 0 ] ; then
          tmpstoprunformat=$stoprunformat
          tmpstoprun=$stoprun
        fi
      fi

      tmpstartyear="$(echo $tmpstartrun | cut -c1-4)"
      tmpstartmonth="$(echo $tmpstartrun | cut -c5-6)"
      tmpstartday="$(echo $tmpstartrun | cut -c7-8)"
      tmpstarthour="$(echo $tmpstartrun | cut -c10-11)"
      tmpstartmin="$(echo $tmpstartrun | cut -c12-13)"

      timesec=$(echo $(($(date -d "${tmpstartyear}-${tmpstartmonth}-${tmpstartday} ${tmpstarthour}:${tmpstartmin}:00" +%s) - $(date -d "1990-01-01 00:00:00" +%s))) )
      timeday=$(echo "scale=1;$timesec/86400" | bc)

      echo "$tmpstartrun / $tmpstoprun"


      # output directory
      dir_outobs="${dir_out}/$(basename $obs)_${tmpstartrun:0:8}_${tmpstoprun:0:8}"
      dir_log=$dir_outobs/OUTPUT
      mkdir -p $dir_log
      mkdir -p $dir_outobs
      cd $dir_outobs

      # do not rerun if overall_stat.nc exists 
#     if [ -e ${project}${mod}_$(basename $obs)_${year}_overall_stat.nc ]; then
#       echo "${project}${mod}_$(basename $obs)_${year}_overall_stat.nc exists. skip"
#       continue
      # only rerun when missing overall_stat.nc file
#     else
#       echo "${project}${mod}_$(basename $obs)_${year}_overall_stat.nc does not exists. clean up and run"
#       rm -rf $dir_outobs/*
#       mkdir -p $dir_log
#     fi

#      mkdir -p $dir_out/all
      if [ ! -e $obs ] ; then
        echo "[WARNING] No data for $obs"
#        echo '' > $dir_out/all/track.list_$(basename $obs)
        rm -rf $dir_outobs
        continue
      fi


      # define mod list
      echo "moddir : $moddir"
      echo "modsamefile : $modsamefile"
      echo "filebase : $filebase"
      rm -f mod.list.tmp
      if [ "$modsamefile" = 'F' ] ; then
        if [ $hs -eq 1 ] ; then
          find ${mod_dir}/ -maxdepth 4 -name "${filebase}_hs.nc"  >> mod.list.tmp
          n_tree=$(find ${mod_dir}/ -maxdepth 4 -name "${filebase}_hs.nc" | tail -n1 | xargs echo | cut -d' ' -f1 | awk -F'/' '{print NF}' )
        fi
        if [ $wnd -eq 1 ] ; then
          find ${mod_dir}/ -maxdepth 4 -name "${filebase}_wnd.nc" >> mod.list.tmp
          n_tree=$(find ${mod_dir}/ -maxdepth 4 -name "${filebase}_wnd.nc" | tail -n1 | xargs echo | cut -d' ' -f1 | awk -F'/' '{print NF}' )
        fi
        if [ $cur -eq 1 ] ; then
          find ${mod_dir}/ -maxdepth 4 -name "${filebase}_cur.nc" >> mod.list.tmp
          n_tree=$(find ${mod_dir}/ -maxdepth 4 -name "${filebase}_cur.nc" | tail -n1 | xargs echo | cut -d' ' -f1 | awk -F'/' '{print NF}' )
        fi
        if [ $mss -eq 1 ] ; then
          find ${mod_dir}/ -maxdepth 4 -name "${filebase}_mss.nc" -o -name "${filebase}_msd.nc" >> mod.list.tmp
          n_tree=$(find ${mod_dir}/ -maxdepth 4 -name "${filebase}_mss.nc" -o -name "${filebase}_msd.nc" | tail -n1 | xargs echo | cut -d' ' -f1 | awk -F'/' '{print NF}' )
        fi
        if [ ! -z "$(echo $modaddvar)"  ] ; then
          find ${mod_dir}/ -maxdepth 4 -name "${filebase}_*.nc" >> mod.list.tmp
          n_tree=$(find ${mod_dir}/ -maxdepth 4 -name "${filebase}_*.nc" | tail -n1 | xargs echo | cut -d' ' -f1 | awk -F'/' '{print NF}' )
        fi
      elif [ "$modsamefile" = 'T' ] ; then
        find ${mod_dir}/ -maxdepth 4 -name "${filebase}.nc"  >> mod.list.tmp
        n_tree=$(find ${mod_dir}/ -maxdepth 4 -name "${filebase}.nc" | tail -n1 | xargs echo | cut -d' ' -f1 | awk -F'/' '{print NF}' )
      fi
      echo "mod n_tree : $n_tree"
      if [ $n_tree -eq 0 ] ; then
        echo "[ERROR] no file ${filebase} found in $mod_dir"
        exit 1
      fi
      sort mod.list.tmp -t '/' -k$n_tree > mod.list
      rm mod.list.tmp

      # skip if no data
      if [ $(cat mod.list | wc -l) -eq 0 ] ; then
        echo "no data for $mod_dir "
#        echo '' > $dir_out/all/track.list_$(basename $obs)
        rm -rf $dir_outobs
        continue
      fi


      # define obs list
      echo "$obs" > obs.list

      # filter missing variables
      tmpobsvarlist=''
      for obsvar in $obsvarlist
      do
        echo $obsvar
        if [ ! -z "$(ncdump -h $obs | grep $obsvar)" ]; then
          tmpobsvarlist="$tmpobsvarlist $obsvar"
        fi
      done
      tmpobsvarlist=$(echo $tmpobsvarlist)
      tmpobsflaglist=''
      for obsflag in $obsflaglist
      do
        echo $obsflag
        if [ ! -z "$(ncdump -h $obs | grep $obsflag)" ]; then
          tmpobsflaglist="$tmpobsflaglist $obsflag"
        fi
      done
      tmpobsflaglist=$(echo $tmpobsflaglist)
      #if [ -z "$(echo $tmpobsvarlist)" ] ; then
      if [ "$tmpobsvarlist" != "$obsvarlist" ] || \
         [ "$tmpobsflaglist" != "$obsflaglist" ] ; then
        echo "[INFO] no matching variable for $(basename $obs):"
        echo "found .${tmpobsvarlist}."
        echo "expecting .${obsvarlist}."
        echo "found .${tmpobsflaglist}."
        echo "expecting .${obsflaglist}."
#        echo '' > $dir_out/all/track.list_$(basename $obs)
        rm -rf ${dir_outobs}
        continue
      fi



      # define prefix
      prefix="${project}${mod}_$(basename $obs)_${year}_"



#--------------------------------------------------------------------------------------------------#
#                                     WW3_MATCH                                                    #
#--------------------------------------------------------------------------------------------------#

      # create ww3_match.nml
      if [ -e ${path_d}/ww3_match.nml_$obsprovider ] ; then
        sed -e "s:<obstime>:'${obstime}':" \
        -e "s:<obscoordlist>:'${obscoordlist}':" \
        -e "s:<obsvarlist>:'${obsvarlist}':" \
        -e "s:<obsflaglist>:'${obsflaglist}':" \
        -e "s:<obsflagcomp>:'${obsflagcomp}':" \
        -e "s:<obsflagvalue>:'${obsflagvalue}':" \
        -e "s:<obssamefile>:'${obssamefile}':" \
        -e "s:<obslon360>:'${obslon360}':" \
        -e "s:<modvarlist>:'${modvarlist}':" \
        -e "s:<modflaglist>:'${modflaglist}':" \
        -e "s:<modflagcomp>:'${modflagcomp}':" \
        -e "s:<modflagvalue>:'${modflagvalue}':" \
        -e "s:<modsamefile>:'${modsamefile}':" \
        -e "s:<modlon360>:'${modlon360}':" \
        -e "s:<dir_out>:'${dir_outobs}':" \
        -e "s/<outprefix>/'$prefix'/" \
        -e "s/<filesplit>/'$filesplit'/" \
        -e "s/<timestart>/'${tmpstartrun}'/" \
        -e "s/<timestop>/'${tmpstoprun}'/" \
        -e "s/<timesplit>/$ndates/" \
        -e "s/<interpspace>/'$interpspace'/" \
        -e "s/<interptime>/$interptime/" \
        -e "s/<maxlandpoints>/$maxlandpoints/" \
        -e "s/<alltimeindex>/$alltimeindex/" \
        -e "s/<clos>/'${closure}'/" \
        -e "s/<lon_min>/$lon_min/" \
        -e "s/<lon_max>/$lon_max/" \
        -e "s/<lat_min>/$lat_min/" \
        -e "s/<lat_max>/$lat_max/" \
        ${path_d}/ww3_match.nml_$obsprovider > ww3_match.nml
      else
cat > ww3_match.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_match.nml - Match in space&time post-processing  !
! -------------------------------------------------------------------- !

! -------------------------------------------------------------------- !
! Define the parameters to postprocess via MATCH_NML namelist
!
! *  To get obs listing file from directory $obs_dir : 
!     find ${obs_dir}/ -name "*nc" | sort -u > obs.list
!
! *  To get mod listing file from directory $mod_dir :
!     find ${mod_dir}/ -name "*wnd.nc" -or -name "*hs.nc" | sort -t '/' -k8 > mod.list
!     Note : with -k8 the number of level in the directory tree $mod_dir
!
! * Converting u-v wind component into speed-direction can be done with nco commandline :
!    ncap2 -s 'wnd=sqrt(pow(uwnd,2)+pow(vwnd,2));
!              wnddir=(180/4*atan(1))*(atan(vwnd/uwnd)+4*atan(1)*((uwnd-abs(uwnd))/(-2*uwnd)))'
!             $file_in $file_out
!
! * Converting longitude from [0.;360.] to [-180.;180.] can be done with nco commandline : 
!    ncap2 -s 'where(longitude>180) longitude=longitude-360' $file_in $file_out
!
! * namelist must be terminated with /
! * definitions & defaults:
!
!     MATCH%OBSFILELIST      = 'obs.list'               ! obs listing file
!     MATCH%OBSTIME          = 'time'                   ! obs time variable
!     MATCH%OBSCOORDLIST     = 'lat lon'                ! obs lat/lon variables
!     MATCH%OBSVARLIST       = 'swh wind_speed'         ! obs variable
!     MATCH%OBSFLAGLIST      = 'swh_quality'            ! obs flag variable
!     MATCH%OBSFLAGCOMP      = '.LT.'                   ! obs flag comparison operator
!     MATCH%OBSFLAGVALUE     = '3'                      ! obs flag value to skip
!     MATCH%OBSSAMEFILE      = T                        ! all the variables in the same file
!     MATCH%OBSLON360        = F                        ! obs longitude [0;360] or [-180;180]
!
!     MATCH%MODFILELIST      = 'mod.list'               ! model listing file
!     MATCH%MODVARLIST       = 'hs uwnd vwnd'           ! model variable
!     MATCH%MODFLAGLIST      = 'dpt'                    ! model flag variable
!     MATCH%MODFLAGCOMP      = '.LT.'                   ! model flag comparison operator
!     MATCH%MODFLAGVALUE     = '0'                      ! model flag value to skip
!     MATCH%MODSAMEFILE      = T                        ! all the variables in the same file
!     MATCH%MODLON360        = F                        ! mod longitude [0;360] or [-180;180]
!
!     MATCH%DIR_OUT          = 'unset'                  ! output directory
!     MATCH%OUTPREFIX        = 'obs_'                   ! output file prefix
!     MATCH%FILESPLIT        = 'NOT'                    ! split output file ['NOT','OBS','MOD']
!     MATCH%NCTYPE           = 4                        ! Netcdf version [3|4]
!
!     MATCH%TIMESTART        = '19000101 000000'        ! Start date for matching fields
!     MATCH%TIMESTOP         = '19000101 000000'        ! Stop  date for matching fields
!     MATCH%TIMESPLIT        = 6                        ! [4(yearly),6(monthly),8(daily),10(hourly)]
!     MATCH%INTERPSPACE      = 'bilinr'                 ! spatial interp. ['nearpt','bilinr','bicubc','filter']
!     MATCH%INTERPTIME       = T                        ! time interpolation
!     MATCH%MAXLANDPOINTS    = 3                        ! maximum number of land points as neighbors
!     MATCH%ALLTIMEINDEX     = F                        ! store all time indexes and set outliers at NaN
!
!     MATCH%CLOS             = 'SMPL'                   ! grid closure ['NONE','SMPL','TRPL']
!     MATCH%LON_MIN          = -180.                    ! minimum longitude [0.;360.] or [-180.;180.]
!     MATCH%LON_MAX          =  180.                    ! maximum longitude [0.;360.] or [-180.;180.]
!     MATCH%LAT_MIN          = -90.                     ! minimum latitude  [-90.;90.]
!     MATCH%LAT_MAX          =  90.                     ! maximum latitude  [-90.;90.]
! -------------------------------------------------------------------- !
&MATCH_NML
MATCH%OBSTIME          = '${obstime}'
MATCH%OBSCOORDLIST     = '${obscoordlist}'
MATCH%OBSVARLIST       = '${obsvarlist}'
MATCH%OBSFLAGLIST      = '${obsflaglist}'
MATCH%OBSFLAGCOMP      = '${obsflagcomp}'
MATCH%OBSFLAGVALUE     = '${obsflagvalue}'
MATCH%OBSSAMEFILE      = ${obssamefile}
MATCH%OBSLON360        = ${obslon360}
!
MATCH%MODVARLIST       = '${modvarlist}'
MATCH%MODFLAGLIST      = '${modflaglist}'
MATCH%MODFLAGCOMP      = '${modflagcomp}'
MATCH%MODFLAGVALUE     = '${modflagvalue}'
MATCH%MODSAMEFILE      = ${modsamefile}
MATCH%MODLON360        = ${modlon360}
!
MATCH%DIR_OUT          = '${dir_outobs}'
MATCH%OUTPREFIX        = '$prefix'
MATCH%FILESPLIT        = '${filesplit}'
MATCH%INTERPSPACE      = '${interpspace}'
MATCH%INTERPTIME       = ${interptime}
MATCH%MAXLANDPOINTS    = ${maxlandpoints}
MATCH%ALLTIMEINDEX     = ${alltimeindex}
!
MATCH%TIMESTART        = '${tmpstartrun}'
MATCH%TIMESTOP         = '${tmpstoprun}'
MATCH%TIMESPLIT        = $ndates
!
MATCH%CLOS             = '${closure}'
MATCH%LON_MIN          = ${lon_min}
MATCH%LON_MAX          = ${lon_max}
MATCH%LAT_MIN          = ${lat_min}
MATCH%LAT_MAX          = ${lat_max}
/


! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF

      fi


      #
      # interpolate model results on obs positions
      #
      echo ""
      echo -n "ww3_match... "
      if ! $tool_exe_path/ww3_match >& $dir_log/ww3_match_${mod}_$(basename $obs)_${tmpstartrun:0:8}_${tmpstoprun:0:8}.out
      then errmsg "Error occured during ww3_match";  exit 1;
      else echo "OK"; fi

      # skip if no data
      if [ -z "$(ls -A ${dir_outobs}/${prefix}*track.nc 2> /dev/null)" ] ; then
        echo "no track data for $(basename $obs)"
#        echo '' > $dir_out/all/track.list_$(basename $obs)
        rm -rf ${dir_outobs}
        continue
      fi

#  if [ $varcur -eq 1 ] ; then
#    ncap2 -O -s 'cur=sqrt(pow(ucur,2)+pow(vcur,2)); curdir=(180/4*atan(1))*(atan(vcur/ucur)+4*atan(1)*((ucur-abs(ucur))/(-2*ucur)))' \
#          ${prefix}track.nc ${prefix}track.nc
#    if [ "$obsvar" = "EWCT" ] ; then
#      ncap2 -O -s 'HCSP=sqrt(pow(EWCT,2)+pow(NSCT,2)); HCDT=(180/4*atan(1))*(atan(NSCT/EWCT)+4*atan(1)*((EWCT-abs(EWCT))/(-2*EWCT)))' \
#            ${prefix}track.nc ${prefix}track.nc
#      tmpobsvarlist="$(echo $tmpobsvarlist | sed 's/EWCT NSCT/HCSP HCDT/')"
#    fi
#  fi
#  if [ $varwnd -eq 1 ] ; then
#    ncap2 -O -s 'wnd=sqrt(pow(uwnd,2)+pow(vwnd,2)); wnddir=(180/4*atan(1))*(atan(vwnd/uwnd)+4*atan(1)*((uwnd-abs(uwnd))/(-2*uwnd)))' \
#          ${prefix}track.nc ${prefix}track.nc
#    if [ "$obsvar" = "WSPE" ] ; then
#      ncap2 -O -s 'WSPD=sqrt(pow(WSPE,2)+pow(WSPN,2)); WSDIR=(180/4*atan(1))*(atan(WSPN/WSPE)+4*atan(1)*((WSPE-abs(WSPE))/(-2*WSPE)))' \
#            ${prefix}track.nc ${prefix}track.nc
#      tmpobsvarlist="$(echo $tmpobsvarlist | sed 's/WSPE WSPN/WSPD WSDIR/')"
#    fi
#  fi
#  #if [ $varmss -eq 1 ] ; then
#    # obs
#    #ncap2 -O -s "${obsmss}=1/(exp((${obsmss})*(0.1*log(10))))" ${prefix}track.nc ${prefix}track.nc
#    #ncatted -a units,${obsmss},m,c,'linear scale' ${prefix}track.nc
#    #ncatted -a formula,${obsmss},c,c,"${obsmss}=1/(exp((${obsmss})*(0.1*log(10))))" ${prefix}track.nc
#    # mod
#  #  ncap2 -O -s 'mss=mssu+mssc' ${prefix}track.nc ${prefix}track.nc
#  #  ncatted -a formula,mss,c,c,'mss=mssu+mssc' ${prefix}track.nc
#  #  ncatted -a long_name,mss,m,c,'mean square slope' ${prefix}track.nc
#  #  ncatted -a standard_name,mss,m,c,'mean_square_slope' ${prefix}track.nc
#  #fi



#--------------------------------------------------------------------------------------------------#
#                                     WW3_STAT IN REGULAR GRID                                     #
#--------------------------------------------------------------------------------------------------#

      # create track.list
      echo "${dir_outobs}/${prefix}track.nc" > track.list

      # estimate min/max lat/lon for stat
      lat="$(echo $obscoordlist | cut -d ' ' -f1)"
      lon="$(echo $obscoordlist | cut -d ' ' -f2)"
      ncwa -O -y avg -v $lat ${dir_outobs}/${prefix}track.nc ${dir_outobs}/${prefix}track_lat_avg.nc
      ncwa -O -y min -v $lat ${dir_outobs}/${prefix}track.nc ${dir_outobs}/${prefix}track_lat_min.nc
      ncwa -O -y max -v $lat ${dir_outobs}/${prefix}track.nc ${dir_outobs}/${prefix}track_lat_max.nc
      ncwa -O -y avg -v $lon ${dir_outobs}/${prefix}track.nc ${dir_outobs}/${prefix}track_lon_avg.nc
      ncwa -O -y min -v $lon ${dir_outobs}/${prefix}track.nc ${dir_outobs}/${prefix}track_lon_min.nc
      ncwa -O -y max -v $lon ${dir_outobs}/${prefix}track.nc ${dir_outobs}/${prefix}track_lon_max.nc
      tmplat_avg=$(ncdump ${dir_outobs}/${prefix}track_lat_avg.nc | grep "$lat =" | awk -F '=' '{print $2}' | awk -F ';' '{print $1}')
      tmplat_min=$(ncdump ${dir_outobs}/${prefix}track_lat_min.nc | grep "$lat =" | awk -F '=' '{print $2}' | awk -F ';' '{print $1}')
      tmplat_max=$(ncdump ${dir_outobs}/${prefix}track_lat_max.nc | grep "$lat =" | awk -F '=' '{print $2}' | awk -F ';' '{print $1}')
      tmplon_avg=$(ncdump ${dir_outobs}/${prefix}track_lon_avg.nc | grep "$lon =" | awk -F '=' '{print $2}' | awk -F ';' '{print $1}')
      tmplon_min=$(ncdump ${dir_outobs}/${prefix}track_lon_min.nc | grep "$lon =" | awk -F '=' '{print $2}' | awk -F ';' '{print $1}')
      tmplon_max=$(ncdump ${dir_outobs}/${prefix}track_lon_max.nc | grep "$lon =" | awk -F '=' '{print $2}' | awk -F ';' '{print $1}')


      # skip if the difference between min and max latitude or longitude is more than 30km (0.33deg)
      rangethreshold=0.33
      rangelat=$(echo "scale=4;$tmplat_max-$tmplat_min" | bc)
      rangelon=$(echo "scale=4;$tmplon_max-$tmplon_min" | bc)
      if [ $rangelon -gt $rangethreshold ] || [ $rangelat -gt $rangethreshold ] ; then
        echo "[WARNING] min and max latitude or longitude varies more than 30km. statistics are skipped"
        continue
      fi

      # add shift for averaging
      tmplat_min=$(echo "scale=4;$tmplat_min-$dy/10" | bc)
      tmplat_max=$(echo "scale=4;$tmplat_max+$dy/10" | bc)
      tmplon_min=$(echo "scale=4;$tmplon_min-$dx/10" | bc)
      tmplon_max=$(echo "scale=4;$tmplon_max+$dx/10" | bc)

      # create ww3_stat.nml
      if [ -e ${path_d}/ww3_stat.nml_$obsprovider ] ; then
        sed -e "s:<trkcoordlist>:'${obscoordlist}':" \
        -e "s:<obsvarlist>:'${statobsvarlist}':" \
        -e "s:<modvarlist>:'${statmodvarlist}':" \
        -e "s:<dir_out>:'${dir_outobs}':" \
        -e "s/<outprefix>/'$prefix'/" \
        -e "s/<timestart>/'${tmpstartrun}'/" \
        -e "s/<timestop>/'${tmpstoprun}'/" \
        -e "s/<lon_min>/${tmplon_min}/" \
        -e "s/<lon_max>/${tmplon_max}/" \
        -e "s/<lat_min>/${tmplat_min}/" \
        -e "s/<lat_max>/${tmplat_max}/" \
        -e "s/<dx>/$dx/" \
        -e "s/<dy>/$dy/" \
        -e "s/<navgmin>/$navgmin/" \
        -e "s/<norm>/'$norm'/" \
        ${path_d}/ww3_stat.nml_$obsprovider > ww3_stat.nml
      else
cat > ww3_stat.nml << EOF
! -------------------------------------------------------------------- !
! WAVEWATCH III - ww3_stat.nml - Averaging date over a regular grid    !
! -------------------------------------------------------------------- !

! -------------------------------------------------------------------- !
! Define the parameters to postprocess via STAT_NML namelist
!
! *  To get track listing file from directory $obs_dir : 
!     find ${obs_dir}/ -name "*nc" | sort -u > track.list
!
! * Converting u-v wind component into speed-direction can be done with nco commandline :
!    ncap2 -s 'wnd=sqrt(pow(uwnd,2)+pow(vwnd,2));
!              wnddir=(180/4*atan(1))*(atan(vwnd/uwnd)+4*atan(1)*((uwnd-abs(uwnd))/(-2*uwnd)))'
!             $file_in $file_out
!
! * Converting longitude from [0.;360.] to [-180.;180.] can be done with nco commandline : 
!    ncap2 -s 'where(longitude>180) longitude=longitude-360' $file_in $file_out
!
! * namelist must be terminated with /
! * definitions & defaults:
!
!     STAT%TRKFILELIST      = 'track.list'             ! track listing file
!     STAT%TRKCOORDLIST     = 'lat lon'                ! track lat/lon variables
!     STAT%OBSVARLIST       = 'swh u10m'               ! obs variables
!     STAT%MODVARLIST       = 'hs wnd'                 ! model variables
!     STAT%TRKLON360        = F                        ! track longitude [0;360] or [-180;180]
!
!     STAT%DIR_OUT          = 'unset'                  ! output directory
!     STAT%OUTPREFIX        = 'ww3.'                   ! output file prefix
!     STAT%NCTYPE           = 4                        ! Netcdf version [3|4]
!
!     STAT%TIMESTART        = '19000101 000000'        ! Start date for averaging fields
!     STAT%TIMESTOP         = '19000101 000000'        ! Stop  date for averaging fields
!
!     STAT%LON_MIN          = -180.                    ! minimum longitude [0.;360.] or [-180.;180.]
!     STAT%LON_MAX          =  180.                    ! maximum longitude [0.;360.] or [-180.;180.]
!     STAT%LAT_MIN          = -90.                     ! minimum latitude  [-90.;90.]
!     STAT%LAT_MAX          =  90.                     ! maximum latitude  [-90.;90.]
!
!     STAT%DX               = 0.5                      ! resolution along longitude in degrees
!     STAT%DY               = 0.5                      ! resolution along latitude in degrees
!     STAT%NAVGMIN          = 6                        ! minimum number of value for averaging
!     STAT%NORM             = 'OBS'                    ! normalize by 'MOD' or 'OBS'
! -------------------------------------------------------------------- !
&STAT_NML
STAT%TRKCOORDLIST     = '${obscoordlist}'
STAT%OBSVARLIST       = '${statobsvarlist}'
STAT%MODVARLIST       = '${statmodvarlist}'
!
STAT%DIR_OUT          = '${dir_outobs}'
STAT%OUTPREFIX        = '$prefix'
!
STAT%TIMESTART        = '${tmpstartrun}'
STAT%TIMESTOP         = '${tmpstoprun}'
!
STAT%LON_MIN          = ${tmplon_min}
STAT%LON_MAX          = ${tmplon_max}
STAT%LAT_MIN          = ${tmplat_min}
STAT%LAT_MAX          = ${tmplat_max}
! 
STAT%DX               = $dx
STAT%DY               = $dy
STAT%NAVGMIN          = $navgmin
STAT%NORM             = '$norm'
/


! -------------------------------------------------------------------- !
! WAVEWATCH III - end of namelist                                      !
! -------------------------------------------------------------------- !
EOF
      fi

      #
      # compute statistics between model and satellite on regular grid
      #
      echo ""
      echo -n "ww3_stat... "
      #rm -f $dir_log/ww3_stat_${year}_${mod}_$(basename $obs)_${tmpstartrun:0:8}_${tmpstoprun:0:8}.out 
      if ! $tool_exe_path/ww3_stat >& $dir_log/ww3_stat_${year}_${mod}_$(basename $obs)_${tmpstartrun:0:8}_${tmpstoprun:0:8}.out
      then errmsg "Error occured during ww3_stat";  exit 1;
      else echo "OK"; fi
#      mv ww3_stat.nml ww3_stat_${dx}.nml
#      mv ww3_stat.nml.log ww3_stat_${dx}.nml.log

      # skip if no data
      if [ ! -e ${prefix}stat.nc ] ; then
        echo "no data for $(basename $obs)"
        continue
      fi

      # add time dimension
      ncecat -O ${prefix}stat.nc ${prefix}stat.nc
      ncrename -d record,time ${prefix}stat.nc
      ncap2 -O -s "time[time]=double($timeday)" ${prefix}stat.nc ${prefix}stat.nc
      ncatted -h -a units,time,o,c,'days since 1990-01-01T00:00:00Z' ${prefix}stat.nc
      ncatted -h -a calendar,time,c,c,'standard' ${prefix}stat.nc

      #check if some errors occured during process
      echo ""
      echo -n "Check logs in $dir_outobs ... "
      $waverun_path/check_error.sh $dir_outobs
      echo "OK"

    done # loop on stat period

  done # loop on obs


#  # remove empty directories
#  echo "remove empty directories..."
#  for dir in $(find $dir_out/ -type d -mindepth 1 -maxdepth 2 -name "*_MO_*")
#  do
#    if [ -z "$(find $dir -name "*nc")" ]; then
#      echo "empty $dir removed"
#      rm -rf $dir
#    fi
#  done



done # loop on mods


echo ""
echo -e "\n END OF CORREL obs!"
